#include "frontera.h"
#define v3f glVertex3f

#define DISPLAY_ROBOT 0x01UL
#define DISPLAY_SONARS 0x04UL
#define DISPLAY_LASER 0x08U
#define DISPLAY_COLORIMAGEA 0x10UL
#define DISPLAY_COLORIMAGEB 0x20UL
#define DISPLAY_COLORIMAGEC 0x40UL
#define DISPLAY_COLORIMAGED 0x80UL

//DEFINIMOS LOS VALORES QUE TENDRA QUE CUMPLIR EL FILTRO HSI
#define H_SUELO_MAX   224.321*DEGTORAD//47.595*DEGTORAD
#define H_SUELO_MIN   282.025*DEGTORAD//110.619*DEGTORAD
#define S_SUELO_MAX   1.0  
#define S_SUELO_MIN   0.0

#define PI 3.141592654

#define MOUSELEFT 1
#define PUSHED 1
#define RELEASED 0

#define MAX_RADIUS_VALUE 10000000
#define MIN_RADIUS_VALUE 0
#define WHEEL_DELTA 1000

// Valores imprescindibles para hacer estimaciones de distancias
#define foco 4.0
#define	focover 37.0	// Angulos de apertura
#define	focohor 48.0

// ARCO QUE REALIZA LA CAMARA AL REALIZAR EL BARRIDO
#define GRADOS_ARCO 180

#define resolucion (int) ((SIFNTSC_COLUMNS/focohor)*GRADOS_ARCO)

#define puntos_clave 5
#define IZQ  1
#define DER  -1

#define MAXWORLD 30000.

// Variables para guardar las matrices K, R y T de nuestra c�mara:
gsl_matrix *K_1,*R_1;
gsl_vector* x_1;
HPoint2D progtest2D;
HPoint3D progtest3D;

struct HSV* myHSV;
TPinHoleCamera virtualcam0; /* para ver el minimundillo */
TPinHoleCamera virtualcam1; /* para moverme por el mundillo */
TPinHoleCamera virtualcam2; /* ser� mycolorA */
TPinHoleCamera virtualcam3; /* ser� mycolorB */
TPinHoleCamera virtualcam4; /* ser� mycolorC */
TPinHoleCamera virtualcam5; /* ser� mycolorD */
TPinHoleCamera robotCamera;
TPinHoleCamera *myCamera;

int flashImage = TRUE;

Display *mydisplay;
int *myscreen;

HPoint2D myActualPoint2D; // variables para el c�lculo de los rayos virtuales
HPoint3D myActualPoint3D;
HPoint2D myActualPointImage;
HPoint3D cameraPos3D;
HPoint3D myEndPoint3D;
HPoint3D intersectionPoint;

HPoint3D groundPoints[SIFNTSC_COLUMNS*SIFNTSC_ROWS];
int actualGroundPoint = 0; // contador para el punto actual del suelo...

float distanciaPlanoImagen, incremento;

HPoint2D mouse_on_canvas;

/*Gui callbacks*/
registerbuttons myregister_buttonscallback;
registerdisplay myregister_displaycallback;
deletebuttons mydelete_buttonscallback;
deletedisplay mydelete_displaycallback;

// char myR[SIFNTSC_COLUMNS*SIFNTSC_ROWS], myG[SIFNTSC_COLUMNS*SIFNTSC_ROWS], myB[SIFNTSC_COLUMNS*SIFNTSC_ROWS];
float myR, myG, myB;
//struct HSV* myHSV;
static int vmode;
static XImage *imagenA,*imagenB,*imagenC,*imagenD,*imagenFiltrada, *imagenSuelo;
static char *imagenA_buf, *imagenB_buf, *imagenC_buf, *imagenD_buf, *imagenFiltrada_buf, *imagenSuelo_buf; /* puntero a memoria para la imagen a visualizar en el servidor X. No compartida con el servidor */
static long int tabla[256]; 
/* tabla con la traduccion de niveles de gris a numero de pixel en Pseudocolor-8bpp. Cada numero de pixel apunta al valor adecuado del ColorMap, con el color adecuado instalado */
static int pixel8bpp_rojo, pixel8bpp_blanco, pixel8bpp_amarillo;

int groundView = FALSE;
int foa_mode, cam_mode;
float radius = 500;
float t = 0.5; // lambda

static int message = FALSE;

static char *samplesource=NULL;

int frontera_id=0; 
int frontera_brothers[MAX_SCHEMAS];
arbitration frontera_callforarbitration;
int frontera_cycle=300; /* ms */

char **myActualCamera;

char **mycolorA;
resumeFn colorAresume;
suspendFn colorAsuspend;

char **mycolorB;
resumeFn colorBresume;
suspendFn colorBsuspend;

char **mycolorC;
resumeFn colorCresume;
suspendFn colorCsuspend;

char **mycolorD;
resumeFn colorDresume;
suspendFn colorDsuspend;

FD_fronteragui *fd_fronteragui;
GC fronteragui_gc;
Window fronteragui_win; 
unsigned long display_state;

void initCalibration () {
  K_1 = gsl_matrix_alloc(3,3);
  R_1 = gsl_matrix_alloc(3,3);
  x_1 = gsl_vector_alloc(3);

	// Fill K_1 matrix, with information got on Calibrator GUI
	gsl_matrix_set(K_1,0,0,428.0);
  gsl_matrix_set(K_1,0,1,-5.66);
  gsl_matrix_set(K_1,0,2,173.71);

	gsl_matrix_set(K_1,1,0,0);
  gsl_matrix_set(K_1,1,1,439.39);
  gsl_matrix_set(K_1,1,2,129.55);

	gsl_matrix_set(K_1,2,0,0);
  gsl_matrix_set(K_1,2,1,0);
  gsl_matrix_set(K_1,2,2,0);

	// Fill R_1 matrix, with information got on Calibrator GUI
	gsl_matrix_set(R_1,0,0,0.66);
  gsl_matrix_set(R_1,0,1,-0.75);
  gsl_matrix_set(R_1,0,2,-0.02);

	gsl_matrix_set(R_1,1,0,-0.36);
  gsl_matrix_set(R_1,1,1,-0.34);
  gsl_matrix_set(R_1,1,2,0.87);

	gsl_matrix_set(R_1,2,0,0.66);
  gsl_matrix_set(R_1,2,1,0.57);
  gsl_matrix_set(R_1,2,2,0.50);

	// Fill x_1 vector, with information got on Calibrator GUI
  gsl_vector_set(x_1, 0, -275.9);
  gsl_vector_set(x_1, 1, -276.5);
  gsl_vector_set(x_1, 2, -223.6);
}

double rad2deg(double alfa){
  return (alfa*180)/3.14159264;
}

void setCameraPos(int camara, gsl_matrix* K, gsl_matrix* R, gsl_vector* T ){

  double psi,theta,gama;
  float ppx = gsl_matrix_get(K,0,2)/SIFNTSC_COLUMNS;
  float ppy = gsl_matrix_get(K,1,2)/SIFNTSC_COLUMNS;
  
  /* Aplicamos las transformaciones necesarias, para pasar
     de nuestra mariz de rotacion a la angulos Euler */

  theta = acos(gsl_matrix_get(R,2,2));
  gama = acos(gsl_matrix_get(R,2,1)/-sin(theta));
  psi = asin(gsl_matrix_get(R,0,2)/sin(theta));
  theta = rad2deg(theta);
  gama  = rad2deg(gama);
  psi = rad2deg(psi);

  glMatrixMode(GL_MODELVIEW);  
  glLoadIdentity();
  glTranslatef(
	       -(float)gsl_vector_get(T,0),
	       -(float)gsl_vector_get(T,1),
	       -(float)gsl_vector_get(T,2)
	       );
  
  /* Rotacion de gama grados en Z*/
  glRotatef(gama,0.,0.,1.);
  /* Rotacion de gama grados en X*/
  glRotatef(theta,1.,0.,0.);
  /* Rotacion de gama grados en Z*/
  glRotatef(psi,0.,0.,1.);

	//drawOpticalRays (&robotCamera); // ejes �pticos

	glLoadIdentity ();
	drawCam (&robotCamera, 1);
  //drawRobotCamera(ppx, ppy, camara);
}

void drawAxes() {
  int i=0;

  /* ejes X, Y, Z*/
  glLineWidth(2.3f);
  glMatrixMode(GL_MODELVIEW);

  glLoadIdentity();
  glColor3f( 0.7, 0., 0. );
  glBegin(GL_LINES);
  v3f( 0.0, 0.0, 0.0 );   
  v3f( 10000.0, 0.0, 0.0 );
  glEnd();

  glColor3f( 0.0, 0.7, 0. );
  glBegin(GL_LINES);
  v3f( 0.0, 0.0, 0.0 );   
  v3f( 0.0, 10000.0, 0.0 );
  glEnd();

  glColor3f( 0., 0., 0.7 );
  glBegin(GL_LINES);
  v3f( 0.0, 0.0, 0.0 );   
  v3f( 0.0, 0.0, 10000.0 );
  glEnd();
  glLineWidth(1.0f);
}

void progeo(){
  int i,j=0;
  gsl_matrix *RT,*T,*Res;
  HPoint2D p2;
  HPoint3D p3;
  
  /** Progeo params*/
  
  RT = gsl_matrix_alloc(4,4);
  T = gsl_matrix_alloc(3,4);
  Res = gsl_matrix_alloc(3,4);
  
  gsl_matrix_set(T,0,0,1);
  gsl_matrix_set(T,1,1,1);
  gsl_matrix_set(T,2,2,1);

  gsl_matrix_set(T,0,3,gsl_vector_get(x_1,0));
  gsl_matrix_set(T,1,3,gsl_vector_get(x_1,1));
  gsl_matrix_set(T,2,3,gsl_vector_get(x_1,2));

  gsl_linalg_matmult (R_1,T,Res);  

  for (i=0;i<3;i++)
    for (j=0;j<4;j++)
      gsl_matrix_set(RT,i,j,gsl_matrix_get(Res,i,j));
  
  
  /** set 0001 in the last row of RT */
  gsl_matrix_set(RT,3,0,0);
  gsl_matrix_set(RT,3,1,0);
  gsl_matrix_set(RT,3,2,0);
  gsl_matrix_set(RT,3,3,1);

  /** Set camera position*/

  robotCamera.position.X = gsl_vector_get(x_1,0);
  robotCamera.position.Y = gsl_vector_get(x_1,1);
  robotCamera.position.Z = gsl_vector_get(x_1,2);

	//printf("Camera position %f:%f:%f\n",robotCamera.position.X, robotCamera.position.Y, robotCamera.position.Z);
      
  /** Seting intrensic matrix*/
  robotCamera.k11 = gsl_matrix_get(K_1,0,0);
  robotCamera.k12 = gsl_matrix_get(K_1,0,1);
  robotCamera.k13 = gsl_matrix_get(K_1,0,2);
  robotCamera.k14 = 0;

  robotCamera.k21 = gsl_matrix_get(K_1,1,0);
  robotCamera.k22 = gsl_matrix_get(K_1,1,1);
  robotCamera.k23 = gsl_matrix_get(K_1,1,2);
  robotCamera.k24 = 0;

  robotCamera.k31 = gsl_matrix_get(K_1,2,0);
  robotCamera.k32 = gsl_matrix_get(K_1,2,1);
  robotCamera.k33 = gsl_matrix_get(K_1,2,2);
  robotCamera.k34 = 0;

  /** Seting extrensic*/
  robotCamera.rt11 = gsl_matrix_get(RT,0,0);
  robotCamera.rt12 = gsl_matrix_get(RT,0,1);
  robotCamera.rt13 = gsl_matrix_get(RT,0,2);
  robotCamera.rt14 = gsl_matrix_get(RT,0,3);
  
  robotCamera.rt21 = gsl_matrix_get(RT,1,0);
  robotCamera.rt22 = gsl_matrix_get(RT,1,1);
  robotCamera.rt23 = gsl_matrix_get(RT,1,2);
  robotCamera.rt24 = gsl_matrix_get(RT,1,3);

  robotCamera.rt31 = gsl_matrix_get(RT,2,0);
  robotCamera.rt32 = gsl_matrix_get(RT,2,1);
  robotCamera.rt33 = gsl_matrix_get(RT,2,2);
  robotCamera.rt34 = gsl_matrix_get(RT,2,3);

  robotCamera.rt41 = gsl_matrix_get(RT,3,0);
  robotCamera.rt42 = gsl_matrix_get(RT,3,1);
  robotCamera.rt43 = gsl_matrix_get(RT,3,2);
  robotCamera.rt44 = gsl_matrix_get(RT,3,3);

  progtest2D.x = 0;
  progtest2D.y = 0;
  progtest2D.h = 1;

//  project(p3,&p2,robotCamera);
  
//	printf("El punto project en %f:%f:%f\n",p2.x,p2.y,p2.h);

  if (backproject(&progtest3D, progtest2D, robotCamera)){
    
    progtest3D.X =  progtest3D.X/progtest3D.H;
    progtest3D.Y =  progtest3D.Y/progtest3D.H;
    progtest3D.Z =  progtest3D.Z/progtest3D.H;

    printf("el backproject de [%.2f,%.2f] es [%.2f,%.2f,%.2f,%.2f] \n\n",
	     progtest2D.x,progtest2D.y,
	     progtest3D.X, progtest3D.Y, progtest3D.Z, progtest3D.H
	     );
  }
   
}

void drawCube() {
  
  float i;

  glLoadIdentity();
  /*las 3 caras del cubo */
  
  glColor3f( 1.0, 1.0, 1.0 );
  glBegin(GL_POLYGON);
  v3f(98,98,98);
  v3f(0,98,98);
  v3f(0,98,0);
  v3f(98,98,0);
  glEnd();

  glColor3f( 1.0, 1.0, 1.0 );
  glBegin(GL_POLYGON);
  v3f(98,0,98);
  v3f(98,98,98);
  v3f(98,98,0);
  v3f(98,0,0);
  glEnd();
  
  glColor3f( 1.0, 1.0, 1.0 );
  glBegin(GL_POLYGON);
  v3f(98,0,98);
  v3f(0,0,98);
  v3f(0,98,98);
  v3f(98,98,98);
  glEnd();

  glColor3f( .0, .0, .0 );
  glBegin(GL_LINES);
  for (i=100;i>0;i=i-16){

    v3f(100,i,100);
    v3f(0,i,100);
    v3f(i,0,100);
    v3f(i,100,100);

    v3f(100,100,i);
    v3f(100,0,i);
    v3f(100,i,0);
    v3f(100,i,100);

    v3f(100,100,i);
    v3f(0,100,i);
    v3f(i,100,0);
    v3f(i,100,100);

  }
  glEnd();


  glColor3f( 0.0, 0.0, 1.0 );
  glBegin(GL_POLYGON);
  v3f(52,52,100);
  v3f(52,35,100);
  v3f(35,35,100);  
  v3f(35,52,100);
  glEnd();

  glColor3f( 1.0, 0.0, 0.0 );
  glBegin(GL_POLYGON);
  v3f(100,35,35);
  v3f(100,35,52);
  v3f(100,52,52);
  v3f(100,52,35);
  glEnd();

  glColor3f( 0.0, 1.0, 0.0 );
  glBegin(GL_POLYGON);
  v3f(52,100,35);
  v3f(52,100,52);
  v3f(35,100,52);
  v3f(35,100,35);
  glEnd();

  glFlush();

}

void drawRobotCamera(double ppx, double ppy, int camera){
  
  /* Estos incrementos se usan para obtener el siguiente
     punto*/
  float inc_x = (1./SIFNTSC_COLUMNS)*100;
  float inc_y = (1./SIFNTSC_ROWS)*100;
  
  int i,j,offset,red,green,blue;

	/* dibujamos el rectangulo donde se proyectar� la imagen */
  glColor3f( 0.0, 0.0, 0.0 );  
  glScalef(0.2,0.2,0.2);
  
  glBegin(GL_LINES);
  v3f( -50, 50, -100 );   
  v3f( 50, 50, -100 );   
  glEnd();
  
  glBegin(GL_LINES);
  v3f( -50, 50, -100 );   
  v3f( -50, -50, -100 );   
  glEnd();

  glBegin(GL_LINES);
  v3f( 50, 50, -100 );   
  v3f( 50, -50, -100 );   
  glEnd();

  glBegin(GL_LINES);
  v3f( 50, -50, -100 );   
  v3f( -50, -50, -100 );   
  glEnd();

  /* pintamos la imagen*/
  if ((mycolorA!=NULL) || (mycolorB!=NULL))
    for (j=0;j<SIFNTSC_ROWS;j++)
      for(i=0;i<SIFNTSC_COLUMNS;i++) {
			  /* obtenemos el color del pixel de la imagen de entrada */
				offset = j*320+i;
				if (((display_state&DISPLAY_COLORIMAGEA)!=0) && (mycolorA!=NULL)) {
				  red = (*mycolorA)[offset*3+1];
				  green = (*mycolorA)[offset*3+2];
				  blue = (*mycolorA)[offset*3+3];
				}
				
				glColor3f( (float)red/255.0, (float)green/255.0, (float)blue/255.0 );  
				
				/* pintamos el punto correspondiente en el
				   escenario pintado con OGL */
				glBegin(GL_POINTS);
				v3f(-50.+(inc_x*i),50.-(inc_y*j),-100. );
				glEnd();
			}
  
  /* fin del dibujo del rectangulo y su imagen interior. Ahora dibujamos los tri�ngulos. */
  glColor3f( 1.0, 0.0, 0.0 );  
  glBegin(GL_LINES);
  v3f( 0.0, 0.0, 0.0 );   
  v3f( -50, 50, -100 );   
  glEnd();

  glColor3f( 0.0, 1.0, 0.0 );  
  glBegin(GL_LINES);
  v3f( 0.0, 0.0, 0.0 );   
  v3f( 50, 50, -100 );   
  glEnd();

  glColor3f( 0.0, 0.0, 1.0 );  
  glBegin(GL_LINES);
  v3f( 0.0, 0.0, 0.0 );   
  v3f( 50, -50, -100 );   
  glEnd();
  
  glColor3f( 1.0, 1.0, 0.0 );  
  glBegin(GL_LINES);
  v3f( 0.0, 0.0, 0.0 );   
  v3f( -50, -50, -100 );   
  glEnd();

  glBegin(GL_LINES);
  glColor3f( 0.0, 1.0, 0.0 );  
  v3f( 0.0, 0.0, 0.0 );   
  v3f( -50+ppx,-50+ppy, -100 );   
  glEnd();

  /* Por alguna razon, el tama�o maximo que se puede utilizar para un punto
     es de un solo pixel que es practicamente invisible para un ser humano.
     Visto esto, se ha decidido usar un cubo de un lado muy pequeño para 
     dibujar "puntos grandes
  */
/*  glLoadIdentity();
  glColor3f( 1.0, 0.0, 0.0 );
	if (((display_state&DISPLAY_COLORIMAGEA)!=0) && (mycolorA!=NULL))
    glTranslatef( 
		 -(float)gsl_vector_get(x_1,0),
		 -(float)gsl_vector_get(x_1,1),
		 -(float)gsl_vector_get(x_1,2));
  
  glutSolidCube(0.01);*/
	glScalef(1,1,1); // reestablecemos la escala
}

void drawRoboticLabPlanes () {
  /** Absolute Frame of Reference **/  
  /* floor **
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  glColor3f( 0.6, 0.6, 0.6 );
  glBegin(GL_LINES);
  for(i=0;i<((int)MAXWORLD+1);i++)
    {
      v3f(-(int)MAXWORLD*10/2.+(float)i*10,-(int)MAXWORLD*10/2.,0.);
      v3f(-(int)MAXWORLD*10/2.+(float)i*10,(int)MAXWORLD*10/2.,0.);
      v3f(-(int)MAXWORLD*10/2.,-(int)MAXWORLD*10/2.+(float)i*10,0.);
      v3f((int)MAXWORLD*10/2.,-(int)MAXWORLD*10/2.+(float)i*10,0.);
    }
  glEnd();
*/
  
  /* absolute axis **
  glLineWidth(3.0f);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glColor3f( 0.7, 0., 0. );
  glBegin(GL_LINES);
  v3f( 0.0, 0.0, 0.0 );   
  v3f( 10.0, 0.0, 0.0 );
  glEnd();
  glColor3f( 0.,0.7,0. );
  glBegin(GL_LINES);
  v3f( 0.0, 0.0, 0.0 );   
  v3f( 0.0, 10.0, 0.0 );
  glEnd();
  glColor3f( 0.,0.,0.7 );
  glBegin(GL_LINES);
  v3f( 0.0, 0.0, 0.0 );   
  v3f( 0.0, 0.0, 10.0 );
  glEnd();
  glLineWidth(1.0f);
*/
	/* LABORATORIO DE ROB�TICA */
  glLineWidth(2.0f);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

	/* SUELO */
  glColor3f(0.93, 0.93, 0.455);
	// Parte central:
  glBegin(GL_QUADS);
	  v3f(0.000000, 0.000000, 0.000000);
	  v3f(0.000000, 4589.000000, 0.000000);
	  v3f(7925.000000, 4589.000000, 0.000000);
	  v3f(7925.000000, 0.000000, 0.000000);
  glEnd();

	// Lateral de la puerta (1� parte):
  glBegin(GL_QUADS);
	  v3f(1211.000000, -554.000000, 0.000000);
	  v3f(1211.000000, 5.000000, 0.000000);
	  v3f(5476.000000, 5.000000, 0.000000);
	  v3f(5476.000000, -554.000000, 0.000000);
  glEnd();

	// Lateral de la puerta (trocito intermedio de sobrante defectuoso):
  glBegin(GL_QUADS);
	  v3f(5470.000000, -120.000000, 0.000000);
	  v3f(5470.000000, 10.000000, 0.000000);
	  v3f(6115.000000, 10.000000, 0.000000);
	  v3f(6115.000000, -120.000000, 0.000000);
  glEnd();

	// Lateral de la puerta (2� parte):
  glBegin(GL_QUADS);
	  v3f(6110.000000, -554.000000, 0.000000);
	  v3f(6110.000000, 5.000000, 0.000000);
	  v3f(7925.000000, 5.000000, 0.000000);
	  v3f(7925.000000, -554.000000, 0.000000);
  glEnd();

	// Lateral enfrente de la puerta (1� parte):
  glBegin(GL_QUADS);
	  v3f(2419.000000, 4580.000000, 0.000000);
	  v3f(2419.000000, 4879.000000, 0.000000);
	  v3f(5498.000000, 4879.000000, 0.000000);
	  v3f(5498.000000, 4580.000000, 0.000000);
  glEnd();

	// Lateral enfrente de la puerta (2� parte):
  glBegin(GL_QUADS);
	  v3f(6264.000000, 4580.000000, 0.000000);
	  v3f(6264.000000, 4879.000000, 0.000000);
	  v3f(7925.000000, 4879.000000, 0.000000);
	  v3f(7925.000000, 4580.000000, 0.000000);
  glEnd();

	/* CAMPO DE FUTBOL */
  glColor3f(0., 0.59, 0.);
  glBegin(GL_QUADS); // daremos algo de altura para no causar "conflictos graficos" con el suelo
	  v3f(1560.000000, 596.000000, 25.000000);
	  v3f(1560.000000, 4589.000000, 25.000000);
	  v3f(7925.000000, 4589.000000, 25.000000);
	  v3f(7925.000000, 596.000000, 25.000000);
  glEnd();

	/* PAREDES */

	// Lateral izquierdo:
  glColor3f(0.98, 0.98, 0.98);
  glBegin(GL_QUADS);
	  v3f(0.000000, 0.000000, 3000.000000);
	  v3f(0.000000, 4589.000000, 3000.000000);
	  v3f(0.000000, 4589.000000, 0.000000);
	  v3f(0.000000, 0.000000, 0.000000);
	glEnd();

	// Lateral enfrente de la puerta:
  glBegin(GL_QUADS); // 1� parte
	  v3f(0.000000, 4589.000000, 0.000000);
	  v3f(0.000000, 4589.000000, 3000.000000);
	  v3f(2419.000000, 4589.000000, 3000.000000);
	  v3f(2419.000000, 4589.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 2� parte
	  v3f(2419.000000, 4589.000000, 0.000000);
	  v3f(2419.000000, 4589.000000, 3000.000000);
	  v3f(2419.000000, 4879.000000, 3000.000000);
	  v3f(2419.000000, 4879.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 3� parte
	  v3f(2419.000000, 4879.000000, 0.000000);
	  v3f(2419.000000, 4879.000000, 3000.000000);
	  v3f(5476.000000, 4879.000000, 3000.000000);
	  v3f(5476.000000, 4879.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 4� parte
	  v3f(5476.000000, 4879.000000, 0.000000);
	  v3f(5476.000000, 4879.000000, 3000.000000);
	  v3f(5476.000000, 4589.000000, 3000.000000);
	  v3f(5476.000000, 4589.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 5� parte
	  v3f(5476.000000, 4589.000000, 0.000000);
	  v3f(5476.000000, 4589.000000, 3000.000000);
	  v3f(6264.000000, 4589.000000, 3000.000000);
	  v3f(6264.000000, 4589.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 6� parte
	  v3f(6264.000000, 4589.000000, 0.000000);
	  v3f(6264.000000, 4589.000000, 3000.000000);
	  v3f(6264.000000, 4879.000000, 3000.000000);
	  v3f(6264.000000, 4879.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 6� parte
	  v3f(6264.000000, 4879.000000, 0.000000);
	  v3f(6264.000000, 4879.000000, 3000.000000);
	  v3f(7925.000000, 4879.000000, 3000.000000);
	  v3f(7925.000000, 4879.000000, 0.000000);
	glEnd();

	// Lateral a la derecha de la puerta:
  glBegin(GL_QUADS); // 6� parte
	  v3f(7925.000000, 4879.000000, 0.000000);
	  v3f(7925.000000, 4879.000000, 3000.000000);
	  v3f(7925.000000, -554.000000, 3000.000000);
	  v3f(7925.000000, -554.000000, 0.000000);
	glEnd();

	// Lateral de la puerta:
  glBegin(GL_QUADS); // 1� parte
	  v3f(7925.000000, -554.000000, 0.000000);
	  v3f(7925.000000, -554.000000, 3000.000000);
	  v3f(6110.000000, -554.000000, 3000.000000);
	  v3f(6110.000000, -554.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 2� parte
	  v3f(6110.000000, -554.000000, 0.000000);
	  v3f(6110.000000, -554.000000, 3000.000000);
	  v3f(6110.000000, -119.000000, 3000.000000);
	  v3f(6110.000000, -119.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 3� parte
	  v3f(6110.000000, -119.000000, 0.000000);
	  v3f(6110.000000, -119.000000, 3000.000000);
	  v3f(5476.000000, -119.000000, 3000.000000);
	  v3f(5476.000000, -119.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 4� parte
	  v3f(5476.000000, -554.000000, 0.000000);
	  v3f(5476.000000, -554.000000, 3000.000000);
	  v3f(1211.000000, -554.000000, 3000.000000);
	  v3f(1211.000000, -554.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 5� parte
	  v3f(1211.000000, -554.000000, 0.000000);
	  v3f(1211.000000, -554.000000, 3000.000000);
	  v3f(1211.000000, 0.000000, 3000.000000);
	  v3f(1211.000000, 0.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 6� parte
	  v3f(1211.000000, 0.000000, 0.000000);
	  v3f(1211.000000, 0.000000, 3000.000000);
	  v3f(0.000000, 0.000000, 3000.000000);
	  v3f(0.000000, 0.000000, 0.000000);
	glEnd();

	/* PUERTA */
  glColor3f(0.67, 0., 0.);
  glBegin(GL_QUADS);
	  v3f(4613.000000, -554.000000, 0.000000);
	  v3f(4613.000000, -554.000000, 2030.000000);
	  v3f(3879.000000, -554.000000, 2030.000000);
	  v3f(3879.000000, -554.000000, 0.000000);
  glEnd();

	/* VENTANA */
  glColor3f(0.5, 0.5, 1.);
  glBegin(GL_QUADS);
	  v3f(2585.000000, 4879.000000, 835.000000);
	  v3f(2585.000000, 4879.000000, 2532.000000);
	  v3f(5335.000000, 4879.000000, 2532.000000);
	  v3f(5335.000000, 4879.000000, 835.000000);
  glEnd();
}

void drawRoboticLabGround () {
  glLineWidth(2.0f);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

	glColor3f (1.,1.,1.);
  glBegin(GL_LINES);
  v3f(0.000000, 0.000000, 0.000000);
  v3f(0.000000, 4589.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(0.000000, 0.000000, 0.000000);
  v3f(1211.000000, 0.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(1211.000000, 0.000000, 0.000000);
  v3f(1211.000000, -554.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(1211.000000, -554.000000, 0.000000);
  v3f(5476.000000, -554.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5476.000000, -554.000000, 0.000000);
  v3f(5476.000000, -119.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5476.000000, -119.000000, 0.000000);
  v3f(6110.000000, -119.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6110.000000, -119.000000, 0.000000);
  v3f(6110.000000, -554.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6110.000000, -554.000000, 0.000000);
  v3f(7925.000000, -554.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7925.000000, -554.000000, 0.000000);
  v3f(7925.000000, 4879.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7925.000000, 4879.000000, 0.000000);
  v3f(6264.000000, 4879.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6264.000000, 4879.000000, 0.000000);
  v3f(6264.000000, 4589.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6264.000000, 4589.000000, 0.000000);
  v3f(5498.000000, 4589.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5498.000000, 4589.000000, 0.000000);
  v3f(5498.000000, 4879.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5498.000000, 4879.000000, 0.000000);
  v3f(2419.000000, 4879.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2419.000000, 4879.000000, 0.000000);
  v3f(2419.000000, 4589.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2419.000000, 4589.000000, 0.000000);
  v3f(0.000000, 4589.000000, 0.000000);
  glEnd();

  glColor3f(1., 1., 1.);
	// LINEAS LIMITROFES DEL CAMPO FUTBOL
  glBegin(GL_LINES);
  v3f(1560.000000, 596.000000, 0.000000);
  v3f(1560.000000, 4589.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5498.000000, 4589.000000, 0.000000);
  v3f(2419.000000, 4589.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(1560.000000, 596.000000, 0.000000);
  v3f(7925.000000, 596.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6264.000000, 4589.000000, 0.000000);
  v3f(7925.000000, 4589.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2160.000000, 796.000000, 0.000000);
  v3f(2160.000000, 4301.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2160.000000, 796.000000, 0.000000);
  v3f(7560.000000, 796.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2160.000000, 4301.000000, 0.000000);
  v3f(7560.000000, 4301.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7560.000000, 796.000000, 0.000000);
  v3f(7560.000000, 4301.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(4860.000000, 796.000000, 0.000000);
  v3f(4860.000000, 4301.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2160.000000, 2051.000000, 0.000000);
  v3f(2810.000000, 2051.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2810.000000, 2051.000000, 0.000000);
  v3f(2810.000000, 3351.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2810.000000, 3351.000000, 0.000000);
  v3f(2160.000000, 3351.000000, 0.000000 );
  glEnd();

  glBegin(GL_LINES);
  v3f(7560.000000, 2051.000000, 0.000000);
  v3f(6910.000000, 2051.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7560.000000, 3351.000000, 0.000000);
  v3f(6910.000000, 3351.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6910.000000, 2051.000000, 0.000000);
  v3f(6910.000000, 3351.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(4500.000000, 2341.000000, 0.000000);
  v3f(4500.000000, 3061.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5220.000000, 2341.000000, 0.000000);
  v3f(5220.000000, 3061.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(4500.000000, 2341.000000, 0.000000);
  v3f(5220.000000, 2341.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(4500.000000, 3061.000000, 0.000000);
  v3f(5220.000000, 3061.000000, 0.000000);
  glEnd();
}

void drawRoboticLabLines () {
  glColor3f(1., 1., 1.);
  glBegin(GL_LINES);
  v3f(0.000000, 0.000000, 3000.000000);
  v3f(0.000000, 4589.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(0.000000, 0.000000, 3000.000000);
  v3f(1211.000000, 0.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(1211.000000, 0.000000, 3000.000000);
  v3f(1211.000000, -554.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(1211.000000, -554.000000, 3000.000000);
  v3f(5476.000000, -554.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5476.000000, -554.000000, 3000.000000);
  v3f(5476.000000, -119.000000, 3000.000000);
  glEnd();
	
  glBegin(GL_LINES);
  v3f(5476.000000, -119.000000, 3000.000000);
  v3f(6110.000000, -119.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6110.000000, -119.000000, 3000.000000);
  v3f(6110.000000, -554.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6110.000000, -554.000000, 3000.000000);
  v3f(7925.000000, -554.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7925.000000, -554.000000, 3000.000000);
  v3f(7925.000000, 4879.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7925.000000, 4879.000000, 3000.000000);
  v3f(6264.000000, 4879.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6264.000000, 4879.000000, 3000.000000);
  v3f(6264.000000, 4589.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6264.000000, 4589.000000, 3000.000000);
  v3f(5498.000000, 4589.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5498.000000, 4589.000000, 3000.000000);
  v3f(5498.000000, 4879.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5498.000000, 4879.000000, 3000.000000);
  v3f(2419.000000, 4879.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2419.000000, 4879.000000, 3000.000000);
  v3f(2419.000000, 4589.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2419.000000, 4589.000000, 3000.000000);
  v3f(0.000000, 4589.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(0.000000, 0.000000, 30.000000);
  v3f(0.000000, 4589.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(0.000000, 0.000000, 30.000000);
  v3f(1211.000000, 0.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(1211.000000, 0.000000, 30.000000);
  v3f(1211.000000, -554.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(1211.000000, -554.000000, 30.000000);
  v3f(5476.000000, -554.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5476.000000, -554.000000, 30.000000);
  v3f(5476.000000, -119.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5476.000000, -119.000000, 30.000000);
  v3f(6110.000000, -119.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6110.000000, -119.000000, 30.000000);
  v3f(6110.000000, -554.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6110.000000, -554.000000, 30.000000);
  v3f(7925.000000, -554.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7925.000000, -554.000000, 30.000000);
  v3f(7925.000000, 4879.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7925.000000, 4879.000000, 30.000000);
  v3f(6264.000000, 4879.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6264.000000, 4879.000000, 30.000000);
  v3f(6264.000000, 4589.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6264.000000, 4589.000000, 30.000000);
  v3f(5498.000000, 4589.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5498.000000, 4589.000000, 30.000000);
  v3f(5498.000000, 4879.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5498.000000, 4879.000000, 30.000000);
  v3f(2419.000000, 4879.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2419.000000, 4879.000000, 30.000000);
  v3f(2419.000000, 4589.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2419.000000, 4589.000000, 30.000000);
  v3f(0.000000, 4589.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(0.000000, 0.000000, 0.000000);
  v3f(0.000000, 0.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(1211.000000, 0.000000, 0.000000);
  v3f(1211.000000, 0.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(1211.000000, -554.000000, 0.000000);
  v3f(1211.000000, -554.000000, 3000.000000);
  glEnd();

	// LINEAS LIMITROFES PUERTA-------------------
  glColor3f(1., 0.5, 0.);
  glBegin(GL_LINES);
  v3f(3879.000000, -554.000000, 0.000000);
  v3f(3879.000000, -554.000000, 2030.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(3879.000000, -554.000000, 2030.000000);
  v3f(4613.000000, -554.000000, 2030.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(4613.000000, -554.000000, 2030.000000);
  v3f(4613.000000, -554.000000, 0.000000);
  glEnd();
	//--------------------------------------------

  glColor3f(1., 1., 1.);
  glBegin(GL_LINES);
  v3f(5476.000000, -554.000000, 0.000000);
  v3f(5476.000000, -554.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5476.000000, -119.000000, 0.000000);
  v3f(5476.000000, -119.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6110.000000, -119.000000, 0.000000);
  v3f(6110.000000, -119.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6110.000000, -554.000000, 0.000000);
  v3f(6110.000000, -554.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7925.000000, -554.000000, 0.000000);
  v3f(7925.000000, -554.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7925.000000, 4879.000000, 0.000000);
  v3f(7925.000000, 4879.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6264.000000, 4879.000000, 0.000000);
  v3f(6264.000000, 4879.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6264.000000, 4589.000000, 0.000000);
  v3f(6264.000000, 4589.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5498.000000, 4589.000000, 0.000000);
  v3f(5498.000000, 4589.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5498.000000, 4879.000000, 0.000000);
  v3f(5498.000000, 4879.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2419.000000, 4879.000000, 0.000000);
  v3f(2419.000000, 4879.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2419.000000, 4589.000000, 0.000000);
  v3f(2419.000000, 4589.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(0.000000, 4589.000000, 0.000000);
  v3f(0.000000, 4589.000000, 3000.000000);
  glEnd();

	// LINEAS LIMITROFES DE LA VENTANA-----------
  glColor3f(1., 0.5, 0.);
  glBegin(GL_LINES);
  v3f(2585.000000, 4879.000000, 835.000000);
  v3f(5335.000000, 4879.000000, 835.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2585.000000, 4879.000000, 2532.000000);
  v3f(5335.000000, 4879.000000, 2532.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2585.000000, 4879.000000, 835.000000);
  v3f(2585.000000, 4879.000000, 2532.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5335.000000, 4879.000000, 835.000000);
  v3f(5335.000000, 4879.000000, 2532.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(3960.000000, 4879.000000, 835.000000);
  v3f(3960.000000, 4879.000000, 2532.000000);
  glEnd();
	//--------------------------------------------

  glColor3f(1., 1., 1.);
	// LINEAS LIMITROFES DEL CAMPO FUTBOL
  glBegin(GL_LINES);
  v3f(1560.000000, 596.000000, 30.000000);
  v3f(1560.000000, 4589.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5498.000000, 4589.000000, 30.000000);
  v3f(2419.000000, 4589.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(1560.000000, 596.000000, 30.000000);
  v3f(7925.000000, 596.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6264.000000, 4589.000000, 30.000000);
  v3f(7925.000000, 4589.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2160.000000, 796.000000, 30.000000);
  v3f(2160.000000, 4301.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2160.000000, 796.000000, 30.000000);
  v3f(7560.000000, 796.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2160.000000, 4301.000000, 30.000000);
  v3f(7560.000000, 4301.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7560.000000, 796.000000, 30.000000);
  v3f(7560.000000, 4301.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(4860.000000, 796.000000, 30.000000);
  v3f(4860.000000, 4301.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2160.000000, 2051.000000, 30.000000);
  v3f(2810.000000, 2051.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2810.000000, 2051.000000, 30.000000);
  v3f(2810.000000, 3351.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2810.000000, 3351.000000, 30.000000);
  v3f(2160.000000, 3351.000000, 30.000000 );
  glEnd();

  glBegin(GL_LINES);
  v3f(7560.000000, 2051.000000, 30.000000);
  v3f(6910.000000, 2051.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7560.000000, 3351.000000, 30.000000);
  v3f(6910.000000, 3351.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6910.000000, 2051.000000, 30.000000);
  v3f(6910.000000, 3351.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(4500.000000, 2341.000000, 30.000000);
  v3f(4500.000000, 3061.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5220.000000, 2341.000000, 30.000000);
  v3f(5220.000000, 3061.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(4500.000000, 2341.000000, 30.000000);
  v3f(5220.000000, 2341.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(4500.000000, 3061.000000, 30.000000);
  v3f(5220.000000, 3061.000000, 30.000000);
  glEnd();
}

/*
void calculateDistances () {
	int i;

	for (i = 0; i < (SIFNTSC_COLUMNS*SIFNTSC_ROWS); i ++) {




}
*/

/* We want to know the intersection between a line given by two points (A and B) on 3D, and a plan (the ground, for example). Then, we'll get another point; it'll be our solution */
int linePlaneIntersection (HPoint3D A, HPoint3D B, HPoint3D *intersectionPoint) {
	HPoint3D v;	// Line director vector: it the same to take A or B as origin or destination extrem...

	v.X = (A.X - B.X);
	v.Y = (A.Y - B.Y);
	v.Z = (A.Z - B.Z);

	// We'll calculate the groun intersection (Z = 0) on our robot system. Parametric equations:
	intersectionPoint->Z = 0; // intersectionPoint->Z = A.Z + t*v.Z => t = (-A.Z / v.Z)
	t = (-A.Z) / (v.Z);

	intersectionPoint->X = A.X + (t*v.X);
	intersectionPoint->Y = A.Y + (t*v.Y);
}

void pointProjection (HPoint2D point, char **myColor) {
	if (myColor == mycolorA)
//		myCamera = &virtualcam2;
		myCamera = &robotCamera;
	else if (myColor == mycolorB)
		myCamera = &virtualcam3;
	else if (myColor == mycolorC)
		myCamera = &virtualcam4;
	else if (myColor == mycolorD)
		myCamera = &virtualcam5;

	// Obtenemos las coordenadas opticas 2D del punto actual de la imagen en curso

	myActualPoint2D.y = GRAPHIC_TO_OPTICAL_Y(point.y,point.x);
	myActualPoint2D.x = GRAPHIC_TO_OPTICAL_X(point.y,point.x);

	myActualPoint2D.y = point.y;
	myActualPoint2D.x = point.x;
	myActualPoint2D.h = 1.;

	// Proyectamos tal punto en 3D sobre el Plano Imagen de nuestra c�mara virtual
//	printf ("point 2D %f, %f\n", myActualPoint2D.x, myActualPoint2D.y);
	backproject(&myActualPoint3D, myActualPoint2D, *myCamera);

	// Coordenadas en 3D de la posicion de la c�mara
	cameraPos3D.X = 0;//-myCamera->position.X;
	cameraPos3D.Y = 0;//-myCamera->position.Y;
	cameraPos3D.Z = 0;//-myCamera->position.Z;
	cameraPos3D.H = 1;

	/*myActualPoint3D.X = -myActualPoint3D.X;
	myActualPoint3D.Y = -myActualPoint3D.Y;
	myActualPoint3D.Z = -myActualPoint3D.Z;
	myActualPoint3D.H = 1.;*/

//	printf ("Puntos a dibujar [%f, %f, %f] - [%f, %f, %f]\n", cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z, myActualPoint3D.X, myActualPoint3D.Y, myActualPoint3D.Z);

	// Distancia entre los puntos myActualPoint3D - cameraPos3D
	distanciaPlanoImagen=(float)sqrt((double)((myActualPoint3D.X-cameraPos3D.X)*(myActualPoint3D.X-cameraPos3D.X)+(myActualPoint3D.Y-cameraPos3D.Y)*(myActualPoint3D.Y-cameraPos3D.Y)+(myActualPoint3D.Z-cameraPos3D.Z)*(myActualPoint3D.Z-cameraPos3D.Z)));
	incremento = ROOM_MAX_X/distanciaPlanoImagen; // distancia "relativa"

	// Alargamos tal distancia (lanzamiento del rayo �ptico)
	myEndPoint3D.X = ((incremento * myActualPoint3D.X + (1. - incremento) * cameraPos3D.X));
	myEndPoint3D.Y = ((incremento * myActualPoint3D.Y + (1. - incremento) * cameraPos3D.Y));
	myEndPoint3D.Z = ((incremento * myActualPoint3D.Z + (1. - incremento) * cameraPos3D.Z));
	myEndPoint3D.H = 1.;

	glColor3f (0.5,0.5,0.5);
  glBegin(GL_LINES);
	  v3f(myActualPoint3D.X, myActualPoint3D.Y, myActualPoint3D.Z);
	  v3f(myEndPoint3D.X, myEndPoint3D.Y, myEndPoint3D.Z);
  glEnd();
	
	linePlaneIntersection (myActualPoint3D, myEndPoint3D, &intersectionPoint);

	if (actualGroundPoint == ((SIFNTSC_COLUMNS*SIFNTSC_ROWS) - 1))
		actualGroundPoint = 0;

	groundPoints[actualGroundPoint].X = intersectionPoint.X;
	groundPoints[actualGroundPoint].Y = intersectionPoint.Y;
	groundPoints[actualGroundPoint].Z = intersectionPoint.Z;

	actualGroundPoint ++;
}

void drawOpticalRays (TPinHoleCamera *cam) {
  HPoint3D a3A;
  HPoint2D a;

	glColor3f( 0.7, 0.7, 0.7 );

	cameraPos3D.X=0.;
	cameraPos3D.Y=0.;
	cameraPos3D.Z=0.;
	cameraPos3D.H=1.;

	/* optical axis of camera */
	//if (((display_state&DISPLAY_COLORIMAGEA)!=0) && (mycolorA!=NULL) && (cam == &robotCamera)) {

		a.x = SIFNTSC_ROWS-1;
		a.y = SIFNTSC_COLUMNS-1;
		a.h = 1.;
		backproject (&a3A, a, *cam);

		distanciaPlanoImagen=(float)sqrt((double)((a3A.X-cameraPos3D.X)*(a3A.X-cameraPos3D.X)+(a3A.Y-cameraPos3D.Y)*(a3A.Y-cameraPos3D.Y)+(a3A.Z-cameraPos3D.Z)*(a3A.Z-cameraPos3D.Z)));
		incremento = ROOM_MAX_X/distanciaPlanoImagen; // distancia "relativa"

		a3A.X = incremento * a3A.X + (1. - incremento) * cameraPos3D.X;
		a3A.Y = incremento * a3A.Y + (1. - incremento) * cameraPos3D.Y;
		a3A.Z = incremento * a3A.Z + (1. - incremento) * cameraPos3D.Z;
		a3A.H = 1.;

		glBegin(GL_LINES);
			v3f( a3A.X, a3A.Y, a3A.Z );
			v3f( cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
		glEnd();

		/**/
		a.x = 0;
		a.y = 0;
		a.h = 1.;
		backproject (&a3A, a, *cam);

		distanciaPlanoImagen=(float)sqrt((double)((a3A.X-cameraPos3D.X)*(a3A.X-cameraPos3D.X)+(a3A.Y-cameraPos3D.Y)*(a3A.Y-cameraPos3D.Y)+(a3A.Z-cameraPos3D.Z)*(a3A.Z-cameraPos3D.Z)));
		incremento = ROOM_MAX_X/distanciaPlanoImagen; // distancia "relativa"

		a3A.X = incremento * a3A.X + (1. - incremento) * cameraPos3D.X;
		a3A.Y = incremento * a3A.Y + (1. - incremento) * cameraPos3D.Y;
		a3A.Z = incremento * a3A.Z + (1. - incremento) * cameraPos3D.Z;
		a3A.H = 1.;

		glBegin(GL_LINES);
			v3f( a3A.X, a3A.Y, a3A.Z );
			v3f( cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
		glEnd();

		/**/
		a.x = SIFNTSC_ROWS-1;
		a.y = 0;
		a.h = 1.;
		backproject (&a3A, a, *cam);

		distanciaPlanoImagen=(float)sqrt((double)((a3A.X-cameraPos3D.X)*(a3A.X-cameraPos3D.X)+(a3A.Y-cameraPos3D.Y)*(a3A.Y-cameraPos3D.Y)+(a3A.Z-cameraPos3D.Z)*(a3A.Z-cameraPos3D.Z)));
		incremento = ROOM_MAX_X/distanciaPlanoImagen; // distancia "relativa"

		a3A.X = incremento * a3A.X + (1. - incremento) * cameraPos3D.X;
		a3A.Y = incremento * a3A.Y + (1. - incremento) * cameraPos3D.Y;
		a3A.Z = incremento * a3A.Z + (1. - incremento) * cameraPos3D.Z;
		a3A.H = 1.;

		glBegin(GL_LINES);
			v3f( a3A.X, a3A.Y, a3A.Z );
			v3f( cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
		glEnd();

		/**/
		a.x = 0;
		a.y = SIFNTSC_COLUMNS-1;
		a.h = 1.;
		backproject (&a3A, a, *cam);

		distanciaPlanoImagen=(float)sqrt((double)((a3A.X-cameraPos3D.X)*(a3A.X-cameraPos3D.X)+(a3A.Y-cameraPos3D.Y)*(a3A.Y-cameraPos3D.Y)+(a3A.Z-cameraPos3D.Z)*(a3A.Z-cameraPos3D.Z)));
		incremento = ROOM_MAX_X/distanciaPlanoImagen; // distancia "relativa"

		a3A.X = incremento * a3A.X + (1. - incremento) * cameraPos3D.X;
		a3A.Y = incremento * a3A.Y + (1. - incremento) * cameraPos3D.Y;
		a3A.Z = incremento * a3A.Z + (1. - incremento) * cameraPos3D.Z;
		a3A.H = 1.;

		glBegin(GL_LINES);
			v3f( a3A.X, a3A.Y, a3A.Z );
			v3f( cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
		glEnd();
	//} // fin if (dibujamos los rayos opticos, si estamos en la c�mara oportuna...)
}

void drawCam(TPinHoleCamera *cam, int b)
{
  HPoint3D a3A, a3B, a3C, a3D;
  HPoint2D a;

/*	if (b==2){
		glColor3f( 0.7, 0.7, 0.7 );
	} else if (b==3){
		glColor3f( 0.7, 0.7, 0.7 );
	} else if (b==4){
		glColor3f( 0.7, 0.7, 0.7 );
	} else if (b==5){
		glColor3f( 0.7, 0.7, 0.7 );
	}
*/

	glColor3f( 0.7, 0.7, 0.7 );

	cameraPos3D.X=-cam->position.X;
	cameraPos3D.Y=-cam->position.Y;
	cameraPos3D.Z=-cam->position.Z;
	cameraPos3D.H=cam->position.H;

	/* optical axis of camera */
	if ((((display_state&DISPLAY_COLORIMAGEA)!=0) && (mycolorA!=NULL) && (cam == &virtualcam2)) ||
			(((display_state&DISPLAY_COLORIMAGEB)!=0) && (mycolorB!=NULL) && (cam == &virtualcam3)) ||
			(((display_state&DISPLAY_COLORIMAGEC)!=0) && (mycolorC!=NULL) && (cam == &virtualcam4)) ||
			(((display_state&DISPLAY_COLORIMAGED)!=0) && (mycolorD!=NULL) && (cam == &virtualcam5))) {

		a.x = SIFNTSC_ROWS-1;
		a.y = SIFNTSC_COLUMNS-1;
		a.h = 1.;
		backproject (&a3A, a, *cam);

		distanciaPlanoImagen=(float)sqrt((double)((a3A.X-cameraPos3D.X)*(a3A.X-cameraPos3D.X)+(a3A.Y-cameraPos3D.Y)*(a3A.Y-cameraPos3D.Y)+(a3A.Z-cameraPos3D.Z)*(a3A.Z-cameraPos3D.Z)));
		incremento = ROOM_MAX_X/distanciaPlanoImagen; // distancia "relativa"

		a3A.X = incremento * a3A.X + (1. - incremento) * cameraPos3D.X;
		a3A.Y = incremento * a3A.Y + (1. - incremento) * cameraPos3D.Y;
		a3A.Z = incremento * a3A.Z + (1. - incremento) * cameraPos3D.Z;
		a3A.H = 1.;

		glBegin(GL_LINES);
			v3f( a3A.X, a3A.Y, a3A.Z );
			v3f( cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
		glEnd();

		/**/
		a.x = 0;
		a.y = 0;
		a.h = 1.;
		backproject (&a3A, a, *cam);

		distanciaPlanoImagen=(float)sqrt((double)((a3A.X-cameraPos3D.X)*(a3A.X-cameraPos3D.X)+(a3A.Y-cameraPos3D.Y)*(a3A.Y-cameraPos3D.Y)+(a3A.Z-cameraPos3D.Z)*(a3A.Z-cameraPos3D.Z)));
		incremento = ROOM_MAX_X/distanciaPlanoImagen; // distancia "relativa"

		a3A.X = incremento * a3A.X + (1. - incremento) * cameraPos3D.X;
		a3A.Y = incremento * a3A.Y + (1. - incremento) * cameraPos3D.Y;
		a3A.Z = incremento * a3A.Z + (1. - incremento) * cameraPos3D.Z;
		a3A.H = 1.;

		glBegin(GL_LINES);
			v3f( a3A.X, a3A.Y, a3A.Z );
			v3f( cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
		glEnd();

		/**/
		a.x = SIFNTSC_ROWS-1;
		a.y = 0;
		a.h = 1.;
		backproject (&a3A, a, *cam);

		distanciaPlanoImagen=(float)sqrt((double)((a3A.X-cameraPos3D.X)*(a3A.X-cameraPos3D.X)+(a3A.Y-cameraPos3D.Y)*(a3A.Y-cameraPos3D.Y)+(a3A.Z-cameraPos3D.Z)*(a3A.Z-cameraPos3D.Z)));
		incremento = ROOM_MAX_X/distanciaPlanoImagen; // distancia "relativa"

		a3A.X = incremento * a3A.X + (1. - incremento) * cameraPos3D.X;
		a3A.Y = incremento * a3A.Y + (1. - incremento) * cameraPos3D.Y;
		a3A.Z = incremento * a3A.Z + (1. - incremento) * cameraPos3D.Z;
		a3A.H = 1.;

		glBegin(GL_LINES);
			v3f( a3A.X, a3A.Y, a3A.Z );
			v3f( cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
		glEnd();

		/**/
		a.x = 0;
		a.y = SIFNTSC_COLUMNS-1;
		a.h = 1.;
		backproject (&a3A, a, *cam);

		distanciaPlanoImagen=(float)sqrt((double)((a3A.X-cameraPos3D.X)*(a3A.X-cameraPos3D.X)+(a3A.Y-cameraPos3D.Y)*(a3A.Y-cameraPos3D.Y)+(a3A.Z-cameraPos3D.Z)*(a3A.Z-cameraPos3D.Z)));
		incremento = ROOM_MAX_X/distanciaPlanoImagen; // distancia "relativa"

		a3A.X = incremento * a3A.X + (1. - incremento) * cameraPos3D.X;
		a3A.Y = incremento * a3A.Y + (1. - incremento) * cameraPos3D.Y;
		a3A.Z = incremento * a3A.Z + (1. - incremento) * cameraPos3D.Z;
		a3A.H = 1.;

		glBegin(GL_LINES);
			v3f( a3A.X, a3A.Y, a3A.Z );
			v3f( cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
		glEnd();
	} // fin if (dibujamos los rayos opticos, si estamos en la c�mara oportuna...)

	/* FieldOfView of camera */
	a.x = SIFNTSC_ROWS - 1.;
	a.y = SIFNTSC_COLUMNS - 1.;
	a.h = 1.;
	backproject(&a3A, a, *cam);
	a.x = 0.;
	a.y = SIFNTSC_COLUMNS - 1.;
	a.h = 1.;
	backproject(&a3B, a, *cam);
	a.x = 0.;
	a.y = 0.;
	a.h = 1.;
	backproject(&a3C, a, *cam);
	a.x = SIFNTSC_ROWS - 1.;
	a.y = 0.;
	a.h = 1.;
	backproject(&a3D, a, *cam);

	// first triangle
	glBegin(GL_TRIANGLES);
		glVertex3f(cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
		glVertex3f(a3A.X, a3A.Y, a3A.Z );
		glVertex3f(a3B.X, a3B.Y, a3B.Z );
	glEnd();

	glColor3f( 0.3, 0.3, 0.3 );
	// second triangle
	glBegin(GL_TRIANGLES);
		glVertex3f(cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
		glVertex3f(a3B.X, a3B.Y, a3B.Z );
		glVertex3f(a3C.X, a3C.Y, a3C.Z );
	glEnd();

	glColor3f( 0.7, 0.7, 0.7 );
	// third triangle
	glBegin(GL_TRIANGLES);
		glVertex3f(cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
		glVertex3f(a3C.X, a3C.Y, a3C.Z );
		glVertex3f(a3D.X, a3D.Y, a3D.Z );
	glEnd();

	glColor3f( 0.3, 0.3, 0.3 );
	// last triangle
	glBegin(GL_TRIANGLES);
		glVertex3f(cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
		glVertex3f(a3D.X, a3D.Y, a3D.Z );
		glVertex3f(a3A.X, a3A.Y, a3A.Z );
	glEnd();

	glColor3f( 0.1, 0.1, 0.1 );
	// square
	glBegin(GL_POLYGON);
		glVertex3f(a3A.X, a3A.Y, a3A.Z );
		glVertex3f(a3D.X, a3D.Y, a3D.Z );
		glVertex3f(a3C.X, a3C.Y, a3C.Z );
		glVertex3f(a3B.X, a3B.Y, a3B.Z );
	glEnd();
}

void frontera_iteration()
{
	speedcounter(frontera_id); 
  static int key_barrido=puntos_clave-1;
  static int direccion=IZQ;
}

void frontera_suspend()
{
  if ((display_state & DISPLAY_COLORIMAGEA)!=0)
     colorAsuspend();
  if ((display_state & DISPLAY_COLORIMAGEB)!=0)
     colorBsuspend();
  if ((display_state & DISPLAY_COLORIMAGEC)!=0)
     colorCsuspend();
  if ((display_state & DISPLAY_COLORIMAGED)!=0)
     colorDsuspend();

	RGB2HSV_destroyTable();

  pthread_mutex_lock(&(all[frontera_id].mymutex));
  put_state(frontera_id,slept);
  printf("frontera: off\n");
  pthread_mutex_unlock(&(all[frontera_id].mymutex));
}

void frontera_resume(int father, int *brothers, arbitration fn)
{
	int i;

  pthread_mutex_lock(&(all[frontera_id].mymutex));
  /* this schema resumes its execution with no children at all */
  for(i=0;i<MAX_SCHEMAS;i++) all[frontera_id].children[i]=FALSE;
 
  all[frontera_id].father=father;
  if (brothers!=NULL)
    {
      for(i=0;i<MAX_SCHEMAS;i++) frontera_brothers[i]=-1;
      i=0;
      while(brothers[i]!=-1) {frontera_brothers[i]=brothers[i];i++;}
    }
  frontera_callforarbitration=fn;
  put_state(frontera_id,notready);

  printf("frontera: on\n");
  pthread_cond_signal(&(all[frontera_id].condition));
  pthread_mutex_unlock(&(all[frontera_id].mymutex));
}

void *frontera_thread(void *not_used) 
{
  struct timeval a,b;
  long diff, next;

  for(;;)
    {
      pthread_mutex_lock(&(all[frontera_id].mymutex));

      if (all[frontera_id].state==slept) 
	{
	  pthread_cond_wait(&(all[frontera_id].condition),&(all[frontera_id].mymutex));
	  pthread_mutex_unlock(&(all[frontera_id].mymutex));
	}
      else 
	{
	  /* check preconditions. For now, preconditions are always satisfied*/
	  if (all[frontera_id].state==notready) put_state(frontera_id,ready);
	  else if (all[frontera_id].state==ready)	  /* check brothers and arbitrate. For now this is the only winner */
	    { 
	      put_state(frontera_id,winner);
	    }	  
	  else if (all[frontera_id].state==winner);

	  if (all[frontera_id].state==winner)
	    /* I'm the winner and must execute my iteration */
	    {
	      pthread_mutex_unlock(&(all[frontera_id].mymutex));
	      gettimeofday(&a,NULL);
	      frontera_iteration();
	      gettimeofday(&b,NULL);  

	      diff = (b.tv_sec-a.tv_sec)*1000000+b.tv_usec-a.tv_usec;
	      next = frontera_cycle*1000-diff-10000; 
	      if (next>0) 
		/* discounts 10ms taken by calling usleep itself */
		usleep(frontera_cycle*1000-diff);
	      else 
		/* just let this iteration go away. overhead time negligible */
		{printf("time interval violated: frontera\n"); 
		usleep(frontera_cycle*1000);
		}
	    }
	  else 
	    {
	      pthread_mutex_unlock(&(all[frontera_id].mymutex));
	      usleep(frontera_cycle*1000);
	    }
	}
    }
}

void frontera_close()
{
  pthread_mutex_lock(&(all[frontera_id].mymutex));
  frontera_suspend();  
  pthread_mutex_unlock(&(all[frontera_id].mymutex));
  sleep(2);
  fl_free_form(fd_fronteragui->fronteragui);
  free(imagenA_buf);
  free(imagenB_buf);
  free(imagenC_buf);
  free(imagenD_buf);
  free(imagenFiltrada_buf);
  free(imagenSuelo_buf);
}

static int InitOGL(FL_OBJECT *ob, Window win,int w,int h, XEvent *xev, void *ud)
 /* Inicializa OpenGL con los parametros que diran como se visualiza. */
{
   GLfloat ambient[] = {1.0, 1.0, 1.0, 1.0};
   GLfloat diffuse[] = {1.0, 1.0, 1.0, 1.0};
   GLfloat position[] = {0.0, 3.0, 3.0, 0.0};
   GLfloat lmodel_ambient[] = {0.2, 0.2, 0.2, 1.0};
   GLfloat local_view[] = {0.0};
 
   glViewport(0,0,(GLint)w,(GLint)h);  
   /*  glDrawBuffer(GL_FRONT);*/
   glDrawBuffer(GL_BACK);
   glClearColor(0.6f, 0.8f, 1.0f, 0.0f);  
   glClearDepth(1.0);                           /* Enables Clearing Of The Depth Buffer */
   /* This Will Clear The Background Color To Light Blue */
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
   /* With this, the pioneer appears correctly, but the cubes don't */
   glLightfv (GL_LIGHT0, GL_AMBIENT, ambient);
   glLightfv (GL_LIGHT0, GL_DIFFUSE, diffuse);
   glLightfv (GL_LIGHT0, GL_POSITION, position);
   glLightModelfv (GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);
   glLightModelfv (GL_LIGHT_MODEL_LOCAL_VIEWER, local_view);
   glEnable (GL_LIGHT0);
   /*glEnable (GL_LIGHTING);*/
   
   glEnable(GL_TEXTURE_2D);     /* Enable Texture Mapping */
   glEnable (GL_AUTO_NORMAL);
   glEnable (GL_NORMALIZE);  
   glEnable(GL_DEPTH_TEST);     /* Enables Depth Testing */
   glDepthFunc(GL_LESS);  
   glShadeModel(GL_SMOOTH);     /* Enables Smooth Color Shading */
   glEnable(GL_BLEND);
   glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

   return 0;
}

static int InitMiniOGL(FL_OBJECT *ob, Window win,int w,int h, XEvent *xev, void *ud)
 /* Inicializa OpenGL con los parametros que diran como se visualiza. */
{
   GLfloat ambient[] = {1.0, 1.0, 1.0, 1.0};
   GLfloat diffuse[] = {1.0, 1.0, 1.0, 1.0};
   GLfloat position[] = {0.0, 3.0, 3.0, 0.0};
   GLfloat lmodel_ambient[] = {0.2, 0.2, 0.2, 1.0};
   GLfloat local_view[] = {0.0};
 
   glViewport(0,0,(GLint)w,(GLint)h);  
   /*  glDrawBuffer(GL_FRONT);*/
   glDrawBuffer(GL_BACK);
   glClearColor(0.f, 0.8f, 0.5f, 0.0f);  
   glClearDepth(1.0);                           /* Enables Clearing Of The Depth Buffer */
   /* This Will Clear The Background Color To Light Blue */
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
   /* With this, the pioneer appears correctly, but the cubes don't */
   glLightfv (GL_LIGHT0, GL_AMBIENT, ambient);
   glLightfv (GL_LIGHT0, GL_DIFFUSE, diffuse);
   glLightfv (GL_LIGHT0, GL_POSITION, position);
   glLightModelfv (GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);
   glLightModelfv (GL_LIGHT_MODEL_LOCAL_VIEWER, local_view);
   glEnable (GL_LIGHT0);
   /*glEnable (GL_LIGHTING);*/
   
   glEnable(GL_TEXTURE_2D);     /* Enable Texture Mapping */
   glEnable (GL_AUTO_NORMAL);
   glEnable (GL_NORMALIZE);  
   glEnable(GL_DEPTH_TEST);     /* Enables Depth Testing */
   glDepthFunc(GL_LESS);  
   glShadeModel(GL_SMOOTH);     /* Enables Smooth Color Shading */
   glEnable(GL_BLEND);
   glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

   return 0;
}

static int image_displaysetup() {
	/* Inicializa las ventanas, la paleta de colores y memoria compartida para visualizacion*/ 
	XGCValues gc_values;
	XWindowAttributes win_attributes;
	XColor nuevocolor;
	int pixelNum, numCols;
	int allocated_colors=0, non_allocated_colors=0;

	fronteragui_win= FL_ObjWin(fd_fronteragui->ventanaA);
	XGetWindowAttributes(mydisplay, fronteragui_win, &win_attributes);
	XMapWindow(mydisplay, fronteragui_win);
	/*XSelectInput(mydisplay, fronteragui_win, ButtonPress|StructureNotifyMask);*/   
	gc_values.graphics_exposures = False;
	fronteragui_gc = XCreateGC(mydisplay, fronteragui_win, GCGraphicsExposures, &gc_values);  

	myHSV = (struct HSV*)malloc(sizeof(struct HSV));

	imagenA_buf = (char *) malloc(SIFNTSC_COLUMNS*SIFNTSC_ROWS);
	imagenA = XCreateImage(mydisplay,DefaultVisual(mydisplay,*myscreen),24, ZPixmap,0,imagenA_buf,SIFNTSC_COLUMNS/2, SIFNTSC_ROWS/2,8,0);
	imagenB_buf = (char *) malloc(SIFNTSC_COLUMNS*SIFNTSC_ROWS);
	imagenB = XCreateImage(mydisplay,DefaultVisual(mydisplay,*myscreen),24, ZPixmap,0,imagenB_buf,SIFNTSC_COLUMNS/2, SIFNTSC_ROWS/2,8,0);
	imagenC_buf = (char *) malloc(SIFNTSC_COLUMNS*SIFNTSC_ROWS); 
	imagenC = XCreateImage(mydisplay,DefaultVisual(mydisplay,*myscreen),24, ZPixmap,0,imagenC_buf,SIFNTSC_COLUMNS/2, SIFNTSC_ROWS/2,8,0);
	imagenD_buf = (char *) malloc(SIFNTSC_COLUMNS*SIFNTSC_ROWS); 
	imagenD = XCreateImage(mydisplay,DefaultVisual(mydisplay,*myscreen),24, ZPixmap,0,imagenD_buf,SIFNTSC_COLUMNS/2, SIFNTSC_ROWS/2,8,0);
	imagenFiltrada_buf = (char *) malloc(SIFNTSC_COLUMNS*SIFNTSC_ROWS*4);
	imagenFiltrada = XCreateImage(mydisplay,DefaultVisual(mydisplay,*myscreen),24, ZPixmap,0,imagenFiltrada_buf,SIFNTSC_COLUMNS, SIFNTSC_ROWS,8,0);
	imagenSuelo_buf = (char *) malloc(SIFNTSC_COLUMNS*SIFNTSC_ROWS*4);
	imagenSuelo = XCreateImage(mydisplay,DefaultVisual(mydisplay,*myscreen),24, ZPixmap,0,imagenSuelo_buf,SIFNTSC_COLUMNS, SIFNTSC_ROWS,8,0);
	return win_attributes.depth;
}

void initConfiguration () {
  float r,lati,longi;

	if (!flashImage) {
	  if (foa_mode){
	    longi=2*PI*mouse_on_canvas.x/360.;
	    lati=2*PI*mouse_on_canvas.y/360.;
	    r=5000;
	    virtualcam1.foa.X=r*cos(lati)*cos(longi);
	    virtualcam1.foa.Y=r*cos(lati)*sin(longi);
	    virtualcam1.foa.Z=r*sin(lati);
	  }
	  
	  if (cam_mode) {
	    longi=2*PI*mouse_on_canvas.x/360.;
	    lati=2*PI*mouse_on_canvas.y/360.;
	    
	    virtualcam1.position.X=radius*cos(lati)*cos(longi);
	    virtualcam1.position.Y=radius*cos(lati)*sin(longi);
	    virtualcam1.position.Z=radius*sin(lati);
	    
	    virtualcam1.foa.X=0;
	    virtualcam1.foa.Y=0;
	    virtualcam1.foa.Z=0;
	  }
	}
  
  fl_activate_glcanvas(fd_fronteragui->canvas);
  /* Set the OpenGL state machine to the right context for this display */
  /* reset of the depth and color buffers */
  InitOGL(fd_fronteragui->canvas, FL_ObjWin(fd_fronteragui->canvas),fd_fronteragui->canvas->w,fd_fronteragui->canvas->h,NULL,NULL);
    
  /* Virtual camera */
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity(); 

  /* perspective projection. intrinsic parameters + frustrum */
  gluPerspective(45.,(GLfloat)640/(GLfloat)480,1.0,50000.0);
  /* extrinsic parameters */
  gluLookAt(virtualcam1.position.X,virtualcam1.position.Y,virtualcam1.position.Z,virtualcam1.foa.X,virtualcam1.foa.Y,virtualcam1.foa.Z,0.,0.,1.);

	glEnable (GL_CULL_FACE); // solo veremos una cara de los planos, para el "efecto transparencia"
	glCullFace (GL_FRONT);
}

void initMiniCanvasSettings () {
  float r,lati,longi;

	if (!flashImage) {
	  if (foa_mode) {
	    longi=2*PI*mouse_on_canvas.x/360.;
	    lati=2*PI*mouse_on_canvas.y/360.;
	    r=5000;
	    virtualcam0.foa.X=r*cos(lati)*cos(longi);
	    virtualcam0.foa.Y=r*cos(lati)*sin(longi);
	    virtualcam0.foa.Z=r*sin(lati);
	  }
	  
	  if (cam_mode) {
	    longi=2*PI*mouse_on_canvas.x/360.;
	    lati=2*PI*mouse_on_canvas.y/360.;
	    
	    virtualcam0.position.X=radius*cos(lati)*cos(longi);
	    virtualcam0.position.Y=radius*cos(lati)*sin(longi);
	    virtualcam0.position.Z=radius*sin(lati);
	    
	    virtualcam0.foa.X=0;
	    virtualcam0.foa.Y=0;
	    virtualcam0.foa.Z=0;
	  }
	}

	/* minicanvas settings */
  fl_activate_glcanvas(fd_fronteragui->minicanvas);
  InitMiniOGL(fd_fronteragui->minicanvas, FL_ObjWin(fd_fronteragui->minicanvas),fd_fronteragui->minicanvas->w,fd_fronteragui->minicanvas->h,NULL,NULL);
    
  /* Virtual camera */
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity(); 

  /* perspective projection. intrinsic parameters + frustrum */
  gluPerspective(45.,(GLfloat)640/(GLfloat)480,1.0,50000.0);
  /* extrinsic parameters */
  gluLookAt(virtualcam0.position.X,virtualcam0.position.Y,virtualcam0.position.Z,virtualcam0.foa.X,virtualcam0.foa.Y,virtualcam0.foa.Z,0.,0.,1.);
}

void drawFilteredImage () {
	int i,c,row,j,k;
  double H, S, V;
  float myR, myG, myB;

  /* FilteredImage display */
	for (i=0, k=0/*(SIFNTSC_ROWS*SIFNTSC_COLUMNS)-1*/; i<SIFNTSC_ROWS*SIFNTSC_COLUMNS; i++, k++) {
		c=i%(SIFNTSC_COLUMNS);
		row=i/(SIFNTSC_COLUMNS);
		j=row*SIFNTSC_COLUMNS+c;

    if (((display_state&DISPLAY_COLORIMAGEA)!=0) && (mycolorA!=NULL)) {
			imagenFiltrada_buf[k*4] = (*mycolorA)[j*3];
			imagenFiltrada_buf[k*4+1] = (*mycolorA)[j*3+1];
			imagenFiltrada_buf[k*4+2] = (*mycolorA)[j*3+2];
		} else if (((display_state&DISPLAY_COLORIMAGEB)!=0) && (mycolorB!=NULL)) {
			imagenFiltrada_buf[k*4] = (*mycolorB)[j*3];
			imagenFiltrada_buf[k*4+1] = (*mycolorB)[j*3+1];
			imagenFiltrada_buf[k*4+2] = (*mycolorB)[j*3+2];
		} else if (((display_state&DISPLAY_COLORIMAGEC)!=0) && (mycolorC!=NULL)) {
			imagenFiltrada_buf[k*4] = (*mycolorC)[j*3];
			imagenFiltrada_buf[k*4+1] = (*mycolorC)[j*3+1];
			imagenFiltrada_buf[k*4+2] = (*mycolorC)[j*3+2];
		} else if (((display_state&DISPLAY_COLORIMAGED)!=0) && (mycolorD!=NULL)) {
			imagenFiltrada_buf[k*4] = (*mycolorD)[j*3];
			imagenFiltrada_buf[k*4+1] = (*mycolorD)[j*3+1];
			imagenFiltrada_buf[k*4+2] = (*mycolorD)[j*3+2];
    } else { // no recibimos se�al
			imagenFiltrada_buf[k*4]=0;
			imagenFiltrada_buf[k*4+1]=0;
			imagenFiltrada_buf[k*4+2]=0;
    }
		imagenFiltrada_buf[k*4+3]=0; // dummy

		myR = (float)(unsigned int)(unsigned char)imagenFiltrada_buf[k*4+2];
		myG = (float)(unsigned int)(unsigned char)imagenFiltrada_buf[k*4+1];
		myB = (float)(unsigned int)(unsigned char)imagenFiltrada_buf[k*4+0];

		myHSV = (struct HSV*) RGB2HSV_getHSV ((int)myR,(int)myG,(int)myB); // pasamos de RGB -> HSV

		if (!((myHSV->H*DEGTORAD > 1.7017) && (myHSV->H*DEGTORAD < 2.5394) &&
			(myHSV->S > 0.31) && (myHSV->S < 0.7) &&
			(myHSV->V > 77.6) && (myHSV->V < 207.2))) { // si no pasamos el filtro de campo f�tbol
			imagenFiltrada_buf[k*4]=0;
			imagenFiltrada_buf[k*4+1]=0;
			imagenFiltrada_buf[k*4+2]=0;
		}
	}
}

void drawImageA () {
	int i,c,row,j,k;

  /* color imageA display */
  if ((display_state&DISPLAY_COLORIMAGEA)!=0){
    for(i=0, k=0/*(SIFNTSC_ROWS*SIFNTSC_COLUMNS/4)-1*/; i<SIFNTSC_ROWS*SIFNTSC_COLUMNS/4; i++, k++){
      c=i%(SIFNTSC_COLUMNS/2);
      row=i/(SIFNTSC_COLUMNS/2);
      j=2*row*SIFNTSC_COLUMNS+2*c;
      if (mycolorA!=NULL){
        imagenA_buf[k*4]=(*mycolorA)[j*3];
        imagenA_buf[k*4+1]=(*mycolorA)[j*3+1];
        imagenA_buf[k*4+2]=(*mycolorA)[j*3+2];
      }
      else{
        imagenA_buf[k*4]=0;
        imagenA_buf[k*4+1]=0;
        imagenA_buf[k*4+2]=0;
      }
      imagenA_buf[k*4+3]=UCHAR_MAX;
    }
	}
}

void drawImageB () {
	int i,c,row,j,k;

  /* color imageB display */
  if ((display_state&DISPLAY_COLORIMAGEB)!=0){
    for(i=0, k=(SIFNTSC_ROWS*SIFNTSC_COLUMNS/4)-1; i<SIFNTSC_ROWS*SIFNTSC_COLUMNS/4; i++, k--){
        c=i%(SIFNTSC_COLUMNS/2);
        row=i/(SIFNTSC_COLUMNS/2);
        j=2*row*SIFNTSC_COLUMNS+2*c;
        if (mycolorB!=NULL){
          imagenB_buf[k*4]=(*mycolorB)[j*3]; /* Blue Byte */
          imagenB_buf[k*4+1]=(*mycolorB)[j*3+1]; /* Green Byte */
          imagenB_buf[k*4+2]=(*mycolorB)[j*3+2]; /* Red Byte */
        }
        else{
          imagenB_buf[k*4]=0; /* Blue Byte */
          imagenB_buf[k*4+1]=0; /* Green Byte */
          imagenB_buf[k*4+2]=0; /* Red Byte */
        }
        imagenB_buf[k*4+3]=UCHAR_MAX; /* alpha byte */
    }
  }
}

void drawImageC () {
	int i,c,row,j,k;

  /* color imageC display */
  if ((display_state&DISPLAY_COLORIMAGEC)!=0){
    for(i=0, k=(SIFNTSC_ROWS*SIFNTSC_COLUMNS/4)-1; i<SIFNTSC_ROWS*SIFNTSC_COLUMNS/4; i++, k--){
        c=i%(SIFNTSC_COLUMNS/2);
        row=i/(SIFNTSC_COLUMNS/2);
        j=2*row*SIFNTSC_COLUMNS+2*c;
        if (mycolorC!=NULL){
          imagenC_buf[k*4]=(*mycolorC)[j*3]; /* Blue Byte */
          imagenC_buf[k*4+1]=(*mycolorC)[j*3+1]; /* Green Byte */
          imagenC_buf[k*4+2]=(*mycolorC)[j*3+2]; /* Red Byte */
        }
        else{
          imagenC_buf[k*4]=0; /* Blue Byte */
          imagenC_buf[k*4+1]=0; /* Green Byte */
          imagenC_buf[k*4+2]=0; /* Red Byte */
        }
        imagenC_buf[k*4+3]=0; /* dummy byte */
    }
  }
}

void drawImageD () {
	int i,c,row,j,k;

  /* color imageD display */
  if ((display_state&DISPLAY_COLORIMAGED)!=0){
    for(i=0, k=(SIFNTSC_ROWS*SIFNTSC_COLUMNS/4)-1; i<SIFNTSC_ROWS*SIFNTSC_COLUMNS/4; i++, k--){
        c=i%(SIFNTSC_COLUMNS/2);
        row=i/(SIFNTSC_COLUMNS/2);
        j=2*row*SIFNTSC_COLUMNS+2*c;
        if (mycolorD!=NULL) {
	        imagenD_buf[k*4]=(*mycolorD)[j*3]; /* Blue Byte */
	        imagenD_buf[k*4+1]=(*mycolorD)[j*3+1]; /* Green Byte */
	        imagenD_buf[k*4+2]=(*mycolorD)[j*3+2]; /* Red Byte */
				}
				else {
          imagenD_buf[k*4]=0; /* Blue Byte */
          imagenD_buf[k*4+1]=0; /* Green Byte */
          imagenD_buf[k*4+2]=0; /* Red Byte */
        }
        imagenD_buf[k*4+3]=0; /* dummy byte */
		}
  }
}

void drawGroundImage () {
  int i, k, c, row, pos, columnPoint, rowPoint, posPoint;

  double H, S, V;
  float myR, myG, myB;

	int esFrontera;
	int v1, v2, v3, v4, v5, v6, v7, v8;

	/* Ground image */
	for (i=0, k=0/*k=(SIFNTSC_ROWS*SIFNTSC_COLUMNS)-1*/; i<SIFNTSC_ROWS*SIFNTSC_COLUMNS; i++, k++) {
		if ((((display_state&DISPLAY_COLORIMAGEA)!=0) && (mycolorA!=NULL)) ||
				(((display_state&DISPLAY_COLORIMAGEB)!=0) && (mycolorB!=NULL)) ||
				(((display_state&DISPLAY_COLORIMAGEC)!=0) && (mycolorC!=NULL)) ||
				(((display_state&DISPLAY_COLORIMAGED)!=0) && (mycolorD!=NULL))) {
			if (((display_state&DISPLAY_COLORIMAGEA)!=0) && (mycolorA!=NULL)) {
				myActualCamera = mycolorA;
			} else if (((display_state&DISPLAY_COLORIMAGEB)!=0) && (mycolorB!=NULL)) {
				myActualCamera = mycolorB;
			} else if (((display_state&DISPLAY_COLORIMAGEC)!=0) && (mycolorC!=NULL)) {
				myActualCamera = mycolorC;
			} else if (((display_state&DISPLAY_COLORIMAGED)!=0) && (mycolorD!=NULL)) {
				myActualCamera = mycolorD;
			}

			columnPoint = i%(SIFNTSC_COLUMNS);
			rowPoint = i/(SIFNTSC_COLUMNS);
			pos = rowPoint*SIFNTSC_COLUMNS+columnPoint;

			myR = (float)(unsigned int)(unsigned char)(*myActualCamera)[pos*3+2];
			myG = (float)(unsigned int)(unsigned char)(*myActualCamera)[pos*3+1];
			myB = (float)(unsigned int)(unsigned char)(*myActualCamera)[pos*3+0];

			myHSV = (struct HSV*) RGB2HSV_getHSV ((int)myR,(int)myG,(int)myB); // pasamos de RGB -> HSV

			if (!((myHSV->H*DEGTORAD > 1.7017) && (myHSV->H*DEGTORAD < 2.5394) &&
				(myHSV->S > 0.31) && (myHSV->S < 0.7) &&
				(myHSV->V > 77.6) && (myHSV->V < 207.2))) { // si soy negro

				esFrontera = TRUE; // lo damos por frontera en un principio, luego veremos

				// Calculo los dem�s vecinos, para ver de qu� color son...
				c = (i-(SIFNTSC_COLUMNS))%(SIFNTSC_COLUMNS);
				row=(i-(SIFNTSC_COLUMNS))/(SIFNTSC_COLUMNS);
				v1=row*SIFNTSC_COLUMNS+c; // vecinito 1

				if (!((c >= 0) && (c < SIFNTSC_COLUMNS) && 
					 (row >= 0) && (row < SIFNTSC_ROWS))) { // si no es v�lido ponemos sus valores a 0
					myR = 0.;
					myG = 0.;
					myB = 0.;
				} else {
					myR = (float)(unsigned int)(unsigned char)(*myActualCamera)[v1*3+2];
					myG = (float)(unsigned int)(unsigned char)(*myActualCamera)[v1*3+1];
					myB = (float)(unsigned int)(unsigned char)(*myActualCamera)[v1*3+0];
				}

				myHSV = (struct HSV*) RGB2HSV_getHSV ((int)myR,(int)myG,(int)myB); // pasamos de RGB -> HSV

				if (!((myHSV->H*DEGTORAD > 1.7017) && (myHSV->H*DEGTORAD < 2.5394) &&
					(myHSV->S > 0.31) && (myHSV->S < 0.7) &&
					(myHSV->V > 77.6) && (myHSV->V < 207.2))) { // si no soy color campo, sigo comprobando

					c = (i+(SIFNTSC_COLUMNS))%(SIFNTSC_COLUMNS);
					row=(i+(SIFNTSC_COLUMNS))/(SIFNTSC_COLUMNS);
					v2=row*SIFNTSC_COLUMNS+c; // vecinito 2

					if (!((c >= 0) && (c < SIFNTSC_COLUMNS) && 
						 (row >= 0) && (row < SIFNTSC_ROWS))) { // si no es v�lido ponemos sus valores a 0
						myR = 0.;
						myG = 0.;
						myB = 0.;
					} else {
						myR = (float)(unsigned int)(unsigned char)(*myActualCamera)[v2*3+2];
						myG = (float)(unsigned int)(unsigned char)(*myActualCamera)[v2*3+1];
						myB = (float)(unsigned int)(unsigned char)(*myActualCamera)[v2*3+0];
					}

					myHSV = (struct HSV*) RGB2HSV_getHSV ((int)myR,(int)myG,(int)myB); // pasamos de RGB -> HSV

					if (!((myHSV->H*DEGTORAD > 1.7017) && (myHSV->H*DEGTORAD < 2.5394) &&
						(myHSV->S > 0.31) && (myHSV->S < 0.7) &&
						(myHSV->V > 77.6) && (myHSV->V < 207.2))) { // si no soy color campo, sigo comprobando

						c = (i-1)%(SIFNTSC_COLUMNS);
						row=(i-1)/(SIFNTSC_COLUMNS);
						v3=row*SIFNTSC_COLUMNS+c; // vecinito 3

						if (!((c >= 0) && (c < SIFNTSC_COLUMNS) && 
							 (row >= 0) && (row < SIFNTSC_ROWS))) { // si no es v�lido ponemos sus valores a 0
							myR = 0.;
							myG = 0.;
							myB = 0.;
						} else {
							myR = (float)(unsigned int)(unsigned char)(*myActualCamera)[v3*3+2];
							myG = (float)(unsigned int)(unsigned char)(*myActualCamera)[v3*3+1];
							myB = (float)(unsigned int)(unsigned char)(*myActualCamera)[v3*3+0];
						}

						myHSV = (struct HSV*) RGB2HSV_getHSV ((int)myR,(int)myG,(int)myB); // pasamos de RGB -> HSV

						if (!((myHSV->H*DEGTORAD > 1.7017) && (myHSV->H*DEGTORAD < 2.5394) &&
							(myHSV->S > 0.31) && (myHSV->S < 0.7) &&
							(myHSV->V > 77.6) && (myHSV->V < 207.2))) { // si no soy color campo, sigo comprobando

							c = ((i-1)-(SIFNTSC_COLUMNS))%(SIFNTSC_COLUMNS);
							row=((i-1)-(SIFNTSC_COLUMNS))/(SIFNTSC_COLUMNS);
							v4=row*SIFNTSC_COLUMNS+c; // vecinito 4

							if (!((c >= 0) && (c < SIFNTSC_COLUMNS) && 
								 (row >= 0) && (row < SIFNTSC_ROWS))) { // si no es v�lido ponemos sus valores a 0
								myR = 0.;
								myG = 0.;
								myB = 0.;
							} else {
								myR = (float)(unsigned int)(unsigned char)(*myActualCamera)[v4*3+2];
								myG = (float)(unsigned int)(unsigned char)(*myActualCamera)[v4*3+1];
								myB = (float)(unsigned int)(unsigned char)(*myActualCamera)[v4*3+0];
							}

							myHSV = (struct HSV*) RGB2HSV_getHSV ((int)myR,(int)myG,(int)myB); // pasamos de RGB -> HSV

							if (!((myHSV->H*DEGTORAD > 1.7017) && (myHSV->H*DEGTORAD < 2.5394) &&
								(myHSV->S > 0.31) && (myHSV->S < 0.7) &&
								(myHSV->V > 77.6) && (myHSV->V < 207.2))) { // si no soy color campo, sigo comprobando

								c = ((i-1)+(SIFNTSC_COLUMNS))%(SIFNTSC_COLUMNS);
								row=((i-1)+(SIFNTSC_COLUMNS))/(SIFNTSC_COLUMNS);
								v5=row*SIFNTSC_COLUMNS+c; // vecinito 5

								if (!((c >= 0) && (c < SIFNTSC_COLUMNS) && 
									 (row >= 0) && (row < SIFNTSC_ROWS))) { // si no es v�lido ponemos sus valores a 0
									myR = 0.;
									myG = 0.;
									myB = 0.;
								} else {
									myR = (float)(unsigned int)(unsigned char)(*myActualCamera)[v5*3+2];
									myG = (float)(unsigned int)(unsigned char)(*myActualCamera)[v5*3+1];
									myB = (float)(unsigned int)(unsigned char)(*myActualCamera)[v5*3+0];
								}

								myHSV = (struct HSV*) RGB2HSV_getHSV ((int)myR,(int)myG,(int)myB); // pasamos de RGB -> HSV

								if (!((myHSV->H*DEGTORAD > 1.7017) && (myHSV->H*DEGTORAD < 2.5394) &&
									(myHSV->S > 0.31) && (myHSV->S < 0.7) &&
									(myHSV->V > 77.6) && (myHSV->V < 207.2))) { // si no soy color campo, sigo comprobando

									c = (i+1)%(SIFNTSC_COLUMNS);
									row=(i+1)/(SIFNTSC_COLUMNS);
									v6=row*SIFNTSC_COLUMNS+c; // vecinito 6

									if (!((c >= 0) && (c < SIFNTSC_COLUMNS) && 
										 (row >= 0) && (row < SIFNTSC_ROWS))) { // si no es v�lido ponemos sus valores a 0
										myR = 0.;
										myG = 0.;
										myB = 0.;
									} else {
										myR = (float)(unsigned int)(unsigned char)(*myActualCamera)[v6*3+2];
										myG = (float)(unsigned int)(unsigned char)(*myActualCamera)[v6*3+1];
										myB = (float)(unsigned int)(unsigned char)(*myActualCamera)[v6*3+0];
									}

									myHSV = (struct HSV*) RGB2HSV_getHSV ((int)myR,(int)myG,(int)myB); // pasamos de RGB -> HSV

									if (!((myHSV->H*DEGTORAD > 1.7017) && (myHSV->H*DEGTORAD < 2.5394) &&
										(myHSV->S > 0.31) && (myHSV->S < 0.7) &&
										(myHSV->V > 77.6) && (myHSV->V < 207.2))) { // si no soy color campo, sigo comprobando

										c = ((i+1)-(SIFNTSC_COLUMNS))%(SIFNTSC_COLUMNS);
										row=((i+1)-(SIFNTSC_COLUMNS))/(SIFNTSC_COLUMNS);
										v7=row*SIFNTSC_COLUMNS+c; // vecinito 7

										if (!((c >= 0) && (c < SIFNTSC_COLUMNS) && 
											 (row >= 0) && (row < SIFNTSC_ROWS))) { // si no es v�lido ponemos sus valores a 0
											myR = 0.;
											myG = 0.;
											myB = 0.;
										} else {
											myR = (float)(unsigned int)(unsigned char)(*myActualCamera)[v7*3+2];
											myG = (float)(unsigned int)(unsigned char)(*myActualCamera)[v7*3+1];
											myB = (float)(unsigned int)(unsigned char)(*myActualCamera)[v7*3+0];
										}

										myHSV = (struct HSV*) RGB2HSV_getHSV ((int)myR,(int)myG,(int)myB); // pasamos de RGB -> HSV

										if (!((myHSV->H*DEGTORAD > 1.7017) && (myHSV->H*DEGTORAD < 2.5394) &&
											(myHSV->S > 0.31) && (myHSV->S < 0.7) &&
											(myHSV->V > 77.6) && (myHSV->V < 207.2))) { // si no soy color campo, sigo comprobando

											c = ((i+1)+(SIFNTSC_COLUMNS))%(SIFNTSC_COLUMNS);
											row=((i+1)+(SIFNTSC_COLUMNS))/(SIFNTSC_COLUMNS);
											v8=row*SIFNTSC_COLUMNS+c; // vecinito 8

											if (!((c >= 0) && (c < SIFNTSC_COLUMNS) && 
												 (row >= 0) && (row < SIFNTSC_ROWS))) { // si no es v�lido ponemos sus valores a 0
												myR = 0.;
												myG = 0.;
												myB = 0.;
											} else {
												myR = (float)(unsigned int)(unsigned char)(*myActualCamera)[v8*3+2];
												myG = (float)(unsigned int)(unsigned char)(*myActualCamera)[v8*3+1];
												myB = (float)(unsigned int)(unsigned char)(*myActualCamera)[v8*3+0];
											}

											myHSV = (struct HSV*) RGB2HSV_getHSV ((int)myR,(int)myG,(int)myB); // pasamos de RGB -> HSV

											if (!((myHSV->H*DEGTORAD > 1.7017) && (myHSV->H*DEGTORAD < 2.5394) &&
												(myHSV->S > 0.31) && (myHSV->S < 0.7) &&
												(myHSV->V > 77.6) && (myHSV->V < 207.2))) { // si no soy color campo, se acab�, no es pto. frontera
												esFrontera = FALSE;
											} // fin if vecinito 8
										} // fin if vecinito 7
									} // fin if vecinito 6
								} // fin if vecinito 5
							} // fin if vecinito 4
						} // fin if vecinito 3
					} // fin if vecinito 2
				} // fin if vecinito 1

				if (esFrontera == TRUE) { // si NO SOY COLOR CAMPO y alguno de los vecinitos ES color campo...
					imagenSuelo_buf[k*4]=255;
					imagenSuelo_buf[k*4+1]=255;
					imagenSuelo_buf[k*4+2]=255;

					myActualPointImage.x = rowPoint;
					myActualPointImage.y = columnPoint;
						
					pointProjection (myActualPointImage, myActualCamera);
				} else { // si todos los vecinitos son color NO campo
					imagenSuelo_buf[k*4]=0;
					imagenSuelo_buf[k*4+1]=0;
					imagenSuelo_buf[k*4+2]=0;
				}
			} else { // fin if soy negro (else no soy negro)
				imagenSuelo_buf[k*4]=0;
				imagenSuelo_buf[k*4+1]=0;
				imagenSuelo_buf[k*4+2]=0;
			}

			imagenSuelo_buf[k*4+3]=0;
		} // fin if myColorX != NULL
	} // fin FOR
}

void displayFilteredImage () {
  if (((display_state&DISPLAY_COLORIMAGEA) != 0) ||
			((display_state&DISPLAY_COLORIMAGEB) != 0) ||
			((display_state&DISPLAY_COLORIMAGEC) != 0) ||
			((display_state&DISPLAY_COLORIMAGED) != 0)) {
		XPutImage(mydisplay,fronteragui_win,fronteragui_gc,imagenFiltrada,0,0,fd_fronteragui->filteredImage->x+1, fd_fronteragui->filteredImage->y+1, SIFNTSC_COLUMNS, SIFNTSC_ROWS);
		XPutImage(mydisplay,fronteragui_win,fronteragui_gc,imagenSuelo,0,0,fd_fronteragui->groundImage->x+1, fd_fronteragui->filteredImage->y+1, SIFNTSC_COLUMNS, SIFNTSC_ROWS);
	}
}

void displayImageA () {
  if ((display_state&DISPLAY_COLORIMAGEA)!=0) { /* Draw *myscreen onto display */
		XPutImage(mydisplay,fronteragui_win,fronteragui_gc,imagenA,0,0,fd_fronteragui->ventanaA->x+1, fd_fronteragui->ventanaA->y+1, SIFNTSC_COLUMNS/2, SIFNTSC_ROWS/2);
	}
}
void displayImageB () {
  if ((display_state&DISPLAY_COLORIMAGEB)!=0)
    {    /* Draw *myscreen onto display */
      XPutImage(mydisplay,fronteragui_win,fronteragui_gc,imagenB,0,0,fd_fronteragui->ventanaB->x+1, fd_fronteragui->ventanaB->y+1, SIFNTSC_COLUMNS/2, SIFNTSC_ROWS/2);
    }
}

void displayImageC () {
  if ((display_state&DISPLAY_COLORIMAGEC)!=0)
    {    /* Draw *myscreen onto display */
      XPutImage(mydisplay,fronteragui_win,fronteragui_gc,imagenC,0,0,fd_fronteragui->ventanaC->x+1, fd_fronteragui->ventanaC->y+1, SIFNTSC_COLUMNS/2, SIFNTSC_ROWS/2);
    }
}

void displayImageD () {  
  if ((display_state&DISPLAY_COLORIMAGED)!=0){    /* Draw *myscreen onto display */
     XPutImage(mydisplay,fronteragui_win,fronteragui_gc,imagenD,0,0,
               fd_fronteragui->ventanaD->x+1,
               fd_fronteragui->ventanaD->y+1, SIFNTSC_COLUMNS/2,
               SIFNTSC_ROWS/2);
  }
}

void initGroundPoints () {
	int i;

	for (i = 0; i < SIFNTSC_COLUMNS*SIFNTSC_ROWS; i ++) {
		groundPoints[i].X = 0.;
		groundPoints[i].X = 0.;
		groundPoints[i].X = 0.;
	}
	actualGroundPoint = 0; // inicializamos contador
}

void drawGroundPoints () {
	int i;

	glColor3f(0.0f, 0.0f, 1.0f);
	glBegin (GL_POINTS);
		for (i = 0; i < SIFNTSC_COLUMNS*SIFNTSC_ROWS; i ++) {
			glVertex3f (groundPoints [i].X, groundPoints [i].Y, groundPoints [i].Z);
			//printf ("%f %f %f\n", groundPoints [i].X, groundPoints [i].Y, groundPoints [i].Z);
		}
	glEnd ();
}

void frontera_guidisplay() {	
	if (groundView == FALSE) {
		initConfiguration ();
		initCalibration ();  

		drawRoboticLabPlanes ();
		drawRoboticLabLines ();

		drawAxes ();
		drawCube();
		progeo ();

    glLineWidth(2.f);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glColor3f( 0, 0, 0.7 );
    glBegin(GL_LINES);
    v3f(-gsl_vector_get(x_1,0),-gsl_vector_get(x_1,1),-gsl_vector_get(x_1,2));   
    v3f(-progtest3D.X,-progtest3D.Y,-progtest3D.Z);
    glEnd();

		setCameraPos(1, K_1,R_1,x_1);

		//glLoadIdentity (); // nos volvemos al origen para dibujar las siguientes c�maras
/*
		drawCam (&virtualcam2, 2); // dibujamos la c�mara virtual de visualizaci�n de imagen
		drawCam (&virtualcam3, 3); // dibujamos la c�mara virtual de visualizaci�n de imagen
		drawCam (&virtualcam4, 4); // dibujamos la c�mara virtual de visualizaci�n de imagen
		drawCam (&virtualcam5, 5); // dibujamos la c�mara virtual de visualizaci�n de imagen
		drawCam (&virtualcam0, 6);
*/
		drawFilteredImage ();

		drawGroundImage (); // aqu� tambi�n se ir�n generando los puntos de intersecci�n con el plano suelo

		drawImageA ();
		drawImageB ();
		drawImageC ();
		drawImageD ();

		displayFilteredImage ();

		displayImageA ();
		displayImageB ();
		displayImageC ();
		displayImageD ();

	  /** intercambio de buffers: ha de hacerse lo �ltimo para tomar siempre todos los objetos de OpenGL **/
	  glXSwapBuffers(fl_display, fl_get_canvas_id(fd_fronteragui->canvas));
	} else {
		initMiniCanvasSettings ();
		drawRoboticLabGround ();

		drawGroundPoints ();

		glXSwapBuffers(fl_display, fl_get_canvas_id(fd_fronteragui->minicanvas));
	}
}

void frontera_guibuttons(void *obj1) {
	 float lati, longi, r;

   FL_OBJECT *obj;
   obj=(FL_OBJECT *)obj1;

   if (obj == fd_fronteragui->colorA){
      if (fl_get_button(fd_fronteragui->colorA)==PUSHED){
         mycolorA=myimport ("colorA", "colorA");
         colorAresume=(resumeFn)myimport("colorA", "resume");
         colorAsuspend=(suspendFn)myimport("colorA", "suspend");
         if (colorAresume!=NULL){
            display_state = display_state | DISPLAY_COLORIMAGEA;
            colorAresume(frontera_id, NULL, NULL);
         }
         else{
            display_state = display_state & ~DISPLAY_COLORIMAGEA;
            fl_set_button(obj, RELEASED);
         }
      }
      else {
         display_state = display_state & ~DISPLAY_COLORIMAGEA;
         if (colorAsuspend!=NULL)
            colorAsuspend();
      }
   }
   else if (obj == fd_fronteragui->colorB){
      if (fl_get_button(fd_fronteragui->colorB)==PUSHED){
         mycolorB=myimport ("colorB", "colorB");
         colorBresume=(resumeFn)myimport("colorB", "resume");
         colorBsuspend=(suspendFn)myimport("colorB", "suspend");
         if (colorBresume!=NULL){
            display_state = display_state | DISPLAY_COLORIMAGEB;
            colorBresume(frontera_id, NULL, NULL);
         }
         else{
            display_state = display_state & ~DISPLAY_COLORIMAGEB;
            fl_set_button(obj, RELEASED);
         }
      }
      else {
         display_state = display_state & ~DISPLAY_COLORIMAGEB;
         if (colorBsuspend!=NULL)
            colorBsuspend();
      }
   }

   else if (obj == fd_fronteragui->colorC){
      if (fl_get_button(fd_fronteragui->colorC)==PUSHED){
         mycolorC=myimport ("colorC", "colorC");
         colorCresume=(resumeFn)myimport("colorC", "resume");
         colorCsuspend=(suspendFn)myimport("colorC", "suspend");
         if (colorCresume!=NULL){
            display_state = display_state | DISPLAY_COLORIMAGEC;
            colorCresume(frontera_id, NULL, NULL);
         }
         else{
            display_state = display_state & ~DISPLAY_COLORIMAGEC;
            fl_set_button(obj, RELEASED);
         }
      }
      else {
         display_state = display_state & ~DISPLAY_COLORIMAGEC;
         if (colorCsuspend!=NULL)
            colorCsuspend();
      }
   }

   else if (obj == fd_fronteragui->colorD)
    {
      if (fl_get_button(fd_fronteragui->colorD)==PUSHED){
         mycolorD=myimport ("colorD", "colorD");
         colorDresume=(resumeFn)myimport("colorD", "resume");
         colorDsuspend=(suspendFn)myimport("colorD", "suspend");
         if (colorDresume!=NULL){
            display_state = display_state | DISPLAY_COLORIMAGED;
            colorDresume(frontera_id, NULL, NULL);
         }
         else{
            display_state = display_state & ~DISPLAY_COLORIMAGED;
            fl_set_button(obj, RELEASED);
         }
      }
      else {
         display_state = display_state & ~DISPLAY_COLORIMAGED;
         if (colorDsuspend!=NULL)
            colorDsuspend();
      }
    }

   else if (obj == fd_fronteragui->canvasButton) {
      if (fl_get_button(fd_fronteragui->canvasButton)==PUSHED)
				groundView = TRUE;
      else
				groundView = FALSE;
    }

   else if (obj == fd_fronteragui->valuator) {
			t = fl_get_slider_value (fd_fronteragui->valuator);
		}
   
}

void frontera_guisuspend_aux(void)
{
  /* to make a safety stop when the robot is being teleoperated from GUI */
  mydelete_buttonscallback(frontera_guibuttons);
  mydelete_displaycallback(frontera_guidisplay);
  fl_hide_form(fd_fronteragui->fronteragui);
}

void frontera_guisuspend(){
   static callback fn=NULL;
   if (fn==NULL){
      if ((fn=(callback)myimport ("graphics_xforms", "suspend_callback"))!=NULL){
         fn ((gui_function)frontera_guisuspend_aux);
      }
   }
   else{
      fn ((gui_function)frontera_guisuspend_aux);
   }
}

int freeobj_filteredImage_handle(FL_OBJECT *obj, int event, FL_Coord mx, FL_Coord my,int key, void *xev){
  if ((event==FL_PUSH)&&(key==MOUSELEFT)&&
      ((display_state&DISPLAY_COLORIMAGEA)!=0))
    samplesource=*mycolorA;
  return 0;
}

int freeobj_groundImage_handle(FL_OBJECT *obj, int event, FL_Coord mx, FL_Coord my,int key, void *xev){
  if ((event==FL_PUSH)&&(key==MOUSELEFT)&&
      ((display_state&DISPLAY_COLORIMAGEA)!=0))
    samplesource=*mycolorA;
  return 0;
}

int freeobj_ventanaAA_handle(FL_OBJECT *obj, int event, FL_Coord mx, FL_Coord my,int key, void *xev){
  if ((event==FL_PUSH)&&(key==MOUSELEFT)&&
      ((display_state&DISPLAY_COLORIMAGEA)!=0))
    samplesource=*mycolorA;
  return 0;
}

int freeobj_ventanaBB_handle(FL_OBJECT *obj, int event, FL_Coord mx, FL_Coord my,int key, void *xev){
  if ((event==FL_PUSH)&&(key==MOUSELEFT)&&
      ((display_state&DISPLAY_COLORIMAGEB)!=0))
    samplesource=*mycolorB;
  return 0;
}

int freeobj_ventanaCC_handle(FL_OBJECT *obj, int event, FL_Coord mx, FL_Coord my,int key, void *xev){
  if ((event==FL_PUSH)&&(key==MOUSELEFT)&&
      ((display_state&DISPLAY_COLORIMAGEC)!=0)) 
    samplesource=*mycolorC;
  return 0;
}

int freeobj_ventanaDD_handle(FL_OBJECT *obj, int event, FL_Coord mx, FL_Coord my,int key, void *xev){
  if ((event==FL_PUSH)&&(key==MOUSELEFT)&&
      ((display_state&DISPLAY_COLORIMAGED)!=0))
    samplesource=*mycolorD;
  return 0;
}

void motion_event(FL_OBJECT *ob, Window win, int win_width, 
		  int win_height, XKeyEvent *xev, void *user_data){
  float longi, lati,r ;

	xev = (XKeyPressedEvent*)xev;

	/* las siguiente ecuaciones sirven mapear el movimiento:

	Eje X :: (0,w) -> (-180,180)
	Eje Y :: (0,h) -> (-90,90)

	donde (h,w) es el larcho,ancho del gl_canvas
	Si el button pulsado es 2 (centro del raton) entonces no actualizamos la posicion

	*/

	if (2 == xev->keycode) {
		flashImage = TRUE;
	  return;
	}

	mouse_on_canvas.x = ((xev->x*360/fd_fronteragui->canvas->w)-180);
	mouse_on_canvas.y = ((xev->y*-180/fd_fronteragui->canvas->h)+90);
	
}

void button_press_event (FL_OBJECT *ob, Window win, int win_width, 
			 int win_height, XKeyEvent *xev, void *user_data){
  
  xev = (XKeyPressedEvent*)xev;
  float longi, lati,r;

	/* Los dos modos son excluyentes */
	if (2 == xev->keycode) {
		flashImage = TRUE;
	  return;
	}else {
		flashImage = FALSE;
		if (1 == xev->keycode){
		  cam_mode = 1;
		  foa_mode = 0;  
		}else if (3 == xev->keycode){
		  foa_mode = 1;
		  cam_mode = 0;
		}else if (5 == xev->keycode){
		  if (radius < MAX_RADIUS_VALUE)
		    radius+=WHEEL_DELTA;
		}else if (4 == xev->keycode){
		  if (radius>MIN_RADIUS_VALUE)
		    radius-=WHEEL_DELTA;
		}
	}
}

void frontera_guiresume_aux(void)
{
  static int k=0;

  if (k==0) /* not initialized */ {
		k++;
		fd_fronteragui = create_form_fronteragui();
		fl_set_form_position(fd_fronteragui->fronteragui,400,50);
		fl_add_canvas_handler(fd_fronteragui->canvas,Expose,InitOGL,0);
		fl_add_canvas_handler(fd_fronteragui->minicanvas,Expose,InitMiniOGL,0);
		fl_add_canvas_handler(fd_fronteragui->minicanvas,MotionNotify,motion_event,0);
		fl_add_canvas_handler(fd_fronteragui->minicanvas,ButtonPress,button_press_event,0);
		fl_add_canvas_handler(fd_fronteragui->canvas,MotionNotify,motion_event,0);
		fl_add_canvas_handler(fd_fronteragui->canvas,ButtonPress,button_press_event,0);
  }

	myregister_buttonscallback(frontera_guibuttons);
	myregister_displaycallback(frontera_guidisplay);
	fl_show_form(fd_fronteragui->fronteragui,FL_PLACE_POSITION,FL_FULLBORDER,"frontera");
	fronteragui_win = FL_ObjWin(fd_fronteragui->ventanaA);
	image_displaysetup();
}

void frontera_guiresume(){
   static callback fn=NULL;
   if (fn==NULL){
      if ((fn=(callback)myimport ("graphics_xforms", "resume_callback"))!=NULL){
         fn ((gui_function)frontera_guiresume_aux);
      }
   }
   else{
      fn ((gui_function)frontera_guiresume_aux);
   }
}

extern void frontera_guisuspend(void);

void frontera_startup()
{
  pthread_mutex_lock(&(all[frontera_id].mymutex));
  myexport("frontera","id",&frontera_id);
  myexport("frontera","resume",(void *) &frontera_resume);
  myexport("frontera","suspend",(void *) &frontera_suspend);
  printf("frontera schema started up\n");
  put_state(frontera_id,slept);
  pthread_create(&(all[frontera_id].mythread),NULL,frontera_thread,NULL);
	if (myregister_buttonscallback==NULL){
    if ((myregister_buttonscallback=
	 (registerbuttons)myimport ("graphics_xforms", "register_buttonscallback"))==NULL){
      printf ("I can't fetch register_buttonscallback from graphics_xforms\n");
      jdeshutdown(1);
    }
    if ((mydelete_buttonscallback=(deletebuttons)myimport ("graphics_xforms", "delete_buttonscallback"))==NULL){
      printf ("I can't fetch delete_buttonscallback from graphics_xforms\n");
      jdeshutdown(1);
    }
    if ((myregister_displaycallback=
	 (registerdisplay)myimport ("graphics_xforms", "register_displaycallback"))==NULL){
      printf ("I can't fetch register_displaycallback from graphics_xforms\n");
      jdeshutdown(1);
    }
    if ((mydelete_displaycallback=(deletedisplay)myimport ("graphics_xforms", "delete_displaycallback"))==NULL){
      jdeshutdown(1);
      printf ("I can't fetch delete_displaycallback from graphics_xforms\n");
    }
  }

  if ((myscreen=(int *)myimport("graphics_xforms", "screen"))==NULL){
    fprintf (stderr, "frontera: I can't fetch screen from graphics_xforms\n");
    jdeshutdown(1);
  }
  if ((mydisplay=(Display *)myimport("graphics_xforms", "display"))==NULL){
    fprintf (stderr, "frontera: I can't fetch display from graphics_xforms\n");
    jdeshutdown(1);
  }
  pthread_mutex_unlock(&(all[frontera_id].mymutex));

	// Valores iniciales para la c�mara virtual con la que observo la escena
  virtualcam0.position.X=3937.;
  virtualcam0.position.Y=2117.;
  virtualcam0.position.Z=12000.;
  virtualcam0.position.H=1.;
  virtualcam0.foa.X=3937.;
  virtualcam0.foa.Y=2117.;
  virtualcam0.foa.Z=0.;
  virtualcam0.position.H=1.;
  virtualcam0.roll=0.;

	// Valores iniciales para la c�mara virtual con la que observo la escena
  virtualcam1.position.X=-4000;
  virtualcam1.position.Y=-4000.;
  virtualcam1.position.Z=6000.;
  virtualcam1.position.H=1.;
  virtualcam1.foa.X=6000.;
  virtualcam1.foa.Y=4500.;
  virtualcam1.foa.Z=0.;
  virtualcam1.position.H=1.;
  virtualcam1.roll=0.;

	// Valores para la c�mara virtual de la esquina derecha del laboratorio (myColorA)
  virtualcam2.position.X=7875.000000;
  virtualcam2.position.Y=-514.000000;
  virtualcam2.position.Z=3000.000000;
  virtualcam2.position.H=1.;
  virtualcam2.foa.X=6264.000000;
  virtualcam2.foa.Y=785.0000000;
  virtualcam2.foa.Z=1950.00000;
  virtualcam2.foa.H=1.;
  virtualcam2.fdist=405.399994;
  virtualcam2.u0=142.600006;
  virtualcam2.v0=150.399994;
  virtualcam2.roll=3.107262;

	// Valores para la c�mara virtual de la esquina derecha de enfrente del laboratorio (myColorB)
  virtualcam3.position.X=7875.000000;
  virtualcam3.position.Y=4749.000000;
  virtualcam3.position.Z=3000.000000;
  virtualcam3.position.H=1.;
  virtualcam3.foa.X=6264.000000;
  virtualcam3.foa.Y=3700.000000;
  virtualcam3.foa.Z=1950.00000;
  virtualcam3.foa.H=1.;
  virtualcam3.fdist=405.399994;
  virtualcam3.u0=142.600006;
  virtualcam3.v0=142.600006;
  virtualcam3.roll=3.007028;

	// Valores para la c�mara virtual de la esquina izquierda de enfrente del laboratorio (myColorC)
  virtualcam4.position.X=50.000000;
  virtualcam4.position.Y=4471.000000;
  virtualcam4.position.Z=2955.000000;
  virtualcam4.position.H=1.;
  virtualcam4.foa.X=2432.399902;
  virtualcam4.foa.Y=2918.000000;
  virtualcam4.foa.Z=1300.000000;
  virtualcam4.foa.H=1.;
  virtualcam4.fdist=405.399994;
  virtualcam4.u0=142.600006;
  virtualcam4.v0=150.399994;
  virtualcam4.roll=3.107262;

	// Valores para la c�mara virtual de la esquina izquierda del laboratorio (myColorD)
  virtualcam5.position.X=50.000000;
  virtualcam5.position.Y=100.000000;
  virtualcam5.position.Z=2955.000000;
  virtualcam5.position.H=1.;
  virtualcam5.foa.X=2160.000000;
  virtualcam5.foa.Y=1151.000000;
  virtualcam5.foa.Z=1550.000000;
  virtualcam5.foa.H=1.;
  virtualcam5.fdist=405.399994;
  virtualcam5.u0=142.600006;
  virtualcam5.v0=150.399994;
  virtualcam5.roll=2.956911;

	update_camera_matrix (&virtualcam2);
	update_camera_matrix (&virtualcam3);
	update_camera_matrix (&virtualcam4);
	update_camera_matrix (&virtualcam5);

  int argc=1;
  char **argv;
  char *aa;
  char a[]="myjde";
 
  aa=a;
  argv=&aa;

	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);

	RGB2HSV_init();
	RGB2HSV_createTable();
}

/** Header file generated with fdesign on Tue Mar 25 09:48:21 2008.**/

#ifndef FD_fronteragui_h_
#define FD_fronteragui_h_

/** Callbacks, globals and object handlers **/
extern int freeobj_ventanaAA_handle(FL_OBJECT *, int, FL_Coord, FL_Coord,
			int, void *);
extern int freeobj_ventanaBB_handle(FL_OBJECT *, int, FL_Coord, FL_Coord,
			int, void *);
extern int freeobj_ventanaCC_handle(FL_OBJECT *, int, FL_Coord, FL_Coord,
			int, void *);
extern int freeobj_ventanaDD_handle(FL_OBJECT *, int, FL_Coord, FL_Coord,
			int, void *);
extern int freeobj_filteredImage_handle(FL_OBJECT *, int, FL_Coord, FL_Coord,
			int, void *);
extern int freeobj_groundImage_handle(FL_OBJECT *, int, FL_Coord, FL_Coord,
			int, void *);


/**** Forms and Objects ****/
typedef struct {
	FL_FORM *fronteragui;
	void *vdata;
	char *cdata;
	long  ldata;
	FL_OBJECT *hide;
	FL_OBJECT *ventanaA;
	FL_OBJECT *ventanaB;
	FL_OBJECT *ventanaC;
	FL_OBJECT *ventanaD;
	FL_OBJECT *canvas;
	FL_OBJECT *colorA;
	FL_OBJECT *colorB;
	FL_OBJECT *colorC;
	FL_OBJECT *colorD;
	FL_OBJECT *filteredImage;
	FL_OBJECT *groundImage;
	FL_OBJECT *minicanvas;
	FL_OBJECT *canvasButton;
	FL_OBJECT *valuator;
} FD_fronteragui;

extern FD_fronteragui * create_form_fronteragui(void);

#endif /* FD_fronteragui_h_ */

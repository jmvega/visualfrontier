/* Form definition file generated with fdesign. */

#include "forms.h"
#include <stdlib.h>
#include "fronteragui.h"

FD_fronteragui *create_form_fronteragui(void)
{
  FL_OBJECT *obj;
  FD_fronteragui *fdui = (FD_fronteragui *) fl_calloc(1, sizeof(*fdui));

  fdui->fronteragui = fl_bgn_form(FL_NO_BOX, 1270, 940);
  obj = fl_add_box(FL_FRAME_BOX,0,0,1270,940,"");
    fl_set_object_lcolor(obj,FL_BLUE);
  fdui->hide = obj = fl_add_button(FL_NORMAL_BUTTON,1210,15,50,20,"HIDE");
    fl_set_object_boxtype(obj,FL_FRAME_BOX);
    fl_set_object_color(obj,FL_DARKCYAN,FL_COL1);
    fl_set_object_lcolor(obj,FL_CYAN);
    fl_set_object_lsize(obj,FL_NORMAL_SIZE);
  fdui->ventanaA = obj = fl_add_free(FL_NORMAL_FREE,235,620,320,240,"ventanaA",
			freeobj_ventanaAA_handle);
    fl_set_object_boxtype(obj,FL_FRAME_BOX);
  fdui->colorA = obj = fl_add_button(FL_PUSH_BUTTON,360,870,70,20,"camera A");
    fl_set_object_boxtype(obj,FL_FRAME_BOX);
    fl_set_object_color(obj,FL_COL1,FL_INACTIVE);
    fl_set_object_lcolor(obj,FL_DARKTOMATO);
  fdui->filteredImage = obj = fl_add_free(FL_NORMAL_FREE,610,620,320,240,"",
			freeobj_filteredImage_handle);
    fl_set_object_boxtype(obj,FL_FRAME_BOX);
  fdui->groundImage = obj = fl_add_free(FL_NORMAL_FREE,940,620,320,240,"",
			freeobj_groundImage_handle);
    fl_set_object_boxtype(obj,FL_FRAME_BOX);
  obj = fl_add_text(FL_NORMAL_TEXT,725,595,100,20,"Filtered image");
    fl_set_object_lcolor(obj,FL_CYAN);
    fl_set_object_lalign(obj,FL_ALIGN_LEFT|FL_ALIGN_INSIDE);
  obj = fl_add_text(FL_NORMAL_TEXT,1060,595,100,20,"Border image");
    fl_set_object_lcolor(obj,FL_WHITE);
    fl_set_object_lalign(obj,FL_ALIGN_LEFT|FL_ALIGN_INSIDE);
  fdui->minicanvas = obj = fl_add_glcanvas(FL_NORMAL_CANVAS,10,10,740,570,"");
    fl_set_object_boxtype(obj,FL_FRAME_BOX);
  fdui->canvasButton = obj = fl_add_button(FL_PUSH_BUTTON,760,10,90,30,"Ground View");
    fl_set_object_lcolor(obj,FL_DODGERBLUE);
    fl_set_object_lstyle(obj,FL_NORMAL_STYLE+FL_EMBOSSED_STYLE);
  fdui->fronteraButton = obj = fl_add_button(FL_PUSH_BUTTON,860,10,90,30,"Border View");
    fl_set_object_lcolor(obj,FL_YELLOW);
    fl_set_object_lstyle(obj,FL_NORMAL_STYLE+FL_EMBOSSED_STYLE);
  fdui->fronteracanvas = obj = fl_add_glcanvas(FL_NORMAL_CANVAS,760,50,500,530,"");
    fl_set_object_boxtype(obj,FL_FRAME_BOX);
  obj = fl_add_text(FL_NORMAL_TEXT,345,595,100,20,"Original image");
    fl_set_object_lcolor(obj,FL_BLUE);
    fl_set_object_lalign(obj,FL_ALIGN_LEFT|FL_ALIGN_INSIDE);
  fdui->go1 = obj = fl_add_button(FL_NORMAL_BUTTON,70,620,90,30,"Go 1 m.");
    fl_set_object_lcolor(obj,FL_RED);
  fdui->go50 = obj = fl_add_button(FL_NORMAL_BUTTON,70,660,90,30,"Go 50 cm.");
    fl_set_object_lcolor(obj,FL_RED);
  fdui->turnRight45 = obj = fl_add_button(FL_NORMAL_BUTTON,70,700,90,30,"Turn 45� R");
    fl_set_object_lcolor(obj,FL_RED);
  fdui->turnLeft45 = obj = fl_add_button(FL_NORMAL_BUTTON,70,740,90,30,"Turn 45� L");
    fl_set_object_lcolor(obj,FL_RED);
  fdui->turnRight90 = obj = fl_add_button(FL_NORMAL_BUTTON,70,780,90,30,"Turn 90� R");
    fl_set_object_lcolor(obj,FL_RED);
  fdui->turnLeft90 = obj = fl_add_button(FL_NORMAL_BUTTON,70,820,90,30,"Turn 90� L");
    fl_set_object_lcolor(obj,FL_RED);
  fdui->flashButton = obj = fl_add_button(FL_PUSH_BUTTON,70,860,90,30,"FLASH !");
    fl_set_object_lcolor(obj,FL_GREEN);
    fl_set_object_lstyle(obj,FL_NORMAL_STYLE+FL_EMBOSSED_STYLE);
  fl_end_form();

  fdui->fronteragui->fdui = fdui;

  return fdui;
}
/*---------------------------------------*/


/*
 *  Copyright (C) 2008 José María Cañas Plaza 
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * 	Authors : Julio Vega Pérez (julio [dot] vega [at] urjc [dot] es)
 *  Authors : José María Cañas (jmplaza [at] gsyc [dot] es
 *  Authors : Darío Rodríguez de Diego (drd [dot] sqki [at] gmail [dot] com)
 *  Authors : Víctor Hidalgo 
 *
 *  This schema was programed for Guiderobot Project http://jde.gsyc.es/index.php/guiderobot
 *
 */

#include "frontera.h"

int frontera_brothers[MAX_SCHEMAS];
arbitration frontera_callforarbitration;

int DEBUG = 1;
int debug = 1;
// Control estático de los movimientos de traslación/rotación del robot
int flash = FALSE;
int traslacion = 0, rotacion = 0;
int flashImage = TRUE;

// Variables para guardar las matrices K, R y T de nuestra cámara:
gsl_matrix *K_1,*R_1;
gsl_vector* x_1;
HPoint2D progtest2D;
HPoint3D progtest3D;

struct HSV* myHSV;
TPinHoleCamera virtualcam0; /* para ver el minimundillo */
TPinHoleCamera virtualcam1; /* para moverme por el mundillo */
TPinHoleCamera virtualcam2; /* para ver el entorno circundante al robot */
TPinHoleCamera roboticLabCam0; // cámaras del techo del laboratorio
TPinHoleCamera roboticLabCam1; //
TPinHoleCamera roboticLabCam2; //
TPinHoleCamera roboticLabCam3; //
TPinHoleCamera ceilLabCam;
TPinHoleCamera robotCamera;
TPinHoleCamera robotCamera2;
TPinHoleCamera *myCamera; // puntero temporal
HPoint2D pixel_camA;
HPoint3D myfloor[18*2]; /* mm */
int myfloor_lines=0;

HPoint2D actualPosition;
float coveredDistance;

static SofReference mypioneer;

int actualCameraView; // identificador de la cámara de la escena actual

registerdisplay myregister_displaycallback;
deletedisplay mydelete_displaycallback;

char *configFile;

HPoint2D myActualPoint2D; // variables para el cálculo de los rayos virtuales
HPoint3D myActualPoint3D;
HPoint2D myActualPointImage, miPrimerPunto, miSegundoPunto, miTercerPunto, miCuartoPunto;
HPoint3D cameraPos3D;
HPoint3D myEndPoint3D;
HPoint3D intersectionPoint;

HPoint2D groundLines2D [MAX_LINES_TO_DETECT];
HPoint3DColor groundLines3D [MAX_LINES_IN_MEMORY];

HPoint3DColor groundPoints[SIFNTSC_COLUMNS*SIFNTSC_ROWS];
HPoint3D fronteraPoints[SIFNTSC_COLUMNS], primerPunto3D, segundoPunto3D, tercerPunto3D, cuartoPunto3D;
int actualGroundPoint = 0, actualGroundLine = 0, actualGroundPointLeft = 0, actualGroundPointRight = 0, actualFronteraPoint = 0; // contador para el punto actual del suelo...
int primerPunto = FALSE, segundoPunto = FALSE, tercerPunto = FALSE, cuartoPunto = FALSE;
float distanciaPlanoImagen, incremento;
int counterColor = 0;
int numIterations = 0;

HPoint2D mouse_on_minicanvas, mouse_on_fronteracanvas;

pthread_mutex_t main_mutex;
/*GTK variables*/
GladeXML *xml=NULL; /*Fichero xml*/
GtkWidget *win;
GtkWidget *fronteraCanvas, *floorCanvas;
GtkWidget *miniCanvas;

GtkImage *originalImage; // Widgets de las imágenes que se obtienen del Window1 (GTK)
GtkImage *filteredImage;
GtkImage *groundedImage;
GtkImage *borderImage;

float sliderPANTILT_BASE_HEIGHT, sliderISIGHT_OPTICAL_CENTER, sliderTILT_HEIGHT, sliderCAMERA_TILT_HEIGHT, sliderPANTILT_BASE_X;

struct image_struct *imageA = NULL; // Imágenes en crudo
struct image_struct *imageAfiltered = NULL;
struct image_struct *imageAgrounded = NULL;
struct image_struct *imageAbordered = NULL;

struct image_struct *originalImageRGB = NULL; // Imágenes preparadas para ser pintadas (cambio de bits)
struct image_struct *filteredImageRGB = NULL;
struct image_struct *groundedImageRGB = NULL;
struct image_struct *borderImageRGB = NULL;

/*Some control variable*/
static int vmode;
static XImage *imagenA, *imagenFiltrada, *imagenSuelo;
static char *imagenA_buf, *imagenFiltrada_buf, *imagenSuelo_buf; /* puntero a memoria para la imagen a visualizar en el servidor X. No compartida con el servidor */
char *image;
char *contourImage;
static long int tabla[256]; 
/* tabla con la traduccion de niveles de gris a numero de pixel en Pseudocolor-8bpp. Cada numero de pixel apunta al valor adecuado del ColorMap, con el color adecuado instalado */
static int pixel8bpp_rojo, pixel8bpp_blanco, pixel8bpp_amarillo;

int floorView = FALSE, worldView = TRUE, whatWorld = 0;

int frontera_id=0; 
int frontera_brothers[MAX_SCHEMAS];
arbitration frontera_callforarbitration;
int frontera_cycle=1000; /* ms */

char **myActualCamera;

int *mycolorAwidth		= NULL;
int *mycolorAheight		= NULL;
char **mycolorA;
float *myencoders=NULL;
float *myv=NULL;
float *myw=NULL;
runFn colorArun, encodersrun, motorsrun;
stopFn colorAstop, encodersstop, motorsstop;
float *mypan_angle=NULL, *mytilt_angle=NULL;  /* degs */
float *mylongitude=NULL; /* degs, pan angle */
float *mylatitude=NULL; /* degs, tilt angle */
float *mylongitude_speed=NULL;
float *mylatitude_speed=NULL;
float *max_pan=NULL;
float *min_pan=NULL;
float *max_tilt=NULL;
float *min_tilt=NULL;
runFn ptmotorsrun, ptencodersrun;
stopFn ptmotorsstop, ptencodersstop;

int botonPulsado;

float old_x=0., old_y=0.;
float longi_foa = 0.0;
float lati_foa = 0.0;
float foax = 0.0;
float foay = 0.0;
float foaz = 0.0;
float xcam = -1.0;
float ycam = -1.0;
float zcam = -1.0;
float lati = 0.0;
float longi = 0.0;
int foa_mode, cam_mode;
float radius = 500;
float radius_old;
int centrado = 0;
float t = 0.5; // lambda
float phi = 0., theta = 0.;
int boton_pulsado;
float tiltAngle, panAngle;
float speed_y, speed_x;
int completedMovement;
float threshold = 50.;

int pantiltStill;
int robotStill;
int comeFromCenter;
int last_movement;
int last_full_movement;

int numFlashes;

double actualInstant;
double stopInstant;

void printCameraInformation (TPinHoleCamera* actualCamera) {
	printf ("CAMERA INFORMATION\n");
	printf ("==================\n");
	printf ("Position = [%f, %f, %f]\n", actualCamera->position.X, actualCamera->position.Y, actualCamera->position.Z);
	printf ("FOA = [%f, %f, %f]\n", actualCamera->foa.X, actualCamera->foa.Y, actualCamera->foa.Z);
	printf ("fdist = [%f, %f], u0 = %f, v0 = %f, roll = %f\n", actualCamera->fdistx, actualCamera->fdisty, actualCamera->u0, actualCamera->v0, actualCamera->roll);
	printf ("K matrix:\n");
	printf ("%f %f %f %f\n", actualCamera->k11, actualCamera->k12, actualCamera->k13, actualCamera->k14);
	printf ("%f %f %f %f\n", actualCamera->k21, actualCamera->k22, actualCamera->k23, actualCamera->k24);
	printf ("%f %f %f %f\n", actualCamera->k31, actualCamera->k32, actualCamera->k33, actualCamera->k34);
	printf ("RT matrix:\n");
	printf ("%f %f %f %f\n", actualCamera->rt11, actualCamera->rt12, actualCamera->rt13, actualCamera->rt14);
	printf ("%f %f %f %f\n", actualCamera->rt21, actualCamera->rt22, actualCamera->rt23, actualCamera->rt24);
	printf ("%f %f %f %f\n", actualCamera->rt31, actualCamera->rt32, actualCamera->rt33, actualCamera->rt34);
	printf ("%f %f %f %f\n", actualCamera->rt41, actualCamera->rt42, actualCamera->rt43, actualCamera->rt44);
}

void drawCross(struct image_struct *image, HPoint2D p2d, int side,	unsigned char r, unsigned char g, unsigned char b) {
		int i, offset;
		float temp;

		temp = p2d.x;
		p2d.x = p2d.y;
		p2d.y = (SIFNTSC_ROWS-1)-p2d.x;

		// linea vertical
		for (i=-side; i<side+1; i++) {
			offset = image->width * ((int)p2d.y+i) * image->bpp + ((int)p2d.x * image->bpp);
			image->image[offset + 0] = b;
			image->image[offset + 1] = g;
			image->image[offset + 2] = r;
		}

		// linea horizontal
		for (i=-side; i<side+1; i++) {
			offset = image->width * (int)p2d.y * image->bpp + (((int)p2d.x+i) * image->bpp);
			image->image[offset + 0] = b;
			image->image[offset + 1] = g;
			image->image[offset + 2] = r;
		}
}

/*CALLBACKS*/
void floorButton_pressed (GtkButton *button, gpointer user_data) {
	GtkToggleButton *myViewButton;

	myViewButton = (GtkToggleButton *)glade_xml_get_widget(xml, "worldButton");
	gtk_toggle_button_set_active (myViewButton, FALSE);
	printf ("entramos en pressed\n");
  floorView = TRUE;
	worldView = FALSE;
}

void floorButton_released (GtkButton *button, gpointer user_data) {}

void worldButton_pressed (GtkButton *button, gpointer user_data) {
	GtkToggleButton *myViewButton;

	myViewButton = (GtkToggleButton *)glade_xml_get_widget(xml, "floorButton");
	gtk_toggle_button_set_active (myViewButton, FALSE);

  worldView = TRUE;
  floorView = FALSE;
}

void worldButton_released (GtkButton *button, gpointer user_data) {}

void go1Button_pressed (GtkButton *button, gpointer user_data) {
	traslacion += 1000;
}

void go1Button_released (GtkButton *button, gpointer user_data) {}

void go50Button_pressed (GtkButton *button, gpointer user_data) {
	traslacion += 500;
}

void go50Button_released (GtkButton *button, gpointer user_data) {}

void turn45RButton_pressed (GtkButton *button, gpointer user_data) {
	rotacion += 45;
}

void turn45RButton_released (GtkButton *button, gpointer user_data) {}

void turn45LButton_pressed (GtkButton *button, gpointer user_data) {
	rotacion -= 45;
}

void turn45LButton_released (GtkButton *button, gpointer user_data) {}

void turn90RButton_pressed (GtkButton *button, gpointer user_data) {
	rotacion += 90;
}

void turn90RButton_released (GtkButton *button, gpointer user_data) {}

void turn90LButton_pressed (GtkButton *button, gpointer user_data) {
	rotacion -= 90;
}

void turn90LButton_released (GtkButton *button, gpointer user_data) {}

void flashButton_pressed (GtkButton *button, gpointer user_data) {
	flash = TRUE;
}

void flashButton_released (GtkButton *button, gpointer user_data) {}

void camera1Button_pressed (GtkButton *button, gpointer user_data) {
	GtkToggleButton *myCameraTempButton;

	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera2Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera3Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera4Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "userCameraButton");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "ceilCameraButton");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);

  actualCameraView = 1;
}

void camera1Button_released (GtkButton *button, gpointer user_data) {}

void camera2Button_pressed (GtkButton *button, gpointer user_data) {
	GtkToggleButton *myCameraTempButton;

	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera1Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera3Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera4Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "userCameraButton");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "ceilCameraButton");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);

  actualCameraView = 2;
}

void camera2Button_released (GtkButton *button, gpointer user_data) {}

void camera3Button_pressed (GtkButton *button, gpointer user_data) {
	GtkToggleButton *myCameraTempButton;

	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera2Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera1Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera4Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "userCameraButton");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "ceilCameraButton");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);

  actualCameraView = 3;
}

void camera3Button_released (GtkButton *button, gpointer user_data) {}

void camera4Button_pressed (GtkButton *button, gpointer user_data) {
	GtkToggleButton *myCameraTempButton;

	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera2Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera3Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera1Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "userCameraButton");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "ceilCameraButton");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);

  actualCameraView = 4;
}

void camera4Button_released (GtkButton *button, gpointer user_data) {}

void ceilCameraButton_pressed (GtkButton *button, gpointer user_data) {
	GtkToggleButton *myCameraTempButton;

	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera2Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera3Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera4Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "userCameraButton");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera1Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);

  actualCameraView = 5;
}

void ceilCameraButton_released (GtkButton *button, gpointer user_data) {}

void userCameraButton_pressed (GtkButton *button, gpointer user_data) {
	GtkToggleButton *myCameraTempButton;

	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera2Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera3Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera4Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera1Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "ceilCameraButton");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);

  actualCameraView = 0;
}

void userCameraButton_released (GtkButton *button, gpointer user_data) {}

static gboolean button_press_event (GtkWidget *widget, GdkEventButton *event, gpointer data) {
  float boton_x;
  float boton_y;
  int back;

  event = (GdkEventButton*)event;
  old_x=event->x;
  old_y=event->y;

  if (2 == event->button){
  	boton_pulsado=2;
  	flashImage = TRUE;
 	  return TRUE;
  } else {
  	flashImage = FALSE;
  	if (1 == event->button){
  	  boton_pulsado=1;
  	  cam_mode = 1;
   	  foa_mode = 0;
	  } else if (3 == event->button) {
	    boton_pulsado=3;
	    foa_mode = 1;
	    cam_mode = 0;
	  }
		return TRUE;
  }
}

static gboolean motion_notify_event (GtkWidget *widget, GdkEventButton *event, gpointer data) {
   float x=event->x;
   float y=event->y;
  
	event = (GdkEventButton*)event;

	if (2==boton_pulsado) {
		flashImage = TRUE;
	  return TRUE;
	}	

	mouse_on_fronteracanvas.x = ((event->x*360/fronteraCanvas->allocation.width)-180);
	mouse_on_fronteracanvas.y = ((event->y*-180/fronteraCanvas->allocation.height)+90);
	//printf ("Valores de mouse_on_canvas: %f y %f \n",mouse_on_fronteracanvas.x,mouse_on_fronteracanvas.y);
 
	if (GDK_BUTTON1_MASK){ /*Si está pulsado el botón 1*/
      theta -= x - old_x;
      phi -= y - old_y;
      gtk_widget_queue_draw (widget);
      old_x=x;
      old_y=y;
   }
 return TRUE;
}

static gboolean scroll_event (GtkRange *range, GdkEventScroll *event, gpointer data){
   if (event->direction == GDK_SCROLL_DOWN){
	if (radius > MIN_RADIUS_VALUE)
   	radius-=WHEEL_DELTA;
   }
   if (event->direction == GDK_SCROLL_UP){
     if (radius<MAX_RADIUS_VALUE){
      radius+=WHEEL_DELTA;
       }
   }
   if (radius < 0.5) radius = 0.5;
   gtk_widget_queue_draw(GTK_WIDGET((GtkWidget *)data));
   
   return TRUE;
}

/*Callback of window closed*/
void on_delete_window (GtkWidget *widget,GdkEvent *event,gpointer user_data)
{
   gdk_threads_leave();
   frontera_hide();
   gdk_threads_enter();
}

struct image_struct *create_image(int width, int height, int bpp) {
	struct image_struct *w;

	w = (struct image_struct*) malloc(sizeof(struct image_struct));
	w->width = width;
	w->height = height;
	w->bpp = bpp;
	w->image = (char*) malloc(width * height * bpp);

	return w;
}

void remove_image(struct image_struct *w) {
	free(w->image);
	free(w);
}

/** prepare2draw ************************************************************************
* Prepare an image to draw it with a GtkImage in BGR format with 3 pixels per byte.	*
*	src: source image								*
*	dest: destiny image. It has to have the same size as the source image and	*
*	      3 bytes per pixel.
*****************************************************************************************/
void prepare2draw (struct image_struct *src, struct image_struct *dest) {
	int i;

	for (i=0; i<src->width*src->height; i++) {
		dest->image[i*dest->bpp+0] = src->image[i*src->bpp+2];
		dest->image[i*dest->bpp+1] = src->image[i*src->bpp+1];
		dest->image[i*dest->bpp+2] = src->image[i*src->bpp+0];
	}
}

unsigned long int dameTiempo() {
	struct timeval t; 

	gettimeofday(&t,NULL);
	return t.tv_sec*1000000 + t.tv_usec;	
}

void drawFloorInfiniteLines () {
	int i;

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glColor3f( 0.3, 0.3, 0.3 );
  glBegin(GL_LINES);
  for(i=0;i<((int)MAXWORLD+1);i++)
    {
      v3f(-(int)MAXWORLD*1000/2.+(float)i*1000,-(int)MAXWORLD*1000/2.,0.);
      v3f(-(int)MAXWORLD*1000/2.+(float)i*1000,(int)MAXWORLD*1000/2.,0.);
      v3f(-(int)MAXWORLD*1000/2.,-(int)MAXWORLD*1000/2.+(float)i*1000,0.);
      v3f((int)MAXWORLD*1000/2.,-(int)MAXWORLD*1000/2.+(float)i*1000,0.);
    }
  glEnd();
}


void drawMyAxes () {
  glLineWidth(2.0f);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  // Dibujamos el arco de visión de la cámara
	glColor3f (1.,0.,0.);
  glBegin(GL_LINES);
  v3f(robotCamera.position.X, robotCamera.position.Y, 0.000000);
  v3f(tercerPunto3D.X, tercerPunto3D.Y, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(robotCamera.position.X, robotCamera.position.Y, 0.000000);
  v3f(cuartoPunto3D.X, cuartoPunto3D.Y, 0.000000);
  glEnd();

	glColor3f (0.,0.,1.);
	// Dibujamos 
  glBegin(GL_LINES);
  v3f(primerPunto3D.X, primerPunto3D.Y, 0.000000);
  v3f(segundoPunto3D.X, segundoPunto3D.Y, 0.000000);
	glEnd();

  glBegin(GL_LINES);
  v3f(segundoPunto3D.X, segundoPunto3D.Y, 0.000000);
  v3f(tercerPunto3D.X, tercerPunto3D.Y, 0.000000);
	glEnd();

  glBegin(GL_LINES);
  v3f(tercerPunto3D.X, tercerPunto3D.Y, 0.000000);
  v3f(cuartoPunto3D.X, cuartoPunto3D.Y, 0.000000);
	glEnd();

  glBegin(GL_LINES);
  v3f(cuartoPunto3D.X, cuartoPunto3D.Y, 0.000000);
  v3f(primerPunto3D.X, primerPunto3D.Y, 0.000000);
	glEnd();

	glColor3f (1.,1.,1.);
}

void drawMyLines () {
  glLineWidth(1.5f);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

	glColor3f (1.,1.,1.);

	// Borde exterior del recinto
  glBegin(GL_LINES);
  v3f(0., 0., 0.000000);
  v3f(2600., 0., 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2600., 0., 0.000000);
  v3f(2600., 1400., 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2600., 1400., 0.000000);
  v3f(0., 1400., 0.000000);
	glEnd();

  glBegin(GL_LINES);
  v3f(0., 1400., 0.000000);
  v3f(0., 0., 0.000000);
	glEnd();

	// Líneas transversales
  glBegin(GL_LINES);
  v3f(500., 0., 0.000000);
  v3f(500., 1400., 0.000000);
	glEnd();

  glBegin(GL_LINES);
  v3f(1000., 0., 0.000000);
  v3f(1000., 1400., 0.000000);
	glEnd();

  glBegin(GL_LINES);
  v3f(1500., 0., 0.000000);
  v3f(1500., 1400., 0.000000);
	glEnd();

  glBegin(GL_LINES);
  v3f(2000., 0., 0.000000);
  v3f(2000., 1400., 0.000000);
	glEnd();

	// Triángulos rectos
	// 1
  glBegin(GL_LINES);
  v3f(800., 100., 0.000000);
  v3f(500., 400., 0.000000);
	glEnd();

  glBegin(GL_LINES);
  v3f(800., 750., 0.000000);
  v3f(500., 400., 0.000000);
	glEnd();

	// 2
  glBegin(GL_LINES);
  v3f(1350., 85., 0.000000);
  v3f(1000., 400., 0.000000);
	glEnd();

  glBegin(GL_LINES);
  v3f(1350., 800., 0.000000);
  v3f(1000., 400., 0.000000);
	glEnd();

	// 3
  glBegin(GL_LINES);
  v3f(1850., 75., 0.000000);
  v3f(1500., 400., 0.000000);
	glEnd();

  glBegin(GL_LINES);
  v3f(1900., 800., 0.000000);
  v3f(1500., 400., 0.000000);
	glEnd();

	// 4
  glBegin(GL_LINES);
  v3f(2350., 85., 0.000000);
  v3f(2000., 400., 0.000000);
	glEnd();

  glBegin(GL_LINES);
  v3f(2400., 850., 0.000000);
  v3f(2000., 400., 0.000000);
	glEnd();
}

void drawRoboticLabPlanes () {
	/* LABORATORIO DE ROBÓTICA */
  glLineWidth(2.0f);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

	/* SUELO */
  glColor3f(0.93, 0.93, 0.455);
	// Parte central:
  glBegin(GL_QUADS);
	  v3f(0.000000, 0.000000, 0.000000);
	  v3f(0.000000, 4589.000000, 0.000000);
	  v3f(7925.000000, 4589.000000, 0.000000);
	  v3f(7925.000000, 0.000000, 0.000000);
  glEnd();

	// Lateral de la puerta (1ª parte):
  glBegin(GL_QUADS);
	  v3f(1211.000000, -554.000000, 0.000000);
	  v3f(1211.000000, 5.000000, 0.000000);
	  v3f(5476.000000, 5.000000, 0.000000);
	  v3f(5476.000000, -554.000000, 0.000000);
  glEnd();

	// Lateral de la puerta (trocito intermedio de sobrante defectuoso):
  glBegin(GL_QUADS);
	  v3f(5470.000000, -120.000000, 0.000000);
	  v3f(5470.000000, 10.000000, 0.000000);
	  v3f(6115.000000, 10.000000, 0.000000);
	  v3f(6115.000000, -120.000000, 0.000000);
  glEnd();

	// Lateral de la puerta (2ª parte):
  glBegin(GL_QUADS);
	  v3f(6110.000000, -554.000000, 0.000000);
	  v3f(6110.000000, 5.000000, 0.000000);
	  v3f(7925.000000, 5.000000, 0.000000);
	  v3f(7925.000000, -554.000000, 0.000000);
  glEnd();

	// Lateral enfrente de la puerta (1ª parte):
  glBegin(GL_QUADS);
	  v3f(2419.000000, 4580.000000, 0.000000);
	  v3f(2419.000000, 4879.000000, 0.000000);
	  v3f(5498.000000, 4879.000000, 0.000000);
	  v3f(5498.000000, 4580.000000, 0.000000);
  glEnd();

	// Lateral enfrente de la puerta (2ª parte):
  glBegin(GL_QUADS);
	  v3f(6264.000000, 4580.000000, 0.000000);
	  v3f(6264.000000, 4879.000000, 0.000000);
	  v3f(7925.000000, 4879.000000, 0.000000);
	  v3f(7925.000000, 4580.000000, 0.000000);
  glEnd();

	/* CAMPO DE FUTBOL */
  glColor3f(0., 0.59, 0.);
  glBegin(GL_QUADS); // daremos algo de altura para no causar "conflictos graficos" con el suelo
	  v3f(1560.000000, 596.000000, 25.000000);
	  v3f(1560.000000, 4589.000000, 25.000000);
	  v3f(7925.000000, 4589.000000, 25.000000);
	  v3f(7925.000000, 596.000000, 25.000000);
  glEnd();

	/* PAREDES */

	// Lateral izquierdo:
  glColor3f(0.98, 0.98, 0.98);
  glBegin(GL_QUADS);
	  v3f(0.000000, 0.000000, 3000.000000);
	  v3f(0.000000, 4589.000000, 3000.000000);
	  v3f(0.000000, 4589.000000, 0.000000);
	  v3f(0.000000, 0.000000, 0.000000);
	glEnd();

	// Lateral enfrente de la puerta:
  glBegin(GL_QUADS); // 1ª parte
	  v3f(0.000000, 4589.000000, 0.000000);
	  v3f(0.000000, 4589.000000, 3000.000000);
	  v3f(2419.000000, 4589.000000, 3000.000000);
	  v3f(2419.000000, 4589.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 2ª parte
	  v3f(2419.000000, 4589.000000, 0.000000);
	  v3f(2419.000000, 4589.000000, 3000.000000);
	  v3f(2419.000000, 4879.000000, 3000.000000);
	  v3f(2419.000000, 4879.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 3ª parte
	  v3f(2419.000000, 4879.000000, 0.000000);
	  v3f(2419.000000, 4879.000000, 3000.000000);
	  v3f(5476.000000, 4879.000000, 3000.000000);
	  v3f(5476.000000, 4879.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 4ª parte
	  v3f(5476.000000, 4879.000000, 0.000000);
	  v3f(5476.000000, 4879.000000, 3000.000000);
	  v3f(5476.000000, 4589.000000, 3000.000000);
	  v3f(5476.000000, 4589.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 5ª parte
	  v3f(5476.000000, 4589.000000, 0.000000);
	  v3f(5476.000000, 4589.000000, 3000.000000);
	  v3f(6264.000000, 4589.000000, 3000.000000);
	  v3f(6264.000000, 4589.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 6ª parte
	  v3f(6264.000000, 4589.000000, 0.000000);
	  v3f(6264.000000, 4589.000000, 3000.000000);
	  v3f(6264.000000, 4879.000000, 3000.000000);
	  v3f(6264.000000, 4879.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 6ª parte
	  v3f(6264.000000, 4879.000000, 0.000000);
	  v3f(6264.000000, 4879.000000, 3000.000000);
	  v3f(7925.000000, 4879.000000, 3000.000000);
	  v3f(7925.000000, 4879.000000, 0.000000);
	glEnd();

	// Lateral a la derecha de la puerta:
  glBegin(GL_QUADS); // 6ª parte
	  v3f(7925.000000, 4879.000000, 0.000000);
	  v3f(7925.000000, 4879.000000, 3000.000000);
	  v3f(7925.000000, -554.000000, 3000.000000);
	  v3f(7925.000000, -554.000000, 0.000000);
	glEnd();

	// Lateral de la puerta:
  glBegin(GL_QUADS); // 1ª parte
	  v3f(7925.000000, -554.000000, 0.000000);
	  v3f(7925.000000, -554.000000, 3000.000000);
	  v3f(6110.000000, -554.000000, 3000.000000);
	  v3f(6110.000000, -554.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 2ª parte
	  v3f(6110.000000, -554.000000, 0.000000);
	  v3f(6110.000000, -554.000000, 3000.000000);
	  v3f(6110.000000, -119.000000, 3000.000000);
	  v3f(6110.000000, -119.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 3ª parte
	  v3f(6110.000000, -119.000000, 0.000000);
	  v3f(6110.000000, -119.000000, 3000.000000);
	  v3f(5476.000000, -119.000000, 3000.000000);
	  v3f(5476.000000, -119.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 4ª parte
	  v3f(5476.000000, -554.000000, 0.000000);
	  v3f(5476.000000, -554.000000, 3000.000000);
	  v3f(1211.000000, -554.000000, 3000.000000);
	  v3f(1211.000000, -554.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 5ª parte
	  v3f(1211.000000, -554.000000, 0.000000);
	  v3f(1211.000000, -554.000000, 3000.000000);
	  v3f(1211.000000, 0.000000, 3000.000000);
	  v3f(1211.000000, 0.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 6ª parte
	  v3f(1211.000000, 0.000000, 0.000000);
	  v3f(1211.000000, 0.000000, 3000.000000);
	  v3f(0.000000, 0.000000, 3000.000000);
	  v3f(0.000000, 0.000000, 0.000000);
	glEnd();

	/* PUERTA */
  glColor3f(0.67, 0., 0.);
  glBegin(GL_QUADS);
	  v3f(4613.000000, -554.000000, 0.000000);
	  v3f(4613.000000, -554.000000, 2030.000000);
	  v3f(3879.000000, -554.000000, 2030.000000);
	  v3f(3879.000000, -554.000000, 0.000000);
  glEnd();

	/* VENTANA */
  glColor3f(0.5, 0.5, 1.);
  glBegin(GL_QUADS);
	  v3f(2585.000000, 4879.000000, 835.000000);
	  v3f(2585.000000, 4879.000000, 2532.000000);
	  v3f(5335.000000, 4879.000000, 2532.000000);
	  v3f(5335.000000, 4879.000000, 835.000000);
  glEnd();
}

void drawHallLines () {
	/* el pasillo mide (15x1.99)+1.50 cm. de largo y 1.90 cm. de ancho */

	/* SUELO */
  glBegin(GL_LINES);
	  v3f(0.0, -554.0, 0.0);
	  v3f(31350.0, -554.0, 0.0);
  glEnd();

  glBegin(GL_LINES);
	  v3f(31350.0, -554.0, 0.0);
	  v3f(31350.0, -2454.0, 0.0);
  glEnd();

  glBegin(GL_LINES);
	  v3f(31350.0, -2454.0, 0.0);
	  v3f(0.0, -2454.0, 0.0);
  glEnd();

  glBegin(GL_LINES);
	  v3f(0.0, -2454.0, 0.0);
	  v3f(0.0, -554.0, 0.0);
  glEnd();

	/* TECHO DEL PASILLO */
  glBegin(GL_LINES);
	  v3f(0.0, -554.0, 3000.0);
	  v3f(31350.0, -554.0, 3000.0);
  glEnd();

  glBegin(GL_LINES);
	  v3f(31350.0, -554.0, 3000.0);
	  v3f(31350.0, -2454.0, 3000.0);
  glEnd();

  glBegin(GL_LINES);
	  v3f(31350.0, -2454.0, 3000.0);
	  v3f(0.0, -2454.0, 3000.0);
  glEnd();

  glBegin(GL_LINES);
	  v3f(0.0, -2454.0, 3000.0);
	  v3f(0.0, -554.0, 3000.0);
  glEnd();

	/* ESQUINAS */
  glBegin(GL_LINES);
	  v3f(0.0, -554.0, 0.0);
	  v3f(0.0, -554.0, 3000.0);
  glEnd();

  glBegin(GL_LINES);
	  v3f(0.0, -2454.0, 0.0);
	  v3f(0.0, -2454.0, 3000.0);
  glEnd();

  glBegin(GL_LINES);
	  v3f(31350.0, -554.0, 0.0);
	  v3f(31350.0, -554.0, 3000.0);
  glEnd();

  glBegin(GL_LINES);
	  v3f(31350.0, -2454.0, 0.0);
	  v3f(31350.0, -2454.0, 3000.0);
  glEnd();
}

void drawRoboticLabLines () {
  glColor3f(1., 1., 1.);
  glBegin(GL_LINES);
  v3f(0.000000, 0.000000, 3000.000000);
  v3f(0.000000, 4589.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(0.000000, 0.000000, 3000.000000);
  v3f(1211.000000, 0.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(1211.000000, 0.000000, 3000.000000);
  v3f(1211.000000, -554.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(1211.000000, -554.000000, 3000.000000);
  v3f(5476.000000, -554.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5476.000000, -554.000000, 3000.000000);
  v3f(5476.000000, -119.000000, 3000.000000);
  glEnd();
	
  glBegin(GL_LINES);
  v3f(5476.000000, -119.000000, 3000.000000);
  v3f(6110.000000, -119.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6110.000000, -119.000000, 3000.000000);
  v3f(6110.000000, -554.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6110.000000, -554.000000, 3000.000000);
  v3f(7925.000000, -554.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7925.000000, -554.000000, 3000.000000);
  v3f(7925.000000, 4879.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7925.000000, 4879.000000, 3000.000000);
  v3f(6264.000000, 4879.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6264.000000, 4879.000000, 3000.000000);
  v3f(6264.000000, 4589.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6264.000000, 4589.000000, 3000.000000);
  v3f(5498.000000, 4589.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5498.000000, 4589.000000, 3000.000000);
  v3f(5498.000000, 4879.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5498.000000, 4879.000000, 3000.000000);
  v3f(2419.000000, 4879.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2419.000000, 4879.000000, 3000.000000);
  v3f(2419.000000, 4589.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2419.000000, 4589.000000, 3000.000000);
  v3f(0.000000, 4589.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(0.000000, 0.000000, 30.000000);
  v3f(0.000000, 4589.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(0.000000, 0.000000, 30.000000);
  v3f(1211.000000, 0.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(1211.000000, 0.000000, 30.000000);
  v3f(1211.000000, -554.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(1211.000000, -554.000000, 30.000000);
  v3f(5476.000000, -554.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5476.000000, -554.000000, 30.000000);
  v3f(5476.000000, -119.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5476.000000, -119.000000, 30.000000);
  v3f(6110.000000, -119.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6110.000000, -119.000000, 30.000000);
  v3f(6110.000000, -554.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6110.000000, -554.000000, 30.000000);
  v3f(7925.000000, -554.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7925.000000, -554.000000, 30.000000);
  v3f(7925.000000, 4879.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7925.000000, 4879.000000, 30.000000);
  v3f(6264.000000, 4879.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6264.000000, 4879.000000, 30.000000);
  v3f(6264.000000, 4589.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6264.000000, 4589.000000, 30.000000);
  v3f(5498.000000, 4589.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5498.000000, 4589.000000, 30.000000);
  v3f(5498.000000, 4879.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5498.000000, 4879.000000, 30.000000);
  v3f(2419.000000, 4879.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2419.000000, 4879.000000, 30.000000);
  v3f(2419.000000, 4589.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2419.000000, 4589.000000, 30.000000);
  v3f(0.000000, 4589.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(0.000000, 0.000000, 0.000000);
  v3f(0.000000, 0.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(1211.000000, 0.000000, 0.000000);
  v3f(1211.000000, 0.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(1211.000000, -554.000000, 0.000000);
  v3f(1211.000000, -554.000000, 3000.000000);
  glEnd();

	// LINEAS LIMITROFES PUERTA-------------------
  glColor3f(1., 0.5, 0.);
  glBegin(GL_LINES);
  v3f(3879.000000, -554.000000, 0.000000);
  v3f(3879.000000, -554.000000, 2030.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(3879.000000, -554.000000, 2030.000000);
  v3f(4613.000000, -554.000000, 2030.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(4613.000000, -554.000000, 2030.000000);
  v3f(4613.000000, -554.000000, 0.000000);
  glEnd();
	//--------------------------------------------

  glColor3f(1., 1., 1.);
  glBegin(GL_LINES);
  v3f(5476.000000, -554.000000, 0.000000);
  v3f(5476.000000, -554.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5476.000000, -119.000000, 0.000000);
  v3f(5476.000000, -119.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6110.000000, -119.000000, 0.000000);
  v3f(6110.000000, -119.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6110.000000, -554.000000, 0.000000);
  v3f(6110.000000, -554.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7925.000000, -554.000000, 0.000000);
  v3f(7925.000000, -554.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7925.000000, 4879.000000, 0.000000);
  v3f(7925.000000, 4879.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6264.000000, 4879.000000, 0.000000);
  v3f(6264.000000, 4879.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6264.000000, 4589.000000, 0.000000);
  v3f(6264.000000, 4589.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5498.000000, 4589.000000, 0.000000);
  v3f(5498.000000, 4589.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5498.000000, 4879.000000, 0.000000);
  v3f(5498.000000, 4879.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2419.000000, 4879.000000, 0.000000);
  v3f(2419.000000, 4879.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2419.000000, 4589.000000, 0.000000);
  v3f(2419.000000, 4589.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(0.000000, 4589.000000, 0.000000);
  v3f(0.000000, 4589.000000, 3000.000000);
  glEnd();

	// LINEAS LIMITROFES DE LA VENTANA-----------
  glColor3f(1., 0.5, 0.);
  glBegin(GL_LINES);
  v3f(2585.000000, 4879.000000, 835.000000);
  v3f(5335.000000, 4879.000000, 835.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2585.000000, 4879.000000, 2532.000000);
  v3f(5335.000000, 4879.000000, 2532.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2585.000000, 4879.000000, 835.000000);
  v3f(2585.000000, 4879.000000, 2532.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5335.000000, 4879.000000, 835.000000);
  v3f(5335.000000, 4879.000000, 2532.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(3960.000000, 4879.000000, 835.000000);
  v3f(3960.000000, 4879.000000, 2532.000000);
  glEnd();
	//--------------------------------------------

  glColor3f(1., 1., 1.);
	// LINEAS LIMITROFES DEL CAMPO FUTBOL
  glBegin(GL_LINES);
  v3f(1560.000000, 596.000000, 30.000000);
  v3f(1560.000000, 4589.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5498.000000, 4589.000000, 30.000000);
  v3f(2419.000000, 4589.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(1560.000000, 596.000000, 30.000000);
  v3f(7925.000000, 596.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6264.000000, 4589.000000, 30.000000);
  v3f(7925.000000, 4589.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2160.000000, 796.000000, 30.000000);
  v3f(2160.000000, 4301.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2160.000000, 796.000000, 30.000000);
  v3f(7560.000000, 796.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2160.000000, 4301.000000, 30.000000);
  v3f(7560.000000, 4301.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7560.000000, 796.000000, 30.000000);
  v3f(7560.000000, 4301.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(4860.000000, 796.000000, 30.000000);
  v3f(4860.000000, 4301.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2160.000000, 2051.000000, 30.000000);
  v3f(2810.000000, 2051.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2810.000000, 2051.000000, 30.000000);
  v3f(2810.000000, 3351.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2810.000000, 3351.000000, 30.000000);
  v3f(2160.000000, 3351.000000, 30.000000 );
  glEnd();

  glBegin(GL_LINES);
  v3f(7560.000000, 2051.000000, 30.000000);
  v3f(6910.000000, 2051.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7560.000000, 3351.000000, 30.000000);
  v3f(6910.000000, 3351.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6910.000000, 2051.000000, 30.000000);
  v3f(6910.000000, 3351.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(4500.000000, 2341.000000, 30.000000);
  v3f(4500.000000, 3061.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5220.000000, 2341.000000, 30.000000);
  v3f(5220.000000, 3061.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(4500.000000, 2341.000000, 30.000000);
  v3f(5220.000000, 2341.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(4500.000000, 3061.000000, 30.000000);
  v3f(5220.000000, 3061.000000, 30.000000);
  glEnd();
}

void drawRoboticLabGround () {
  glLineWidth(2.0f);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

	glColor3f (1.,1.,1.);
  glBegin(GL_LINES);
  v3f(0.000000, 0.000000, 0.000000);
  v3f(0.000000, 4589.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(0.000000, 0.000000, 0.000000);
  v3f(1211.000000, 0.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(1211.000000, 0.000000, 0.000000);
  v3f(1211.000000, -554.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(1211.000000, -554.000000, 0.000000);
  v3f(5476.000000, -554.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5476.000000, -554.000000, 0.000000);
  v3f(5476.000000, -119.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5476.000000, -119.000000, 0.000000);
  v3f(6110.000000, -119.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6110.000000, -119.000000, 0.000000);
  v3f(6110.000000, -554.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6110.000000, -554.000000, 0.000000);
  v3f(7925.000000, -554.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7925.000000, -554.000000, 0.000000);
  v3f(7925.000000, 4879.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7925.000000, 4879.000000, 0.000000);
  v3f(6264.000000, 4879.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6264.000000, 4879.000000, 0.000000);
  v3f(6264.000000, 4589.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6264.000000, 4589.000000, 0.000000);
  v3f(5498.000000, 4589.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5498.000000, 4589.000000, 0.000000);
  v3f(5498.000000, 4879.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5498.000000, 4879.000000, 0.000000);
  v3f(2419.000000, 4879.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2419.000000, 4879.000000, 0.000000);
  v3f(2419.000000, 4589.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2419.000000, 4589.000000, 0.000000);
  v3f(0.000000, 4589.000000, 0.000000);
  glEnd();

  glColor3f(1., 1., 1.);
	// LINEAS LIMITROFES DEL CAMPO FUTBOL
  glBegin(GL_LINES);
  v3f(1560.000000, 596.000000, 0.000000);
  v3f(1560.000000, 4589.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5498.000000, 4589.000000, 0.000000);
  v3f(2419.000000, 4589.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(1560.000000, 596.000000, 0.000000);
  v3f(7925.000000, 596.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6264.000000, 4589.000000, 0.000000);
  v3f(7925.000000, 4589.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2160.000000, 796.000000, 0.000000);
  v3f(2160.000000, 4301.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2160.000000, 796.000000, 0.000000);
  v3f(7560.000000, 796.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2160.000000, 4301.000000, 0.000000);
  v3f(7560.000000, 4301.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7560.000000, 796.000000, 0.000000);
  v3f(7560.000000, 4301.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(4860.000000, 796.000000, 0.000000);
  v3f(4860.000000, 4301.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2160.000000, 2051.000000, 0.000000);
  v3f(2810.000000, 2051.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2810.000000, 2051.000000, 0.000000);
  v3f(2810.000000, 3351.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2810.000000, 3351.000000, 0.000000);
  v3f(2160.000000, 3351.000000, 0.000000 );
  glEnd();

  glBegin(GL_LINES);
  v3f(7560.000000, 2051.000000, 0.000000);
  v3f(6910.000000, 2051.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7560.000000, 3351.000000, 0.000000);
  v3f(6910.000000, 3351.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6910.000000, 2051.000000, 0.000000);
  v3f(6910.000000, 3351.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(4500.000000, 2341.000000, 0.000000);
  v3f(4500.000000, 3061.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5220.000000, 2341.000000, 0.000000);
  v3f(5220.000000, 3061.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(4500.000000, 2341.000000, 0.000000);
  v3f(5220.000000, 2341.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(4500.000000, 3061.000000, 0.000000);
  v3f(5220.000000, 3061.000000, 0.000000);
  glEnd();
}


/* We want to know the intersection between a line given by two points (A and B) on 3D, and a plan (the ground, for example). Then, we'll get another point; it'll be our solution */
int linePlaneIntersection (HPoint3D A, HPoint3D B, HPoint3D *intersectionPoint) {
	HPoint3D v;	// Line director vector: it the same to take A or B as origin or destination extrem...

	A.X = A.X;
	A.Y = A.Y;
	A.Z = A.Z;

	B.X = B.X;
	B.Y = B.Y;
	B.Z = B.Z;

	v.X = (B.X - A.X);
	v.Y = (B.Y - A.Y);
	v.Z = (B.Z - A.Z);

	// We'll calculate the groun intersection (Z = 0) on our robot system. Parametric equations:
	intersectionPoint->Z = 0; // intersectionPoint->Z = A.Z + t*v.Z => t = (-A.Z / v.Z)
	t = (-A.Z) / (v.Z);

	intersectionPoint->X = A.X + (t*v.X);
	intersectionPoint->Y = A.Y + (t*v.Y);
}

void pointProjection (HPoint2D point, char **myColor, int puntosExtremos, int mode) {
	float temp;

	if (myColor == mycolorA) {
		myCamera = &robotCamera;
	}
	myActualPoint2D.y = point.x;
	myActualPoint2D.x = 240.-1.-point.y;
	myActualPoint2D.h = 1.;

	// Proyectamos tal punto en 3D sobre el Plano Imagen de nuestra cámara virtual
	backproject(&myActualPoint3D, myActualPoint2D, *myCamera);

	// Coordenadas en 3D de la posicion de la cámara
	cameraPos3D.X = myCamera->position.X;
	cameraPos3D.Y = myCamera->position.Y;
	cameraPos3D.Z = myCamera->position.Z;
	cameraPos3D.H = 1;

	// Distancia entre los puntos myActualPoint3D - cameraPos3D
	distanciaPlanoImagen=(float)sqrt((double)((myActualPoint3D.X-cameraPos3D.X)*(myActualPoint3D.X-cameraPos3D.X)+(myActualPoint3D.Y-cameraPos3D.Y)*(myActualPoint3D.Y-cameraPos3D.Y)+(myActualPoint3D.Z-cameraPos3D.Z)*(myActualPoint3D.Z-cameraPos3D.Z)));
	incremento = ROOM_MAX_X/distanciaPlanoImagen; // distancia "relativa"
	
	// Alargamos tal distancia (lanzamiento del rayo óptico)
	myEndPoint3D.X = ((incremento * myActualPoint3D.X + (1. - incremento) * cameraPos3D.X));
	myEndPoint3D.Y = ((incremento * myActualPoint3D.Y + (1. - incremento) * cameraPos3D.Y));
	myEndPoint3D.Z = ((incremento * myActualPoint3D.Z + (1. - incremento) * cameraPos3D.Z));
	myEndPoint3D.H = 1.;

	linePlaneIntersection (myActualPoint3D, cameraPos3D, &intersectionPoint);

	if (puntosExtremos == TRUE) {
		if (primerPunto == FALSE) { // no está establecido aun el primer punto
			primerPunto3D.X = intersectionPoint.X;
			primerPunto3D.Y = intersectionPoint.Y;
			primerPunto3D.Z = intersectionPoint.Z;
			primerPunto = TRUE;
		} else if (segundoPunto == FALSE) { // no está establecido aun el segundo punto
			segundoPunto3D.X = intersectionPoint.X;
			segundoPunto3D.Y = intersectionPoint.Y;
			segundoPunto3D.Z = intersectionPoint.Z;
			segundoPunto = TRUE;
		} else if (tercerPunto == FALSE) { // no está establecido aun el segundo punto
			tercerPunto3D.X = intersectionPoint.X;
			tercerPunto3D.Y = intersectionPoint.Y;
			tercerPunto3D.Z = intersectionPoint.Z;
			tercerPunto = TRUE;
		} else if (cuartoPunto == FALSE) { // no está establecido aun el segundo punto
			cuartoPunto3D.X = intersectionPoint.X;
			cuartoPunto3D.Y = intersectionPoint.Y;
			cuartoPunto3D.Z = intersectionPoint.Z;
			cuartoPunto = TRUE;
		}
	}
	else {
		if (actualGroundLine == MAX_LINES_IN_MEMORY)
			actualGroundLine = 0;

		if (comeFromCenter)
			groundLines3D[actualGroundLine].idColor = 1;
		else if (last_full_movement==left)
			groundLines3D[actualGroundLine].idColor = 2;
		else if (last_full_movement==right)
			groundLines3D[actualGroundLine].idColor = 3;

		groundLines3D[actualGroundLine].X = intersectionPoint.X;
		groundLines3D[actualGroundLine].Y = intersectionPoint.Y;
		groundLines3D[actualGroundLine].Z = intersectionPoint.Z;
		groundLines3D[actualGroundLine].H = 1.;

		actualGroundLine ++;


/*		if (actualGroundPoint == ((SIFNTSC_COLUMNS*SIFNTSC_ROWS) - 1))
			actualGroundPoint = 0;

		if (comeFromCenter)
			groundPoints[actualGroundPoint].idColor = 1;
		else if (last_full_movement==left)
			groundPoints[actualGroundPoint].idColor = 2;
		else if (last_full_movement==right)
			groundPoints[actualGroundPoint].idColor = 3;

		groundPoints[actualGroundPoint].X = intersectionPoint.X;
		groundPoints[actualGroundPoint].Y = intersectionPoint.Y;
		groundPoints[actualGroundPoint].Z = intersectionPoint.Z;

		actualGroundPoint ++;*/
	}
}

inline void drawOpticalRays (TPinHoleCamera *cam) {
  HPoint3D a3A;
  HPoint2D a;

	glColor3f( 0.7, 0.7, 0.7 );

	cameraPos3D.X=0.;
	cameraPos3D.Y=0.;
	cameraPos3D.Z=0.;
	cameraPos3D.H=1.;

	/* optical axis of camera */
	a.x = SIFNTSC_ROWS-1;
	a.y = SIFNTSC_COLUMNS-1;
	a.h = 1.;
	backproject (&a3A, a, *cam);

	distanciaPlanoImagen=(float)sqrt((double)((a3A.X-cameraPos3D.X)*(a3A.X-cameraPos3D.X)+(a3A.Y-cameraPos3D.Y)*(a3A.Y-cameraPos3D.Y)+(a3A.Z-cameraPos3D.Z)*(a3A.Z-cameraPos3D.Z)));
	incremento = ROOM_MAX_X/distanciaPlanoImagen; // distancia "relativa"

	a3A.X = incremento * a3A.X + (1. - incremento) * cameraPos3D.X;
	a3A.Y = incremento * a3A.Y + (1. - incremento) * cameraPos3D.Y;
	a3A.Z = incremento * a3A.Z + (1. - incremento) * cameraPos3D.Z;
	a3A.H = 1.;

	glBegin(GL_LINES);
		v3f( a3A.X, a3A.Y, a3A.Z );
		v3f( cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
	glEnd();

	/**/
	a.x = 0;
	a.y = 0;
	a.h = 1.;
	backproject (&a3A, a, *cam);

	distanciaPlanoImagen=(float)sqrt((double)((a3A.X-cameraPos3D.X)*(a3A.X-cameraPos3D.X)+(a3A.Y-cameraPos3D.Y)*(a3A.Y-cameraPos3D.Y)+(a3A.Z-cameraPos3D.Z)*(a3A.Z-cameraPos3D.Z)));
	incremento = ROOM_MAX_X/distanciaPlanoImagen; // distancia "relativa"

	a3A.X = incremento * a3A.X + (1. - incremento) * cameraPos3D.X;
	a3A.Y = incremento * a3A.Y + (1. - incremento) * cameraPos3D.Y;
	a3A.Z = incremento * a3A.Z + (1. - incremento) * cameraPos3D.Z;
	a3A.H = 1.;

	glBegin(GL_LINES);
		v3f( a3A.X, a3A.Y, a3A.Z );
		v3f( cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
	glEnd();

	/**/
	a.x = SIFNTSC_ROWS-1;
	a.y = 0;
	a.h = 1.;
	backproject (&a3A, a, *cam);

	distanciaPlanoImagen=(float)sqrt((double)((a3A.X-cameraPos3D.X)*(a3A.X-cameraPos3D.X)+(a3A.Y-cameraPos3D.Y)*(a3A.Y-cameraPos3D.Y)+(a3A.Z-cameraPos3D.Z)*(a3A.Z-cameraPos3D.Z)));
	incremento = ROOM_MAX_X/distanciaPlanoImagen; // distancia "relativa"

	a3A.X = incremento * a3A.X + (1. - incremento) * cameraPos3D.X;
	a3A.Y = incremento * a3A.Y + (1. - incremento) * cameraPos3D.Y;
	a3A.Z = incremento * a3A.Z + (1. - incremento) * cameraPos3D.Z;
	a3A.H = 1.;

	glBegin(GL_LINES);
		v3f( a3A.X, a3A.Y, a3A.Z );
		v3f( cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
	glEnd();

	/**/
	a.x = 0;
	a.y = SIFNTSC_COLUMNS-1;
	a.h = 1.;
	backproject (&a3A, a, *cam);

	distanciaPlanoImagen=(float)sqrt((double)((a3A.X-cameraPos3D.X)*(a3A.X-cameraPos3D.X)+(a3A.Y-cameraPos3D.Y)*(a3A.Y-cameraPos3D.Y)+(a3A.Z-cameraPos3D.Z)*(a3A.Z-cameraPos3D.Z)));
	incremento = ROOM_MAX_X/distanciaPlanoImagen; // distancia "relativa"

	a3A.X = incremento * a3A.X + (1. - incremento) * cameraPos3D.X;
	a3A.Y = incremento * a3A.Y + (1. - incremento) * cameraPos3D.Y;
	a3A.Z = incremento * a3A.Z + (1. - incremento) * cameraPos3D.Z;
	a3A.H = 1.;

	glBegin(GL_LINES);
		v3f( a3A.X, a3A.Y, a3A.Z );
		v3f( cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
	glEnd();
}

inline void drawCam(TPinHoleCamera *cam, int b)
{
  HPoint3D a3A, a3B, a3C, a3D;
  HPoint2D a;

/*	if (b==2){
		glColor3f( 0.7, 0.7, 0.7 );
	} else if (b==3){
		glColor3f( 0.7, 0.7, 0.7 );
	} else if (b==4){
		glColor3f( 0.7, 0.7, 0.7 );
	} else if (b==5){
		glColor3f( 0.7, 0.7, 0.7 );
	}
*/
	glColor3f( 0.7, 0.7, 0.7 );

	cameraPos3D.X=cam->position.X;
	cameraPos3D.Y=cam->position.Y;
	cameraPos3D.Z=cam->position.Z;
	cameraPos3D.H=cam->position.H;

	/* optical axis of camera */
	if ((mycolorA!=NULL) && (cam == &robotCamera)) {
		a.x = SIFNTSC_ROWS-1;
		a.y = SIFNTSC_COLUMNS-1;
		a.h = 1.;
		//a.y = GRAPHIC_TO_OPTICAL_Y(a.y,a.x);
		//a.x = GRAPHIC_TO_OPTICAL_X(a.y,a.x);
		backproject (&a3A, a, *cam);

		a3A.X = -a3A.X;
		a3A.Y = -a3A.Y;
		a3A.Z = -a3A.Z;

		distanciaPlanoImagen=(float)sqrt((double)((a3A.X-cameraPos3D.X)*(a3A.X-cameraPos3D.X)+(a3A.Y-cameraPos3D.Y)*(a3A.Y-cameraPos3D.Y)+(a3A.Z-cameraPos3D.Z)*(a3A.Z-cameraPos3D.Z)));
		incremento = ROOM_MAX_X/distanciaPlanoImagen; // distancia "relativa"

		a3A.X = incremento * a3A.X + (1. - incremento) * cameraPos3D.X;
		a3A.Y = incremento * a3A.Y + (1. - incremento) * cameraPos3D.Y;
		a3A.Z = incremento * a3A.Z + (1. - incremento) * cameraPos3D.Z;
		a3A.H = 1.;

		glBegin(GL_LINES);
			v3f( a3A.X, a3A.Y, a3A.Z );
			v3f( cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
		glEnd();

		/**/
		a.x = 0;
		a.y = 0;
		a.h = 1.;
		//a.y = GRAPHIC_TO_OPTICAL_Y(a.y,a.x);
		//a.x = GRAPHIC_TO_OPTICAL_X(a.y,a.x);

		backproject (&a3A, a, *cam);

		distanciaPlanoImagen=(float)sqrt((double)((a3A.X-cameraPos3D.X)*(a3A.X-cameraPos3D.X)+(a3A.Y-cameraPos3D.Y)*(a3A.Y-cameraPos3D.Y)+(a3A.Z-cameraPos3D.Z)*(a3A.Z-cameraPos3D.Z)));
		incremento = ROOM_MAX_X/distanciaPlanoImagen; // distancia "relativa"

		a3A.X = incremento * a3A.X + (1. - incremento) * cameraPos3D.X;
		a3A.Y = incremento * a3A.Y + (1. - incremento) * cameraPos3D.Y;
		a3A.Z = incremento * a3A.Z + (1. - incremento) * cameraPos3D.Z;
		a3A.H = 1.;

		glBegin(GL_LINES);
			v3f( a3A.X, a3A.Y, a3A.Z );
			v3f( cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
		glEnd();

		/**/
		a.x = SIFNTSC_ROWS-1;
		a.y = 0;
		a.h = 1.;
		//a.y = GRAPHIC_TO_OPTICAL_Y(a.y,a.x);
		//a.x = GRAPHIC_TO_OPTICAL_X(a.y,a.x);

		backproject (&a3A, a, *cam);

		a3A.X = -a3A.X;
		a3A.Y = -a3A.Y;
		a3A.Z = -a3A.Z;

		distanciaPlanoImagen=(float)sqrt((double)((a3A.X-cameraPos3D.X)*(a3A.X-cameraPos3D.X)+(a3A.Y-cameraPos3D.Y)*(a3A.Y-cameraPos3D.Y)+(a3A.Z-cameraPos3D.Z)*(a3A.Z-cameraPos3D.Z)));
		incremento = ROOM_MAX_X/distanciaPlanoImagen; // distancia "relativa"

		a3A.X = incremento * a3A.X + (1. - incremento) * cameraPos3D.X;
		a3A.Y = incremento * a3A.Y + (1. - incremento) * cameraPos3D.Y;
		a3A.Z = incremento * a3A.Z + (1. - incremento) * cameraPos3D.Z;
		a3A.H = 1.;

		glBegin(GL_LINES);
			v3f( a3A.X, a3A.Y, a3A.Z );
			v3f( cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
		glEnd();

		/**/
		a.x = 0;
		a.y = SIFNTSC_COLUMNS-1;
		a.h = 1.;
		//a.y = GRAPHIC_TO_OPTICAL_Y(a.y,a.x);
		//a.x = GRAPHIC_TO_OPTICAL_X(a.y,a.x);

		backproject (&a3A, a, *cam);

		a3A.X = -a3A.X;
		a3A.Y = -a3A.Y;
		a3A.Z = -a3A.Z;

		distanciaPlanoImagen=(float)sqrt((double)((a3A.X-cameraPos3D.X)*(a3A.X-cameraPos3D.X)+(a3A.Y-cameraPos3D.Y)*(a3A.Y-cameraPos3D.Y)+(a3A.Z-cameraPos3D.Z)*(a3A.Z-cameraPos3D.Z)));
		incremento = ROOM_MAX_X/distanciaPlanoImagen; // distancia "relativa"

		a3A.X = incremento * a3A.X + (1. - incremento) * cameraPos3D.X;
		a3A.Y = incremento * a3A.Y + (1. - incremento) * cameraPos3D.Y;
		a3A.Z = incremento * a3A.Z + (1. - incremento) * cameraPos3D.Z;
		a3A.H = 1.;

		glBegin(GL_LINES);
			v3f( a3A.X, a3A.Y, a3A.Z );
			v3f( cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
		glEnd();
	} // fin if (dibujamos los rayos opticos, si estamos en la cámara oportuna...)

	/* FieldOfView of camera */
	a.x = SIFNTSC_ROWS - 1.;
	a.y = SIFNTSC_COLUMNS - 1.;
	a.h = 1.;
	backproject(&a3A, a, *cam);
	a.x = 0.;
	a.y = SIFNTSC_COLUMNS - 1.;
	a.h = 1.;
	backproject(&a3B, a, *cam);
	a.x = 0.;
	a.y = 0.;
	a.h = 1.;
	backproject(&a3C, a, *cam);
	a.x = SIFNTSC_ROWS - 1.;
	a.y = 0.;
	a.h = 1.;
	backproject(&a3D, a, *cam);

	// first triangle
	glBegin(GL_LINES);
		glVertex3f(cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
		glVertex3f(a3A.X, a3A.Y, a3A.Z );
	glEnd();

	glBegin(GL_LINES);
		glVertex3f(a3A.X, a3A.Y, a3A.Z );
		glVertex3f(a3B.X, a3B.Y, a3B.Z );
	glEnd ();

	glBegin(GL_LINES);
		glVertex3f(a3B.X, a3B.Y, a3B.Z );
		glVertex3f(cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
	glEnd ();

	glColor3f( 0.3, 0.3, 0.3 );
	// second triangle
	glBegin(GL_LINES);
		glVertex3f(cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
		glVertex3f(a3B.X, a3B.Y, a3B.Z );
	glEnd();

	glBegin(GL_LINES);
		glVertex3f(a3B.X, a3B.Y, a3B.Z );
		glVertex3f(a3C.X, a3C.Y, a3C.Z );
	glEnd();

	glBegin(GL_LINES);
		glVertex3f(a3C.X, a3C.Y, a3C.Z );
		glVertex3f(cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
	glEnd();

	glColor3f( 0.7, 0.7, 0.7 );
	// third triangle
	glBegin(GL_LINES);
		glVertex3f(cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
		glVertex3f(a3C.X, a3C.Y, a3C.Z );
	glEnd();

	glBegin(GL_LINES);
		glVertex3f(a3C.X, a3C.Y, a3C.Z );
		glVertex3f(a3D.X, a3D.Y, a3D.Z );
	glEnd();

	glBegin(GL_LINES);
		glVertex3f(a3D.X, a3D.Y, a3D.Z );
		glVertex3f(cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
	glEnd();

	glColor3f( 0.3, 0.3, 0.3 );
	// last triangle
	glBegin(GL_LINES);
		glVertex3f(cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
		glVertex3f(a3D.X, a3D.Y, a3D.Z );
	glEnd();

	glBegin(GL_LINES);
		glVertex3f(a3D.X, a3D.Y, a3D.Z );
		glVertex3f(a3A.X, a3A.Y, a3A.Z );
	glEnd();

	glBegin(GL_LINES);
		glVertex3f(a3A.X, a3A.Y, a3A.Z );
		glVertex3f(cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
	glEnd();

	glColor3f( 0.1, 0.1, 0.1 );
	// square
	glBegin(GL_LINES);
		glVertex3f(a3A.X, a3A.Y, a3A.Z );
		glVertex3f(a3D.X, a3D.Y, a3D.Z );
	glEnd();

	glBegin(GL_LINES);
		glVertex3f(a3D.X, a3D.Y, a3D.Z );
		glVertex3f(a3C.X, a3C.Y, a3C.Z );
	glEnd();

	glBegin(GL_LINES);
		glVertex3f(a3C.X, a3C.Y, a3C.Z );
		glVertex3f(a3B.X, a3B.Y, a3B.Z );
	glEnd();

	glBegin(GL_LINES);
		glVertex3f(a3B.X, a3B.Y, a3B.Z );
		glVertex3f(a3A.X, a3A.Y, a3A.Z );
	glEnd();
}

void pixel2Optical (HPoint2D *p) {
	float aux;

	aux = p->x;
	p->x = 240-1-p->y;
	p->y = aux;		
}

void getCameraWorldPos () {
	int i, j;
	float actualPan, actualTilt;
	gsl_matrix *robotRT, *baseCuelloRT, *alturaEjeTiltRT, *ejeTiltRT, *camaraRT, *temp1, *temp2, *temp3, *temp4, *foaRel, *foaAbs, *myComodin, *myComodin2;
/*
	// pasamos ángulos del cuello a radianes, para hallar posteriormente los senos y eso...
	actualPan = gtk_range_get_value(glade_xml_get_widget(xml, "sliderPAN_ANGLE"));
	actualTilt = gtk_range_get_value(glade_xml_get_widget(xml, "sliderTILT_ANGLE"));

	sliderPANTILT_BASE_HEIGHT = gtk_range_get_value(glade_xml_get_widget(xml, "sliderPANTILT_BASE_HEIGHT"));
	sliderISIGHT_OPTICAL_CENTER = gtk_range_get_value(glade_xml_get_widget(xml, "sliderISIGHT_OPTICAL_CENTER"));
	sliderTILT_HEIGHT = gtk_range_get_value(glade_xml_get_widget(xml, "sliderTILT_HEIGHT"));
	sliderCAMERA_TILT_HEIGHT = gtk_range_get_value(glade_xml_get_widget(xml, "sliderCAMERA_TILT_HEIGHT"));
	sliderPANTILT_BASE_X = gtk_range_get_value(glade_xml_get_widget(xml, "sliderPANTILT_BASE_X"));
*/
	actualPan = *mypan_angle;
	actualPan=actualPan*PI/180.0;
	actualTilt=(*mytilt_angle)*PI/180.0;

	//printf ("sliderPantiltBaseX = %f\n, panAngle = %f, tiltAngle = %f", sliderPANTILT_BASE_X, panAngle, tiltAngle);

  robotRT = gsl_matrix_calloc(4,4);
  baseCuelloRT = gsl_matrix_calloc(4,4);
  alturaEjeTiltRT = gsl_matrix_calloc(4,4);
  ejeTiltRT = gsl_matrix_calloc(4,4);
  camaraRT = gsl_matrix_calloc(4,4);
  temp1 = gsl_matrix_calloc(4,4);
  temp2 = gsl_matrix_calloc(4,4);
  temp3 = gsl_matrix_calloc(4,4);
  temp4 = gsl_matrix_calloc(4,4);
  myComodin = gsl_matrix_calloc(4,4);
  myComodin2 = gsl_matrix_calloc(4,4);
	foaRel = gsl_matrix_calloc(4,1);
	foaAbs = gsl_matrix_calloc(4,1);
/*
	// 1º - El robot está transladado respecto al (0, 0) absoluto, y rotado respecto al eje Z
	gsl_matrix_set(robotRT,0,0,cos(myencoders[2]));
	gsl_matrix_set(robotRT,0,1,sin(myencoders[2]));
	gsl_matrix_set(robotRT,0,2,0.);
	gsl_matrix_set(robotRT,0,3,myencoders[0]);
	gsl_matrix_set(robotRT,1,0,-sin(myencoders[2]));
	gsl_matrix_set(robotRT,1,1,cos(myencoders[2]));
	gsl_matrix_set(robotRT,1,2,0.);
	gsl_matrix_set(robotRT,1,3,myencoders[1]);
	gsl_matrix_set(robotRT,2,0,0.);
	gsl_matrix_set(robotRT,2,1,0.);
	gsl_matrix_set(robotRT,2,2,1.);
	gsl_matrix_set(robotRT,2,3,0.); // Z de la base robot la considero 0
	gsl_matrix_set(robotRT,3,0,0.);
	gsl_matrix_set(robotRT,3,1,0.);
	gsl_matrix_set(robotRT,3,2,0.);
	gsl_matrix_set(robotRT,3,3,1.0);

	// aquí es donde modificamos los parámetros
	gsl_matrix_set(myComodin,0,0,cos(actualTilt));
	gsl_matrix_set(myComodin,0,1,0.);
	gsl_matrix_set(myComodin,0,2,sin(actualTilt));
	gsl_matrix_set(myComodin,0,3,60.);
	gsl_matrix_set(myComodin,1,0,0.);
	gsl_matrix_set(myComodin,1,1,1.);
	gsl_matrix_set(myComodin,1,2,0.);
	gsl_matrix_set(myComodin,1,3,0.);
	gsl_matrix_set(myComodin,2,0,-sin(actualTilt));
	gsl_matrix_set(myComodin,2,1,0.);
	gsl_matrix_set(myComodin,2,2,cos(actualTilt));
	gsl_matrix_set(myComodin,2,3,250.);
	gsl_matrix_set(myComodin,3,0,0.);
	gsl_matrix_set(myComodin,3,1,0.);
	gsl_matrix_set(myComodin,3,2,0.);
	gsl_matrix_set(myComodin,3,3,1.0);

	gsl_matrix_set(myComodin2,0,0,cos(actualPan));
	gsl_matrix_set(myComodin2,0,1,sin(actualPan));
	gsl_matrix_set(myComodin2,0,2,0.);
	gsl_matrix_set(myComodin2,0,3,-60.);
	gsl_matrix_set(myComodin2,1,0,-sin(actualPan));
	gsl_matrix_set(myComodin2,1,1,cos(actualPan));
	gsl_matrix_set(myComodin2,1,2,0.);
	gsl_matrix_set(myComodin2,1,3,0.);
	gsl_matrix_set(myComodin2,2,0,0.);
	gsl_matrix_set(myComodin2,2,1,0.);
	gsl_matrix_set(myComodin2,2,2,1.);
	gsl_matrix_set(myComodin2,2,3,220.); // Z de la base robot la considero 0
	gsl_matrix_set(myComodin2,3,0,0.);
	gsl_matrix_set(myComodin2,3,1,0.);
	gsl_matrix_set(myComodin2,3,2,0.);
	gsl_matrix_set(myComodin2,3,3,1.0);*/

	// 1º - El robot está transladado respecto al (0, 0) absoluto, y rotado respecto al eje Z
	gsl_matrix_set(robotRT,0,0,cos(myencoders[2]));
	gsl_matrix_set(robotRT,0,1,sin(myencoders[2]));
	gsl_matrix_set(robotRT,0,2,0.);
	gsl_matrix_set(robotRT,0,3,myencoders[0]);
	gsl_matrix_set(robotRT,1,0,-sin(myencoders[2]));
	gsl_matrix_set(robotRT,1,1,cos(myencoders[2]));
	gsl_matrix_set(robotRT,1,2,0.);
	gsl_matrix_set(robotRT,1,3,myencoders[1]);
	gsl_matrix_set(robotRT,2,0,0.);
	gsl_matrix_set(robotRT,2,1,0.);
	gsl_matrix_set(robotRT,2,2,1.);
	gsl_matrix_set(robotRT,2,3,0.); // Z de la base robot la considero 0
	gsl_matrix_set(robotRT,3,0,0.);
	gsl_matrix_set(robotRT,3,1,0.);
	gsl_matrix_set(robotRT,3,2,0.);
	gsl_matrix_set(robotRT,3,3,1.0);

	// 2º - La base del cuello está transladado en Z y en X respecto a la base del robot (considerada en el suelo)
	gsl_matrix_set(baseCuelloRT,0,0,cos(0.));
	gsl_matrix_set(baseCuelloRT,0,1,sin(0.));
	gsl_matrix_set(baseCuelloRT,0,2,0.);
	gsl_matrix_set(baseCuelloRT,0,3,sliderPANTILT_BASE_X);
	gsl_matrix_set(baseCuelloRT,1,0,-sin(0.));
	gsl_matrix_set(baseCuelloRT,1,1,cos(0.));
	gsl_matrix_set(baseCuelloRT,1,2,0.);
	gsl_matrix_set(baseCuelloRT,1,3,PANTILT_BASE_Y);
	gsl_matrix_set(baseCuelloRT,2,0,0.);
	gsl_matrix_set(baseCuelloRT,2,1,0.);
	gsl_matrix_set(baseCuelloRT,2,2,1.);
	gsl_matrix_set(baseCuelloRT,2,3,sliderPANTILT_BASE_HEIGHT); // desde el suelo a la base
	gsl_matrix_set(baseCuelloRT,3,0,0.);
	gsl_matrix_set(baseCuelloRT,3,1,0.);
	gsl_matrix_set(baseCuelloRT,3,2,0.);
	gsl_matrix_set(baseCuelloRT,3,3,1.0);

	// 3º - El eje de tilt está transladado en Z respecto a la base del cuello, y rotado respecto al eje Z
	gsl_matrix_set(alturaEjeTiltRT,0,0,cos(actualPan));
	gsl_matrix_set(alturaEjeTiltRT,0,1,-sin(actualPan));
	gsl_matrix_set(alturaEjeTiltRT,0,2,0.);
	gsl_matrix_set(alturaEjeTiltRT,0,3,0.);
	gsl_matrix_set(alturaEjeTiltRT,1,0,sin(actualPan));
	gsl_matrix_set(alturaEjeTiltRT,1,1,cos(actualPan));
	gsl_matrix_set(alturaEjeTiltRT,1,2,0.);
	gsl_matrix_set(alturaEjeTiltRT,1,3,0.);
	gsl_matrix_set(alturaEjeTiltRT,2,0,0.);
	gsl_matrix_set(alturaEjeTiltRT,2,1,0.);
	gsl_matrix_set(alturaEjeTiltRT,2,2,1.);
	gsl_matrix_set(alturaEjeTiltRT,2,3,sliderTILT_HEIGHT);
	gsl_matrix_set(alturaEjeTiltRT,3,0,0.);
	gsl_matrix_set(alturaEjeTiltRT,3,1,0.);
	gsl_matrix_set(alturaEjeTiltRT,3,2,0.);
	gsl_matrix_set(alturaEjeTiltRT,3,3,1.0);

	// 4º - El eje de tilt está además rotado respecto al eje Y
	gsl_matrix_set(ejeTiltRT,0,0,cos(actualTilt));
	gsl_matrix_set(ejeTiltRT,0,1,0.);
	gsl_matrix_set(ejeTiltRT,0,2,-sin(actualTilt));
	gsl_matrix_set(ejeTiltRT,0,3,0.);
	gsl_matrix_set(ejeTiltRT,1,0,0.);
	gsl_matrix_set(ejeTiltRT,1,1,1.);
	gsl_matrix_set(ejeTiltRT,1,2,0.);
	gsl_matrix_set(ejeTiltRT,1,3,0.);
	gsl_matrix_set(ejeTiltRT,2,0,sin(actualTilt));
	gsl_matrix_set(ejeTiltRT,2,1,0.);
	gsl_matrix_set(ejeTiltRT,2,2,cos(actualTilt));
	gsl_matrix_set(ejeTiltRT,2,3,0.);
	gsl_matrix_set(ejeTiltRT,3,0,0.);
	gsl_matrix_set(ejeTiltRT,3,1,0.);
	gsl_matrix_set(ejeTiltRT,3,2,0.);
	gsl_matrix_set(ejeTiltRT,3,3,1.0);

	// El centro óptico de la cámara está transladado en X y en Z respecto al eje tilt
	gsl_matrix_set(camaraRT,0,0,cos(0.));
	gsl_matrix_set(camaraRT,0,1,sin(0.));
	gsl_matrix_set(camaraRT,0,2,0.);
	gsl_matrix_set(camaraRT,0,3,sliderISIGHT_OPTICAL_CENTER);
	gsl_matrix_set(camaraRT,1,0,-sin(0.));
	gsl_matrix_set(camaraRT,1,1,cos(0.));
	gsl_matrix_set(camaraRT,1,2,0.);
	gsl_matrix_set(camaraRT,1,3,0.);
	gsl_matrix_set(camaraRT,2,0,0.);
	gsl_matrix_set(camaraRT,2,1,0.);
	gsl_matrix_set(camaraRT,2,2,1.);
	gsl_matrix_set(camaraRT,2,3,sliderCAMERA_TILT_HEIGHT);
	gsl_matrix_set(camaraRT,3,0,0.);
	gsl_matrix_set(camaraRT,3,1,0.);
	gsl_matrix_set(camaraRT,3,2,0.);
	gsl_matrix_set(camaraRT,3,3,1.0);

	gsl_linalg_matmult (robotRT, baseCuelloRT, temp1);
	gsl_linalg_matmult (temp1, alturaEjeTiltRT, temp2);
	gsl_linalg_matmult (temp2, ejeTiltRT, temp3);
	gsl_linalg_matmult (temp3, camaraRT, temp4);

//	gsl_linalg_matmult (robotRT, myComodin, temp1);
//	gsl_linalg_matmult (temp1, myComodin2, temp2);
	gsl_matrix_set(foaRel,0,0,3000.0);
	gsl_matrix_set(foaRel,1,0,0.0);
	gsl_matrix_set(foaRel,2,0,0.0);
	gsl_matrix_set(foaRel,3,0,1.0);

	gsl_linalg_matmult(temp4,foaRel,foaAbs);

	robotCamera.position.X = gsl_matrix_get (temp4, 0, 3);
	robotCamera.position.Y = gsl_matrix_get (temp4, 1, 3);
	robotCamera.position.Z = gsl_matrix_get (temp4, 2, 3);
  robotCamera.foa.X=(float)gsl_matrix_get(foaAbs,0,0);
  robotCamera.foa.Y=(float)gsl_matrix_get(foaAbs,1,0);
  robotCamera.foa.Z=(float)gsl_matrix_get(foaAbs,2,0);

	update_camera_matrix (&robotCamera);

	//printCameraInformation (&robotCamera);

	// Liberación de memoria de matrices
  gsl_matrix_free(robotRT);
  gsl_matrix_free(baseCuelloRT);
  gsl_matrix_free(alturaEjeTiltRT);
  gsl_matrix_free(ejeTiltRT);
  gsl_matrix_free(camaraRT);
  gsl_matrix_free(temp1);
  gsl_matrix_free(temp2);
  gsl_matrix_free(temp3);
  gsl_matrix_free(temp4);
  gsl_matrix_free(foaRel);
  gsl_matrix_free(foaAbs);
  gsl_matrix_free(myComodin);
  gsl_matrix_free(myComodin2);
}


inline void getFoaOnMovement () {
	float myFoaX, myFoaY, myFoaZ;
	float myPosX, myPosY, myPosZ;
	float actualPan, actualTilt, hip;

	actualPan = *mypan_angle;
	actualTilt = *mytilt_angle;
	myPosX = myencoders[1];
	myPosY = myencoders[0];
	myPosZ = 480.; // a capón, yo sé que la cámara está a esa altura

	actualPan=actualPan*PI/180.0;
	actualTilt=actualTilt*PI/180.0;

	hip = myPosZ/tan(actualTilt);
	myFoaY = hip*sin(actualPan);
	myFoaX = sqrt(pow(hip,2) - pow(myFoaY,2));

  // obtener la posición absoluta, contando la posición del robot (posx, posy)
	myFoaX += myPosX;
	myFoaY += myPosY;
	myFoaZ = 0;

  robotCamera.foa.X=myFoaX;
  robotCamera.foa.Y=myFoaY;
	update_camera_matrix (&robotCamera);

	printf ("Foa que adopta la cámara = [%f, %f]\n", robotCamera.foa.X, robotCamera.foa.Y);
}

inline void movePantilt() {  
  *mylongitude_speed=1200*ENCOD_TO_DEG;
  *mylatitude_speed = 0.0;
	//printf ("actualInstant vs. stopInstant = [%f, %f]\n", actualInstant, stopInstant);

	if (comeFromCenter) {
	  if ((last_full_movement==left) && (actualInstant - stopInstant > TIME_TO_LOOK)) { /* Buscamos hacia la izquierda */
	    if ((*mypan_angle > MAXPAN_NEG/4) && (*mypan_angle != MAXPAN_NEG/4)) {
				pantiltStill = FALSE;
	      *mylongitude = *min_pan/4;
	    } else {
				numIterations++;
				if(numIterations==2) {
		      last_full_movement=right;
					comeFromCenter = FALSE;
					counterColor++;
					getCameraWorldPos ();
					stopInstant = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000000;
					numIterations=0;
					pantiltStill = TRUE;
					numFlashes ++;
				}
	    }
	  } else if ((last_full_movement==right) && (actualInstant - stopInstant > TIME_TO_LOOK)) { /* Buscamos hacia la derecha */
	    if ((*mypan_angle < MAXPAN_POS/4) && (*mypan_angle != MAXPAN_POS/4)){
				pantiltStill = FALSE;
	      *mylongitude = *max_pan/4;
	    } else {
				numIterations++;
				if(numIterations==2) {
		      last_full_movement=left;
					comeFromCenter = FALSE;
					counterColor ++;
					getCameraWorldPos ();
					stopInstant = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000000;
					numIterations=0;
					pantiltStill = TRUE;
					numFlashes ++;
				}
	    }
	  }
	} else if (actualInstant - stopInstant > TIME_TO_LOOK) { // venimos de algún extremo, así que vamos al centro ahora
		if ((*mypan_angle) != 0) {			
			pantiltStill = FALSE;
			*mylongitude = 0;
		} else if ((*mypan_angle) == 0) {
			numIterations++;
			if(numIterations==2) {
				comeFromCenter = TRUE;
				counterColor++;
				numIterations++;
				getCameraWorldPos ();
				stopInstant = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000000;
				numIterations=0;
				pantiltStill = TRUE;
				numFlashes ++;
			}
		}
	}
}

void moveInitialTilt () {
  speed_y = VEL_MAX_TILT-((POS_MAX_TILT-DEG_TO_ENCOD*(abs(tiltAngle)))/((POS_MAX_TILT-POS_MIN_TILT)/(VEL_MAX_TILT-VEL_MIN_TILT)));
  speed_x = VEL_MAX_PAN-((POS_MAX_PAN-DEG_TO_ENCOD*(abs(panAngle)))/((POS_MAX_PAN-POS_MIN_PAN)/(VEL_MAX_PAN-VEL_MIN_PAN)));

	if ((*mylatitude_speed) < VEL_MIN_TILT)
		*mylatitude_speed = VEL_MIN_TILT*4;
	else *mylatitude_speed = speed_y;
	if ((*mylongitude_speed) < VEL_MIN_PAN)
		*mylongitude_speed = VEL_MIN_PAN*4;
	else *mylongitude_speed = speed_x;

	*mylatitude = tiltAngle;
	*mylongitude = panAngle;
	if (((*mypan_angle) < ((*mylongitude) + 1.)) && ((*mypan_angle) > ((*mylongitude) - 1.)) && ((*mytilt_angle) < ((*mylatitude) + 1.)) && ((*mytilt_angle) > ((*mylatitude) - 1.))) {
		completedMovement = TRUE;
		pantiltStill = TRUE;
	}	else
		completedMovement = FALSE;
}

inline void processImage () {
	int i, j, c, row;

	for(i=0; i<SIFNTSC_ROWS*SIFNTSC_COLUMNS; i++) {
		c=i%SIFNTSC_COLUMNS;
		row=i/SIFNTSC_COLUMNS;
		j=row*SIFNTSC_COLUMNS+c;

		if (mycolorA!= NULL) {
		  image[i*3]=(*mycolorA)[j*3+2];
		  image[i*3+1]=(*mycolorA)[j*3+1];
		  image[i*3+2]=(*mycolorA)[j*3];
		}
	}

	if (mycolorA!= NULL)
	  imageA->image = (char *) *mycolorA; // asignamos directamente lo que viene de mycolorA

	cannyFilter (); // aquí hacemos todo el procesado de imagen y retroproyección de líneas en 3D

	prepare2draw(imageAfiltered, filteredImageRGB);
	prepare2draw(imageAgrounded, groundedImageRGB);
	drawCross(imageA, pixel_camA, 5, 255, 0, 0);
	prepare2draw(imageA, originalImageRGB);

	//drawFronteraImage();
	//drawGroundImage();
}

inline void moveRobot () {
	coveredDistance = sqrt(abs(pow ((actualPosition.x-myencoders[0]), 2)) + abs(pow ((actualPosition.y-myencoders[1]), 2)));

	printf ("actual = [%f, %f] myencoders[%f, %f], covered = %f\n", actualPosition.x, actualPosition.y, myencoders[0], myencoders[1], coveredDistance);

	if (coveredDistance >= 250) {
		robotStill = TRUE;
		*myv=0.0; *myw=0.0;
	}	else
		*myv=V_MAX; *myw=0.0;
}

void frontera_iteration() {
	usleep (10);

	speedcounter(frontera_id);
	pthread_mutex_lock(&main_mutex);

	if (!completedMovement) {
		moveInitialTilt ();
	} else {
		if (robotStill) { // cuando esté el robot parado
			actualInstant = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000000;
			movePantilt ();
			//getCameraWorldPos ();
			if (pantiltStill) {
				processImage ();
				checkOverlappedLines ();
				if (numFlashes == 4)
					robotStill = FALSE;
			}
		} else { // el robot no está parado
			if (numFlashes == 4) { // acabamos de mirar 4 veces (izda., centro, dcha. y centro)
				actualPosition.x = myencoders[0]; // almacenamos la pos que tenemos actualmente
				actualPosition.y = myencoders[1];

				numFlashes = 0;
			}
			robotStill = TRUE; // quitar cuando activemos moveRobot
			//moveRobot ();
		}
	}
	pthread_mutex_unlock(&main_mutex);
}

/*Importar símbolos*/
void frontera_imports() {
	/* importar colorA */
	mycolorA = (char**) myimport("colorA","colorA");
	colorArun = (runFn) myimport("colorA", "run");
	colorAstop	= (stopFn) myimport("colorA", "stop");
	mycolorAwidth	= (int*) myimport("colorA", "width");
	mycolorAheight = (int*) myimport("colorA", "height");

	if (mycolorA == NULL || colorArun == NULL || colorAstop == NULL){
	  fprintf(stderr, "I can't import variables from colorA\n");
	  jdeshutdown(1);
	}

	/* importamos motors de pantilt */
  mylongitude=myimport("ptmotors", "longitude");
  mylatitude=myimport ("ptmotors", "latitude");
  mylongitude_speed=myimport("ptmotors", "longitude_speed");
  mylatitude_speed=myimport("ptmotors","latitude_speed");
  
  max_pan=myimport("ptmotors", "max_longitude");
  max_tilt=myimport("ptmotors", "max_latitude");
  min_pan=myimport("ptmotors", "min_longitude");
  min_tilt=myimport("ptmotors", "min_latitude");

	ptmotorsrun=myimport("ptmotors","run");
	ptmotorsstop=myimport("ptmotors","stop");

  /* importamos encoders de pantilt */
  mypan_angle=myimport("ptencoders", "pan_angle");
  mytilt_angle=myimport("ptencoders", "tilt_angle");

  ptencodersrun=myimport("ptencoders", "run");
  ptencodersstop=myimport("ptencoders", "stop");

	/* importamos encoders y motores de la base */
	myencoders=(float *)myimport("encoders","jde_robot");
	encodersrun=(runFn)myimport("encoders","run");
	encodersstop=(stopFn)myimport("encoders","stop");

  myv=(float *)myimport("motors","v");
  myw=(float *)myimport("motors","w");
  motorsrun=(runFn)myimport("motors","run");
  motorsstop=(stopFn)myimport("motors","stop");
}

void frontera_stop()
{
	colorAstop();

	RGB2HSV_destroyTable();

	pthread_mutex_lock(&(all[frontera_id].mymutex));
	put_state(frontera_id,slept);
	printf("frontera: off\n");
	pthread_mutex_unlock(&(all[frontera_id].mymutex));

	free(imageA);
	free(imageAbordered);
	free(imageAfiltered);
	free(imageAgrounded);

	remove_image(originalImageRGB);
	remove_image(borderImageRGB);
	remove_image(filteredImageRGB);
	remove_image(groundedImageRGB);
}


void frontera_run(int father, int *brothers, arbitration fn)
{
	int i;

  pthread_mutex_lock(&(all[frontera_id].mymutex)); // CERROJO -- LOCK
  /* this schema resumes its execution with no children at all */
  for(i=0;i<MAX_SCHEMAS;i++) all[frontera_id].children[i]=FALSE;
 
  all[frontera_id].father=father;
  if (brothers!=NULL)
    {
      for(i=0;i<MAX_SCHEMAS;i++) frontera_brothers[i]=-1;
      i=0;
      while(brothers[i]!=-1) {frontera_brothers[i]=brothers[i];i++;}
    }
  frontera_callforarbitration=fn;
  put_state(frontera_id,notready);
  printf("frontera: on\n");
  frontera_imports();
  /* Wake up drivers schemas */	
  colorArun(frontera_id, NULL, NULL);

  pthread_cond_signal(&(all[frontera_id].condition));
  pthread_mutex_unlock(&(all[frontera_id].mymutex)); // CERROJO -- UNLOCK
}

void *frontera_thread(void *not_used) {
	struct timeval a,b;
	long n=0; /* iteration */
	long next,bb,aa;

	for(;;)	{
		pthread_mutex_lock(&(all[frontera_id].mymutex));

		if (all[frontera_id].state==slept) {
			pthread_cond_wait(&(all[frontera_id].condition),&(all[frontera_id].mymutex));
			pthread_mutex_unlock(&(all[frontera_id].mymutex));
		}
		else {
			if (all[frontera_id].state==notready) /* check preconditions. For now, preconditions are always satisfied*/
				put_state(frontera_id,ready);

			if (all[frontera_id].state==ready) { /* check brothers and arbitrate. For now this is the only winner */
				put_state(frontera_id,winner);
  			*mylongitude_speed= 0.0;
  			*mylatitude_speed= 0.0;
	      all[frontera_id].children[(*(int *)myimport("ptencoders","id"))]=TRUE;
	      all[frontera_id].children[(*(int *)myimport("ptmotors","id"))]=TRUE;
	      all[frontera_id].children[(*(int *)myimport("encoders","id"))]=TRUE;
	      all[frontera_id].children[(*(int *)myimport("motors","id"))]=TRUE;
	      ptencodersrun(frontera_id,NULL,NULL);
	      ptmotorsrun(frontera_id,NULL,NULL);
	      encodersrun(frontera_id,NULL,NULL);
	      motorsrun(frontera_id,NULL,NULL);

				gettimeofday(&a,NULL);
				aa=a.tv_sec*1000000+a.tv_usec;
				n=0;
			}

			if (all[frontera_id].state==winner) {
				/* I'm the winner and must execute my iteration */
				pthread_mutex_unlock(&(all[frontera_id].mymutex));
				/* gettimeofday(&a,NULL); */
				n++;
				frontera_iteration();
				gettimeofday(&b,NULL);
				bb=b.tv_sec*1000000+b.tv_usec;
				next=aa+(n+1)*(long)frontera_cycle*1000-bb;

				if (next>5000) {
					usleep(next-5000); /* discounts 5ms taken by calling usleep itself, on average */
				}	else {
					usleep(25000); /* If iteration takes a long time, sleep 25 ms to avoid overload */
				}
			}	else { /* just let this iteration go away. overhead time negligible */
				pthread_mutex_unlock(&(all[frontera_id].mymutex));
				usleep(frontera_cycle*1000);
			}
		}
	}
}

void frontera_terminate()
{
  pthread_mutex_lock(&(all[frontera_id].mymutex));
  frontera_stop();  
  pthread_mutex_unlock(&(all[frontera_id].mymutex));
  sleep(2);
  //fl_free_form(fd_fronteragui->fronteragui);
//  free(imagenA_buf);
//  free(imagenFiltrada_buf);
//  free(imagenSuelo_buf);
}

inline void createConfigFile () {
	FILE *salida;
	salida = fopen (configFile,"w");

	if (salida == NULL) {
		fprintf(stderr,"Parser: I can't found file %s\n", configFile); exit(-1);
	}

	printf("Building config file %s...\n", configFile);
	fprintf(salida,"dimension = %d\n", 4000);
	fprintf(salida,"resolucion = 50\n");
	fprintf(salida,"tipo = ec_diferencial\n");
	fprintf(salida,"ec_diferencial_speed = 1\n");
	fprintf(salida,"paso_tiempo = nulo\n");
	fprintf(salida,"cell_angles = 0\n");
	fprintf(salida,"mayoria_saturacion = 30\n");
	fprintf(salida,"mayoria_ruido = 10\n");
	fprintf(salida,"long_historia = 0\n");
	fprintf(salida,"\n");
	fprintf(salida,"sonar_filtra = independientes\n");
	fprintf(salida,"sonar_geometria = cono_denso\n");
	fprintf(salida,"sonar_apertura = 20.\n");
	fprintf(salida,"sonar_noobstacle = 3000.\n");
	fprintf(salida,"sonar_radialerror = 10.\n");
	fprintf(salida,"sonar_fdistancia = lineal\n");
	fprintf(salida,"sonar_residuo = 0.05\n");
	fprintf(salida,"sonar_o = 0.4\n");
	fprintf(salida,"sonar_e = -0.5\n");
	fprintf(salida,"sonar_mind = 700\n");
	fprintf(salida,"sonar_maxd = 1100.\n");
	fprintf(salida,"\n");
	fprintf(salida,"robot_geometria = cilindro\n");
	fprintf(salida,"robot_radio = 248.\n");
	fprintf(salida,"robot_e = -0.8\n");
	fprintf(salida,"\n");
	fprintf(salida,"laser_geometria = cono_denso\n");
	fprintf(salida,"laser_apertura = 0.5\n");
	fprintf(salida,"laser_muestras = 90\n");
	fprintf(salida,"laser_noobstacle = 8000.\n");
	fprintf(salida,"laser_o = 1\n");
	fprintf(salida,"laser_e = -0.7\n");

	fclose(salida);
}

/*Exportar símbolos*/
void frontera_exports()
{
   myexport("frontera","cycle",&frontera_cycle);
   myexport("frontera","resume",(void *)frontera_run);
   myexport("frontera","suspend",(void *)frontera_stop);
}

void frontera_init(char *configfile)
{
  int i=0;
  FILE *myconfig;
  pthread_mutex_lock(&(all[frontera_id].mymutex)); // CERROJO -- LOCK

  printf("frontera schema started up\n");
  frontera_exports();
  put_state(frontera_id,slept);
  pthread_create(&(all[frontera_id].mythread),NULL,frontera_thread,NULL);
  if (myregister_displaycallback==NULL){
		if ((myregister_displaycallback=(registerdisplay)myimport ("graphics_gtk", "register_displaycallback"))==NULL)
		{
		  printf ("I can't fetch register_displaycallback from graphics_gtk\n");
		  jdeshutdown(1);
		}
		if ((mydelete_displaycallback=(deletedisplay)myimport ("graphics_gtk", "delete_displaycallback"))==NULL)
		{
		  printf ("I can't fetch delete_displaycallback from graphics_gtk\n");
		  jdeshutdown(1);
		}
  }

  pthread_mutex_unlock(&(all[frontera_id].mymutex)); // CERROJO -- UNLOCK

   /* initializes the 3D world */
  myconfig=fopen(configfile,"r");
  if (myconfig==NULL) {
    printf("extrinsics: configuration file %s does not exits\n",configfile);
  } else {
    printf("extrinsics: configuration file %s\n",configfile);
    do
			i=loadWorldLines(myconfig);
		while(i!=EOF);
	}
	fclose(myconfig);

	// Valores iniciales para la cámara virtual con la que observo la escena de puntos 3D en el suelo
  virtualcam0.position.X=4000.;
  virtualcam0.position.Y=4000.;
  virtualcam0.position.Z=6000.;
  virtualcam0.position.H=1.;
  virtualcam0.foa.X=0.;
  virtualcam0.foa.Y=0.;
  virtualcam0.foa.Z=0.;
  virtualcam0.position.H=1.;
  virtualcam0.roll=0.;

	// Valores iniciales para la cámara virtual con la que observo la escena
  virtualcam1.position.X=4000.;
  virtualcam1.position.Y=4000.;
  virtualcam1.position.Z=6000.;
  virtualcam1.position.H=1.;
  virtualcam1.foa.X=0.;
  virtualcam1.foa.Y=0.;
  virtualcam1.foa.Z=0.;
  virtualcam1.position.H=1.;
  virtualcam1.roll=0.;

	// Valores para la cámara virtual con la que observo el entorno de frontera al robot
  virtualcam2.position.X=4000.;
  virtualcam2.position.Y=4000.;
  virtualcam2.position.Z=6000.;
  virtualcam2.position.H=1.;
  virtualcam2.foa.X=0.;
  virtualcam2.foa.Y=0.;
  virtualcam2.foa.Z=0.;
  virtualcam2.foa.H=1.;
  virtualcam2.roll=0.;

	// Valores para la cámara virtual de la esquina derecha del laboratorio (myColorA)
  roboticLabCam0.position.X=7875.000000;
  roboticLabCam0.position.Y=-514.000000;
  roboticLabCam0.position.Z=3000.000000;
  roboticLabCam0.position.H=1.;
  roboticLabCam0.foa.X=6264.000000;
  roboticLabCam0.foa.Y=785.0000000;
  roboticLabCam0.foa.Z=1950.00000;
  roboticLabCam0.foa.H=1.;
  roboticLabCam0.fdistx=405.399994;
	roboticLabCam0.fdisty=roboticLabCam0.fdistx;
	roboticLabCam0.skew=0.;
  roboticLabCam0.u0=142.600006;
  roboticLabCam0.v0=150.399994;
  roboticLabCam0.roll=3.107262;

	// Valores para la cámara virtual de la esquina derecha de enfrente del laboratorio (myColorB)
  roboticLabCam1.position.X=7875.000000;
  roboticLabCam1.position.Y=4749.000000;
  roboticLabCam1.position.Z=3000.000000;
  roboticLabCam1.position.H=1.;
  roboticLabCam1.foa.X=6264.000000;
  roboticLabCam1.foa.Y=3700.000000;
  roboticLabCam1.foa.Z=1950.00000;
  roboticLabCam1.foa.H=1.;
	roboticLabCam1.fdistx=405.399994;
	roboticLabCam1.fdisty=roboticLabCam1.fdistx;
	roboticLabCam1.skew=0.;
  roboticLabCam1.u0=142.600006;
  roboticLabCam1.v0=142.600006;
  roboticLabCam1.roll=3.007028;

	// Valores para la cámara virtual de la esquina izquierda de enfrente del laboratorio (myColorC)
  roboticLabCam2.position.X=50.000000;
  roboticLabCam2.position.Y=4471.000000;
  roboticLabCam2.position.Z=2955.000000;
  roboticLabCam2.position.H=1.;
  roboticLabCam2.foa.X=2432.399902;
  roboticLabCam2.foa.Y=2918.000000;
  roboticLabCam2.foa.Z=1300.000000;
  roboticLabCam2.foa.H=1.;
	roboticLabCam2.fdistx=405.399994;
	roboticLabCam2.fdisty=roboticLabCam2.fdistx;
	roboticLabCam2.skew=0.;
  roboticLabCam2.u0=142.600006;
  roboticLabCam2.v0=150.399994;
  roboticLabCam2.roll=3.107262;

	// Valores para la cámara virtual de la esquina izquierda del laboratorio (myColorD)
  roboticLabCam3.position.X=50.000000;
  roboticLabCam3.position.Y=100.000000;
  roboticLabCam3.position.Z=2955.000000;
  roboticLabCam3.position.H=1.;
  roboticLabCam3.foa.X=2160.000000;
  roboticLabCam3.foa.Y=1151.000000;
  roboticLabCam3.foa.Z=1550.000000;
  roboticLabCam3.foa.H=1.;
	roboticLabCam3.fdistx=405.399994;
	roboticLabCam3.fdisty=roboticLabCam3.fdistx;
	roboticLabCam3.skew=0.;
  roboticLabCam3.u0=142.600006;
  roboticLabCam3.v0=150.399994;
  roboticLabCam3.roll=2.956911;

	// Valores para la cámara REAL del ROBOT. Valores iniciales, que no se usan ya que luego los modificamos convenientemente :)
  robotCamera.position.X=116.407570;
  robotCamera.position.Y=400.009766;
  robotCamera.position.Z=449.958099;
  robotCamera.position.H=1.;
  robotCamera.foa.X=930.;
  robotCamera.foa.Y=330.;
  robotCamera.foa.Z=0.;
  robotCamera.foa.H=1.;
	robotCamera.fdistx=425.871368;
	robotCamera.fdisty=robotCamera.fdistx;
	robotCamera.skew=0.;
	robotCamera.u0=98.245613;
  robotCamera.v0=164.678360;
  robotCamera.roll=-0.019950;

  ceilLabCam.position.X=-400.;//260.000000;
  ceilLabCam.position.Y=400.000000;
  ceilLabCam.position.Z=800.;//400.000000;
  ceilLabCam.position.H=1.;
  ceilLabCam.foa.X=700.000000;
  ceilLabCam.foa.Y=400.000000;
  ceilLabCam.foa.Z=0.000000;
  ceilLabCam.foa.H=1.;
	ceilLabCam.fdistx=408.350891;
	ceilLabCam.fdisty=ceilLabCam.fdistx;
	ceilLabCam.skew=0.;
  ceilLabCam.u0=156.;
  ceilLabCam.v0=200.233917;
  ceilLabCam.roll=(PI/2)-0.059950;

	tiltAngle = -38.68;//-(float) atan2 (480,600)*360./(2.*PI); // grados
	panAngle = 0.;//-1.5; // grados
  speed_y = VEL_MAX_TILT-((POS_MAX_TILT-DEG_TO_ENCOD*(abs(tiltAngle)))/((POS_MAX_TILT-POS_MIN_TILT)/(VEL_MAX_TILT-VEL_MIN_TILT)));
  speed_x = VEL_MAX_TILT-((POS_MAX_TILT-DEG_TO_ENCOD*(abs(panAngle)))/((POS_MAX_TILT-POS_MIN_TILT)/(VEL_MAX_TILT-VEL_MIN_TILT)));

	sliderPANTILT_BASE_HEIGHT = PANTILT_BASE_HEIGHT;
	sliderISIGHT_OPTICAL_CENTER = ISIGHT_OPTICAL_CENTER;
	sliderTILT_HEIGHT = TILT_HEIGHT;
	sliderCAMERA_TILT_HEIGHT = CAMERA_TILT_HEIGHT;
	sliderPANTILT_BASE_X = PANTILT_BASE_X;

	update_camera_matrix (&roboticLabCam0);
	update_camera_matrix (&roboticLabCam1);
	update_camera_matrix (&roboticLabCam2);
	update_camera_matrix (&roboticLabCam3);
	update_camera_matrix (&robotCamera);
	update_camera_matrix (&ceilLabCam);

  actualCameraView = 5; // empezamos con la cámara del robot

	completedMovement = FALSE;

	actualInstant = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000000;
	stopInstant = actualInstant;
	comeFromCenter = TRUE;
	last_full_movement = right;

  xcam = robotCamera.position.X;
  ycam = robotCamera.position.X;
  zcam = robotCamera.position.X;

	pixel_camA.x = robotCamera.u0; // parameters extracted from Redo's calibrator gui
	pixel_camA.y = robotCamera.v0;

	pantiltStill = FALSE;
	robotStill = TRUE;
	numFlashes = 0;

	configFile = "./gradientPlanning.conf";
	createConfigFile ();

	myHSV = (struct HSV*)malloc(sizeof(struct HSV));

	image = (char*) malloc(SIFNTSC_COLUMNS * SIFNTSC_ROWS * 3);
	contourImage = (char*) malloc(SIFNTSC_COLUMNS * SIFNTSC_ROWS * 3);

  //imageA = create_image(*mycolorAwidth, *mycolorAheight, 3);
  imageA = create_image(SIFNTSC_COLUMNS, SIFNTSC_ROWS, 3);
  //free(imageA->image); // vaciamos el espacio reservado para la imagen

  imageAfiltered = create_image(SIFNTSC_COLUMNS, SIFNTSC_ROWS, 3);
  imageAgrounded = create_image(SIFNTSC_COLUMNS, SIFNTSC_ROWS, 3);
  imageAbordered = create_image(SIFNTSC_COLUMNS, SIFNTSC_ROWS, 3);

  originalImageRGB = create_image(SIFNTSC_COLUMNS, SIFNTSC_ROWS, 3);
  filteredImageRGB = create_image(SIFNTSC_COLUMNS, SIFNTSC_ROWS, 3);
  groundedImageRGB = create_image(SIFNTSC_COLUMNS, SIFNTSC_ROWS, 3);
  borderImageRGB = create_image(SIFNTSC_COLUMNS, SIFNTSC_ROWS, 3);

	RGB2HSV_init();
	RGB2HSV_createTable();

	for (i = 0; i < MAX_LINES_IN_MEMORY; i ++) {
		groundLines3D[i].X = -999999.99;
		groundLines3D[i].Y = -999999.99;
		groundLines3D[i].Z = -999999.99;
	}

}

inline static int initFloorOGL (int w, int h) {
	glClearColor(0.f, 0.8f, 0.5f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glDisable(GL_DEPTH_TEST);
	glMatrixMode (GL_PROJECTION);
	glLoadIdentity ();
	glOrtho (0, w, h, 0, 0, 1);

	return 0;
}

inline static int initFronteraOGL(int w, int h)
 /* Inicializa OpenGL con los parametros que diran como se visualiza. */
{
	GLfloat ambient[] = {1.0, 1.0, 1.0, 1.0};
	GLfloat diffuse[] = {1.0, 1.0, 1.0, 1.0};
	GLfloat position[] = {0.0, 3.0, 3.0, 0.0};
	GLfloat lmodel_ambient[] = {0.2, 0.2, 0.2, 1.0};
	GLfloat local_view[] = {0.0};

	glViewport(0,0,(GLint)w,(GLint)h);  
	glDrawBuffer(GL_BACK);
	glClearColor(0.6f, 0.8f, 1.0f, 0.0f);
	glClearDepth(1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	/* With this, the pioneer appears correctly, but the cubes don't */
	glLightfv (GL_LIGHT0, GL_AMBIENT, ambient);
	glLightfv (GL_LIGHT0, GL_DIFFUSE, diffuse);
	glLightfv (GL_LIGHT0, GL_POSITION, position);
	glLightModelfv (GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);
	glLightModelfv (GL_LIGHT_MODEL_LOCAL_VIEWER, local_view);
	glEnable (GL_LIGHT0);
	/*glEnable (GL_LIGHTING);*/

	glEnable(GL_TEXTURE_2D);     /* Enable Texture Mapping */
	glEnable (GL_AUTO_NORMAL);
	glEnable (GL_NORMALIZE);  
	glEnable(GL_DEPTH_TEST);     /* Enables Depth Testing */
	glDepthFunc(GL_LESS);  
	glShadeModel(GL_SMOOTH);     /* Enables Smooth Color Shading */
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	return 0;
}

inline void initWorldCanvasSettings () {
  float r,lati,longi;

	if (!flashImage) {
	  if (foa_mode) {
	    longi=2*PI*mouse_on_fronteracanvas.x/360.;
	    lati=2*PI*mouse_on_fronteracanvas.y/360.;
	    r=500;
	    virtualcam2.position.X=radius*(virtualcam2.position.X)/radius_old;
	    virtualcam2.position.Y=radius*(virtualcam2.position.Y)/radius_old;
	    virtualcam2.position.Z=radius*(virtualcam2.position.Z)/radius_old;
	    if (centrado==0){
				//Si centrado = 0 se ha pulsado el boton de centrar. Si vale 1, no está pulsado.
				virtualcam2.foa.X=r*cos(lati)*cos(longi);
				virtualcam2.foa.Y=r*cos(lati)*sin(longi);
				virtualcam2.foa.Z=r*sin(lati);
			}
	  }
	  
	  if (cam_mode) {
			centrado = 0;

	    longi=2*PI*mouse_on_fronteracanvas.x/360.;
	    lati=2*PI*mouse_on_fronteracanvas.y/360.;
	    
	    virtualcam2.position.X=radius*cos(lati)*cos(longi);
	    virtualcam2.position.Y=radius*cos(lati)*sin(longi);
	    virtualcam2.position.Z=radius*sin(lati);
	    
//	    virtualcam2.foa.X=0;
//	    virtualcam2.foa.Y=0;
//	    virtualcam2.foa.Z=0;
	  }
		radius_old=radius;
	}

	initFronteraOGL(640,480);
	  
	/* Virtual camera */
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity(); 

	/* perspective projection. intrinsic parameters + frustrum */
	gluPerspective(45.,(GLfloat)640/(GLfloat)480,1.0,50000.0);
	/* extrinsic parameters */
	if (actualCameraView == 0) // user camera view
		//gluLookAt(virtualcam2.position.X,ycam,zcam,foax,foay,foaz,0.,0.,1.);
	  gluLookAt(virtualcam2.position.X,virtualcam2.position.Y,virtualcam2.position.Z,
	          virtualcam2.foa.X,virtualcam2.foa.Y,virtualcam2.foa.Z,
	          0.,0.,1.);
	else if (actualCameraView == 1) // robotics lab camera 1
		gluLookAt(roboticLabCam0.position.X,roboticLabCam0.position.Y,roboticLabCam0.position.Z,roboticLabCam0.foa.X,roboticLabCam0.foa.Y,roboticLabCam0.foa.Z,0.,0.,1.);
	else if (actualCameraView == 2) // robotics lab camera 2
		gluLookAt(roboticLabCam1.position.X,roboticLabCam1.position.Y,roboticLabCam1.position.Z,roboticLabCam1.foa.X,roboticLabCam1.foa.Y,roboticLabCam1.foa.Z,0.,0.,1.);
	else if (actualCameraView == 3) // robotics lab camera 3
		gluLookAt(roboticLabCam2.position.X,roboticLabCam2.position.Y,roboticLabCam2.position.Z,roboticLabCam2.foa.X,roboticLabCam2.foa.Y,roboticLabCam2.foa.Z,0.,0.,1.);
	else if (actualCameraView == 4) // robotics lab camera 4
		gluLookAt(roboticLabCam3.position.X,roboticLabCam3.position.Y,roboticLabCam3.position.Z,roboticLabCam3.foa.X,roboticLabCam3.foa.Y,roboticLabCam3.foa.Z,0.,0.,1.);
	else if (actualCameraView == 5) // robotics lab ceil camera
		gluLookAt(ceilLabCam.position.X,ceilLabCam.position.Y,ceilLabCam.position.Z,ceilLabCam.foa.X,ceilLabCam.foa.Y,ceilLabCam.foa.Z,0.,0.,1.);
}

inline int loadWorldLines (FILE *myfile)
{
  #define limit 256		
  char word1[limit],word2[limit],word3[limit],word4[limit],word5[limit];
  char word6[limit],word7[limit],word8[limit];
  char word[limit];
  int i=0;
  char buffer_file[limit];   

  buffer_file[0]=fgetc(myfile);
  if (feof(myfile)) return EOF;
  if (buffer_file[0]==(char)255) return EOF; 
  if (buffer_file[0]=='#') {while(fgetc(myfile)!='\n'); return 0;}
  if (buffer_file[0]==' ') {while(buffer_file[0]==' ') buffer_file[0]=fgetc(myfile);}
  if (buffer_file[0]=='\t') {while(buffer_file[0]=='\t') buffer_file[0]=fgetc(myfile);}

  /* Captures a line and then we will process it with sscanf checking that the last character is \n. We can't doit with fscanf because this function does not difference \n from blank space. */
  while((buffer_file[i]!='\n') && 
	(buffer_file[i] != (char)255) &&  
	(i<limit-1) ) {
    buffer_file[++i]=fgetc(myfile);
  }
  
  if (i >= limit-1) { 
    printf("%s...\n", buffer_file); 
    printf ("Line too long in config file!\n"); 
    exit(-1);
  }
  buffer_file[++i]='\0';


  if (sscanf(buffer_file,"%s",word)!=1) return 0; 
  /* return EOF; empty line*/
  else {
     if(strcmp(word,"worldline")==0){
	sscanf(buffer_file,"%s %s %s %s %s %s %s %s %s",word,word1,word2,word3,word4,word5,word6,word7,word8);
	myfloor[myfloor_lines*2+0].X=(float)atof(word1); myfloor[myfloor_lines*2+0].Y=(float)atof(word2); myfloor[myfloor_lines*2+0].Z=(float)atof(word3); myfloor[myfloor_lines*2+0].H=(float)atof(word4);
	myfloor[myfloor_lines*2+1].X=(float)atof(word5); myfloor[myfloor_lines*2+1].Y=(float)atof(word6); myfloor[myfloor_lines*2+1].Z=(float)atof(word7); myfloor[myfloor_lines*2+1].H=(float)atof(word8);
	myfloor_lines++;
     }/*
     else if (strcmp(word,"cameraINfile")==0){
       sscanf(buffer_file,"%s %s",word,cameraINfile);
     }
     else if (strcmp(word,"cameraOUTfile")==0){
       sscanf(buffer_file,"%s %s",word,cameraOUTfile);
     }
     else if (strcmp(word,"maxX")==0){
     }
     else if (strcmp(word,"minX")==0){
     }
     else if (strcmp(word,"maxY")==0){
     }
     else if (strcmp(word,"minY")==0){
     }
     else if (strcmp(word,"maxZ")==0){
     }
     else if (strcmp(word,"minZ")==0){
     }*/
  }
  return 1;
}

inline float lineMagnitude (HPoint3DColor *Point1, HPoint3DColor *Point2) {
	HPoint3DColor vector;

	vector.X = Point2->X - Point1->X;
	vector.Y = Point2->Y - Point1->Y;
	vector.Z = Point2->Z - Point1->Z;
	vector.H = 1.;

	return (float) (sqrt(vector.X * vector.X + vector.Y * vector.Y + vector.Z * vector.Z));
}

inline int distancePointLine (HPoint3DColor *Point, HPoint3DColor *LineStart, HPoint3DColor *LineEnd, float *Distance) {
	float LineMag;
	float U;
	HPoint3DColor Intersection;

	LineMag = lineMagnitude (LineEnd, LineStart);

	U = ( ( ( Point->X - LineStart->X ) * ( LineEnd->X - LineStart->X ) ) +
		( ( Point->Y - LineStart->Y ) * ( LineEnd->Y - LineStart->Y ) ) +
		( ( Point->Z - LineStart->Z ) * ( LineEnd->Z - LineStart->Z ) ) ) /
		( LineMag * LineMag );

	if( U < 0.0f || U > 1.0f )
		return 0;   // closest point does not fall within the line segment

	Intersection.X = LineStart->X + U * ( LineEnd->X - LineStart->X );
	Intersection.Y = LineStart->Y + U * ( LineEnd->Y - LineStart->Y );
	Intersection.Z = LineStart->Z + U * ( LineEnd->Z - LineStart->Z );

	*Distance = lineMagnitude(Point, &Intersection);

	return 1;
}

inline int areNearlyParallelLines (HPoint3DColor *Line1Start, HPoint3DColor *Line1End, HPoint3DColor *Line2Start, HPoint3DColor *Line2End) {
	// Let me take m = (y2-y1)/(x2-x1)
	int areParallel = 0;
	float m1, m2;
	if ((Line1End->X - Line1Start->X) != 0.)
		m1 = ((Line1End->Y - Line1Start->Y)/(Line1End->X - Line1Start->X));
	else
		m1 = 999999.9;

	if ((Line2End->X - Line2Start->X) != 0.)
		m2 = ((Line2End->Y - Line2Start->Y)/(Line2End->X - Line2Start->X));
	else
		m2 = 999999.9;

	//printf ("[%f, %f] [%f, %f] m1 = %f, m2 = %f\n", Line1Start->X, Line1Start->Y, Line2Start->X, Line2Start->Y, m1, m2);

	//if (m1 == m2) {
	if ((!(m1 > 990000.9)) && (!(m2 > 990000.9))) {
		if ((((m1 > 0) && (m2 > 0)) || ((m1 < 0) && (m2 < 0))) && (abs (m1-m2) < 1.)) { // We consider parallel lines
		// both positive slopes or negative ones... and a bit threshold
			areParallel = 1;
		}
	}
	return areParallel;
}

inline void getOverlappedSegment (HPoint3DColor *Line1Start, HPoint3DColor *Line1End, HPoint3DColor *Line2Start, HPoint3DColor *Line2End, HPoint3DColor *startPoint, HPoint3DColor *endPoint) {

	HPoint3DColor* myArray[4] = {Line1Start, Line1End, Line2Start, Line2End};
	float max = 0;
	int i, j;

	for (i = 0; i < 4; i ++) {
		for (j = i+1; j < 4; j ++) {
			if (abs(lineMagnitude (myArray[i], myArray[j])) > max) {
				max = abs(lineMagnitude (myArray[i], myArray[j]));
				*startPoint = *myArray[i];
				*endPoint = *myArray[j];
			}
		}
	}
	// Finally, we'll get two points in order to maximize distance between them
}

inline void getLineEquation(int xini, int yini, int xfin, int yfin, float * A, float * B, float * C) {
	/*Line equation, producto vectorial de los puntos*/
	*A = (float)yini - (float)yfin; /*y1*z2 - z1*y2*/
	*B = (float)xfin - (float)xini; /*z1*x2 - x1*z2*/
	*C = (float)xini*(float)yfin - (float)yini*(float)xfin; /*x1*y2 - y1*x2*/
}
/*
inline void checkOverlappedLines () { // to check the overlapping between lines
	float A1, B1, C1, A2, B2, C2;
	float distance1 = 9999999.99, distance2 = 9999999.99;
	int a = 0, i, j;
	HPoint3DColor startPoint, endPoint;

	for (i = 0; i < MAX_LINES_IN_MEMORY; i = i + 2) { // to check every lines in memory
		for (j = i + 2; j < MAX_LINES_IN_MEMORY; j = j + 2) { // to check every lines after the actual
			if (areNearlyParallelLines (&groundLines3D[i], &groundLines3D[i+1], &groundLines3D[j], &groundLines3D[j+1])) {
				//if (debug) printf ("[%f, %f, %f, %f] vs. [%f, %f, %f, %f]\n", groundLines3D[i].X, groundLines3D[i].Y, groundLines3D[i+1].X, groundLines3D[i+1].Y, groundLines3D[j].X, groundLines3D[j].Y, groundLines3D[j+1].X, groundLines3D[j+1].Y);
				if ((!distancePointLine (&groundLines3D[i], &groundLines3D[j], &groundLines3D[j+1], &distance1)) && (!distancePointLine (&groundLines3D[i+1], &groundLines3D[j], &groundLines3D[j+1], &distance2))) { // any point is inside the other projection line => not overlapped lines
					a ++;
				} else { // one point is inside the other projection line => they can be overlapped lines
					if ((distance1 < 50) || (distance2 < 50)) {
						getOverlappedSegment (&groundLines3D[i], &groundLines3D[i+1], &groundLines3D[j], &groundLines3D[j+1], &startPoint, &endPoint);
						//if (debug) printf ("[%f, %f] vs. [%f, %f], distance1 = %f, distance2 = %f\n", startPoint.X, startPoint.Y, endPoint.X, endPoint.Y, distance1, distance2);
						debug = 0;
						groundLines3D[i] = startPoint;
						groundLines3D[i+1] = endPoint;
						groundLines3D[j] = startPoint;
						groundLines3D[j+1] = endPoint;
						//printf ("Got overlapped segment in pos = %i!\n", i);
					}
				}
			} // end check parallel lines
		} // end 2nd for
	} // end 1st for
}*/

inline float calcDistancePV(int px, int py, float A, float B, float C) {

/*			|A*px + B*py + C|
Distance = ------------------
			 sqrt(A^2 + B^2)*/
	float tmp1, tmp2;

	tmp1 = A*px + B*py + C;
	if(tmp1<0)
		tmp1 = tmp1*-1;

	tmp2 = sqrt(pow(A,2) + pow(B,2));
	if(tmp2 == 0)
		return -1;

	return tmp1/tmp2;
}

inline int isPointInsideLine(int px, int py, int ax, int ay, int bx, int by) {
	/*	(Px-Ax)(Bx-Ax) + (Py-Ay)(By-Ay)
	u = -------------------------------
			(Bx-Ax)^2 + (By-Ay)^2*/

	float tmp;
	
	/*Check if the line is a point*/
	if(ax == bx && ay == by)
		return 0;

	tmp = (px-ax)*(bx-ax) + (py-ay)*(by-ay);
	tmp = tmp /(pow(bx-ax,2) + pow(by-ay,2));

	if(tmp >=0 && tmp <=1)
		return 1;

	return 0;
}

inline void checkOverlappedLines () { // to check the overlapping between lines
	float A, B, C;
	float distance1 = 9999999.99, distance2 = 9999999.99;
	int a = 0, i, j;
	HPoint3DColor startPoint, endPoint;

	for (i = 0; i < MAX_LINES_IN_MEMORY; i = i + 2) { // to check every lines in memory
		for (j = i + 2; j < MAX_LINES_IN_MEMORY; j = j + 2) { // to check every lines after the actual
			if (areNearlyParallelLines (&groundLines3D[i], &groundLines3D[i+1], &groundLines3D[j], &groundLines3D[j+1])) {
				//if (debug) printf ("[%f, %f, %f, %f] vs. [%f, %f, %f, %f]\n", groundLines3D[i].X, groundLines3D[i].Y, groundLines3D[i+1].X, groundLines3D[i+1].Y, groundLines3D[j].X, groundLines3D[j].Y, groundLines3D[j+1].X, groundLines3D[j+1].Y);
				if ((isPointInsideLine(groundLines3D[i].X, groundLines3D[i].Y, groundLines3D[j].X, groundLines3D[j].Y, groundLines3D[j+1].X, groundLines3D[j+1].Y)) || (isPointInsideLine(groundLines3D[i+1].X, groundLines3D[i+1].Y, groundLines3D[j].X, groundLines3D[j].Y, groundLines3D[j+1].X, groundLines3D[j+1].Y))) {
					getLineEquation(groundLines3D[j].X, groundLines3D[j].Y, groundLines3D[j+1].X, groundLines3D[j+1].Y, &A, &B, &C);
					distance1 = calcDistancePV(groundLines3D[i].X, groundLines3D[i].Y, A, B, C);
					distance2 = calcDistancePV(groundLines3D[i].X, groundLines3D[i].Y, A, B, C);

					if ((distance1 < 200) || (distance2 < 200)) {
						getOverlappedSegment (&groundLines3D[i], &groundLines3D[i+1], &groundLines3D[j], &groundLines3D[j+1], &startPoint, &endPoint);
						//if (debug) printf ("[%f, %f] vs. [%f, %f], distance1 = %f, distance2 = %f\n", startPoint.X, startPoint.Y, endPoint.X, endPoint.Y, distance1, distance2);
						debug = 0;
						groundLines3D[i] = startPoint;
						groundLines3D[i+1] = endPoint;
						groundLines3D[j] = startPoint;
						groundLines3D[j+1] = endPoint;
						//printf ("Got overlapped segment in pos = %i!\n", i);
					}
				}
			} // end check parallel lines
		} // end 2nd for
	} // end 1st for
}

inline int drawline(IplImage* src, HPoint2D p1, HPoint2D p2) {
	/* it takes care of important features */
	/* before/behind the focal plane, inside/outside the image frame */

	CvPoint pt1, pt2;
	HPoint2D gooda,goodb;

	//printf ("p1 = [%f, %f] y p2 = [%f, %f]\n", p1.x, p1.y, p2.x, p2.y);

	if(displayline(p1,p2,&gooda,&goodb,robotCamera)==1) {
		/*Pasamos de coordenadas opticas a pixels*/
		pt1.x=(int)gooda.y;
		pt1.y=(SIFNTSC_ROWS-1)-(int)gooda.x;
		pt2.x=(int)goodb.y;
		pt2.y=(SIFNTSC_ROWS-1)-(int)goodb.x;
		
		cvLine(src, pt1, pt2, CV_RGB(0,0,255), 1, 8, 0);

		return 1;
	}

	return 0;
}

inline void drawMyLinesOnImage (IplImage *src) {
	int i;
	HPoint2D a,b;

  /* jmvega's lines in camera image */
  for(i=0;i<myfloor_lines;i++){
		//printf ("myfloor[%i]=[%f, %f, %f, %f]\n", i, myfloor[i*2+0].X, myfloor[i*2+0].Y, myfloor[i*2+0].Z, myfloor[i*2+0].H);
    project(myfloor[i*2+0],&a,robotCamera);
    project(myfloor[i*2+1],&b,robotCamera);
		//printf ("myfloor3D[%i]=[%f, %f, %f, %f], myfloor3D[i+1]=[%f, %f, %f, %f], a2D = [%f, %f], b2D = [%f, %f]\n", i, myfloor[i*2+0].X, myfloor[i*2+0].Y, myfloor[i*2+0].Z, myfloor[i*2+0].H, myfloor[i*2+1].X, myfloor[i*2+1].Y, myfloor[i*2+1].Z, myfloor[i*2+1].H, a.x, a.y, b.x, b.y);
    drawline(src,a,b);
  }
}

inline void cannyFilter() {
	IplImage *src;
	IplImage *src2;
	IplImage *gray;
	IplImage *edge;
	IplImage *cedge;
	IplImage *contour;
	CvMemStorage* storage;
	storage = cvCreateMemStorage(0);
	CvSeq* lines;
	CvSeq* contours;
	contours = 0;
	lines = 0;

	double distanceResolution, angleResolution, minLineLength, maxGapBetweenSegments;
	int threshold, numLines, i, j;
	distanceResolution = 1.;
	angleResolution = (PI/180.);
	minLineLength = 30.;
	maxGapBetweenSegments = 10.;
	threshold = 80;

	src = cvCreateImage(cvSize(SIFNTSC_COLUMNS, SIFNTSC_ROWS), IPL_DEPTH_8U, 3);
	src2 = cvCreateImage(cvSize(SIFNTSC_COLUMNS, SIFNTSC_ROWS), IPL_DEPTH_8U, 3);
	cedge = cvCreateImage(cvSize(SIFNTSC_COLUMNS, SIFNTSC_ROWS), IPL_DEPTH_8U, 3);
	gray = cvCreateImage(cvSize(SIFNTSC_COLUMNS, SIFNTSC_ROWS), IPL_DEPTH_8U, 1);
	edge = cvCreateImage(cvSize(SIFNTSC_COLUMNS, SIFNTSC_ROWS), IPL_DEPTH_8U, 1);
	contour = cvCreateImage(cvSize(SIFNTSC_COLUMNS, SIFNTSC_ROWS), IPL_DEPTH_8U, 3);
	
	memcpy(src->imageData, image, src->width*src->height*src->nChannels); // aquí tenemos la original image -> src
	memcpy(src2->imageData, image, src2->width*src2->height*src2->nChannels); // aquí tenemos la original image -> src
	
	drawMyLinesOnImage (src2); // src2 (RGB) -> src2 (RGB) with floor lines
	memcpy(image, src2->imageData, src->width*src2->height*src2->nChannels); // aquí tenemos la original image -> src

	// hago filtrado normal, para luego usar en imageAfiltered (con la frontera)
	cvCvtColor(src, gray, CV_RGB2GRAY); // en gray paso de src (RGB) -> gray (B/N)

	cvCanny(gray, edge, threshold, threshold*3, 3); // saco bordes gray -> edge
	cvCvtColor(edge, cedge, CV_GRAY2RGB); // paso los bordes edge (B/N) -> cedge (RGB)

	memcpy(imageAbordered->image, cedge->imageData, src->width*src->height*src->nChannels); // paso de cedge -> imageAbordered

	lines = cvHoughLines2 (edge, storage, CV_HOUGH_PROBABILISTIC, distanceResolution, angleResolution, threshold, minLineLength, maxGapBetweenSegments);

	if(lines->total > MAX_LINES_TO_DETECT)
		numLines = MAX_LINES_TO_DETECT;
	else
		numLines = lines->total;

	for (i = 0, j = 0; i < numLines; i ++, j = j+2) { // ATENCIÓN: BUCLE IMPORTANTE DE DETECCIÓN DE LÍNEAS Y RETROPROYECCIÓN
		CvPoint* line = (CvPoint*)cvGetSeqElem(lines,i);
		cvLine(contour, line[0], line[1], CV_RGB(255,0,0), 3, 8, 0);
		groundLines2D[j].x = (float)(line[0].x);
		groundLines2D[j].y = (float)(line[0].y);
		groundLines2D[j].h = 1.;
		groundLines2D[j+1].x = (float)(line[1].x);
		groundLines2D[j+1].y = (float)(line[1].y);
		groundLines2D[j+1].h = 1.;
		pointProjection (groundLines2D[j], mycolorA, 0, 0);
		pointProjection (groundLines2D[j+1], mycolorA, 0, 0);
	}
	memcpy(contourImage, contour->imageData, src->width*src->height*src->nChannels);

	/*
	// ahora detecto contornos y meto el contenido en contourImage, la que se muestra en pantalla
	cvThreshold(gray, contour, threshold, 255, CV_THRESH_BINARY);
	cvFindContours(contour, storage, &contours, sizeof(CvContour),CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE,cvPoint(0,0));

	if(contours) {  
		cvCvtColor (contour, cedge, CV_GRAY2RGB);  
		contours = cvApproxPoly(contours, sizeof(CvContour), storage, CV_POLY_APPROX_DP, 3, 1);  
		cvDrawContours(cedge, contours, CV_RGB(255,0,0), CV_RGB(0,255,0), 2, 2, CV_AA, cvPoint(0,0));
	}  

	memcpy(contourImage, cedge->imageData, src->width*src->height*src->nChannels); */

	cvReleaseImage(&src);
	cvReleaseImage(&src2);
	cvReleaseImage(&gray);
	cvReleaseImage(&edge);
	cvReleaseImage(&cedge);
	cvReleaseImage(&contour);
	cvReleaseMemStorage(&storage);
}

inline void drawFronteraImage () {
  int i, j, pos, c, row;//, pos, columnPoint, rowPoint, posPoint;

  float myR = 0., myG = 0., myB = 0.;

	int esFrontera;

	for(i=0; i<SIFNTSC_ROWS*SIFNTSC_COLUMNS; i++) { // limpiamos imageAfiltered de residuos previos
		c=i%SIFNTSC_COLUMNS;
		row=i/SIFNTSC_COLUMNS;
		j=row*SIFNTSC_COLUMNS+c;

		if (image!= NULL) {
		  imageAfiltered->image[i*3]=0.;
		  imageAfiltered->image[i*3+1]=0.;
		  imageAfiltered->image[i*3+2]=0.;
		}
	}

	for (j=0;j<SIFNTSC_COLUMNS;j++) { // recorrido en columnas
		i = SIFNTSC_ROWS-1;
		esFrontera = FALSE;
		while ((i>=0) && (esFrontera == FALSE)) { // recorrido en filas
			if (image!=NULL) {
			  pos = i*SIFNTSC_COLUMNS+j; // posicion actual

				myR = (float)(unsigned int)(unsigned char)imageAbordered->image[pos*3+2];
				myG = (float)(unsigned int)(unsigned char)imageAbordered->image[pos*3+1];
				myB = (float)(unsigned int)(unsigned char)imageAbordered->image[pos*3+0];

				if (!((myR <= 0.) && (myG <= 0.) && (myB <= 0.))) {// si no soy negro -> es que soy frontera
					esFrontera = TRUE;

					imageAfiltered->image[pos*3]=255.;
					imageAfiltered->image[pos*3+1]=255.;
					imageAfiltered->image[pos*3+2]=255.;

					myActualPointImage.x = i;
					myActualPointImage.y = j;
					pointProjection (myActualPointImage, mycolorA, 0, 0);
				} else {
					esFrontera = FALSE;
					imageAfiltered->image[pos*3]=0.;
					imageAfiltered->image[pos*3+1]=0.;
					imageAfiltered->image[pos*3+2]=0.;
				}

				if (primerPunto == FALSE) {
					miPrimerPunto.x = 0;
					miPrimerPunto.y = 0;
					pointProjection (miPrimerPunto, mycolorA, 1, 0);
				} else if (segundoPunto == FALSE) {
					miSegundoPunto.x = 320-1;
					miSegundoPunto.y = 0;
					pointProjection (miSegundoPunto, mycolorA, 1, 0);
				} else if (tercerPunto == FALSE) {
					miTercerPunto.x = 320-1;
					miTercerPunto.y = 240-1;
					pointProjection (miTercerPunto, mycolorA, 1, 0);
				} else if (cuartoPunto == FALSE) {
					miCuartoPunto.x = 0;
					miCuartoPunto.y = 240-1;
					pointProjection (miCuartoPunto, mycolorA, 1, 0);
				}
			} // fin if myColorX != NULL
			i --;
		} // fin while (filas)
 	} // fin for (columnas)
}

inline void drawGroundImage () {
  int i, j, pos, c, row;//, pos, columnPoint, rowPoint, posPoint;

  float myR = 0., myG = 0., myB = 0.;

	int esFrontera;

	for(i=0; i<SIFNTSC_ROWS*SIFNTSC_COLUMNS; i++) { // limpiamos imageAgrounded de residuos previos
		c=i%SIFNTSC_COLUMNS;
		row=i/SIFNTSC_COLUMNS;
		j=row*SIFNTSC_COLUMNS+c;

		if (image!= NULL) {
		  imageAgrounded->image[i*3]=0.;
		  imageAgrounded->image[i*3+1]=0.;
		  imageAgrounded->image[i*3+2]=0.;
		}
	}

	for (j=0;j<SIFNTSC_COLUMNS;j++) { // recorrido en columnas
		i = SIFNTSC_ROWS-1;
		while (i>=0) { // recorrido en filas
			if (image!=NULL) {
			  pos = i*SIFNTSC_COLUMNS+j; // posicion actual

				myR = (float)(unsigned int)(unsigned char)imageAbordered->image[pos*3+2];
				myG = (float)(unsigned int)(unsigned char)imageAbordered->image[pos*3+1];
				myB = (float)(unsigned int)(unsigned char)imageAbordered->image[pos*3+0];

				if (!((myR <= 0.) && (myG <= 0.) && (myB <= 0.))) {// si no soy negro -> es que soy borde
					imageAgrounded->image[pos*3]=255.;
					imageAgrounded->image[pos*3+1]=255.;
					imageAgrounded->image[pos*3+2]=255.;

					myActualPointImage.x = j;
					myActualPointImage.y = i;
					pointProjection (myActualPointImage, mycolorA, 0, 1);
				} else {
					imageAgrounded->image[pos*3]=0.;
					imageAgrounded->image[pos*3+1]=0.;
					imageAgrounded->image[pos*3+2]=0.;
				}
			} // fin if myColorX != NULL
			i --;
		} // fin while (filas)
 	} // fin for (columnas)
}

inline void drawGroundPoints () {
	int i;

	glBegin (GL_POINTS);
		for (i = 0; i < SIFNTSC_COLUMNS*SIFNTSC_ROWS; i ++) {
			if (groundPoints[i].idColor == 1)
				glColor3f(0.0f, 0.0f, 1.0f);
			else if (groundPoints[i].idColor == 2)
				glColor3f(0.0f, 0.5f, 0.0f);
			else if (groundPoints[i].idColor == 3)
				glColor3f(1.0f, 0.0f, 0.0f);
			glVertex2f (groundPoints [i].X, groundPoints [i].Y);
		}
	glEnd ();
}

inline void drawGroundLines () {
	int i;

	glBegin (GL_LINES);
		for (i = 0; i < MAX_LINES_IN_MEMORY; i = i+2) {
			if (groundLines3D[i].idColor == 1)
				glColor3f(0.0f, 0.0f, 1.0f);
			else if (groundLines3D[i].idColor == 2)
				glColor3f(0.0f, 0.5f, 0.0f);
			else if (groundLines3D[i].idColor == 3)
				glColor3f(1.0f, 0.0f, 0.0f);
			glBegin (GL_LINES);
				glVertex3f (groundLines3D [i].X, groundLines3D [i].Y, groundLines3D [i].Z);
				glVertex3f (groundLines3D [i+1].X, groundLines3D [i+1].Y, groundLines3D [i+1].Z);
			glEnd ();
		}
}

inline void drawFronteraPoints (int mode) {
	int i;

	glColor3f(0.0f, 0.0f, 1.0f);
	glBegin (GL_POINTS);
		for (i = 0; i < SIFNTSC_COLUMNS; i ++) {
			if (mode == 0) {
				//printf ("%f, %f, %f\n", fronteraPoints [i].X, fronteraPoints [i].Y, fronteraPoints [i].Z);
				glVertex3f (fronteraPoints [i].X, fronteraPoints [i].Y, fronteraPoints [i].Z);
			}
			else if (mode == 1)
				glVertex2f (fronteraPoints [i].Y, fronteraPoints [i].X);

		}
	glEnd ();
}

inline static gboolean expose_event (GtkWidget *widget, GdkEventExpose *event, gpointer data) {
	GdkGLContext *glcontext;
	GdkGLDrawable *gldrawable;
	static pthread_mutex_t gl_mutex;
	float dxPioneer, dyPioneer, dzPioneer, longiPioneer, latiPioneer, rPioneer;

	pthread_mutex_lock(&gl_mutex);

	glcontext = gtk_widget_get_gl_context (widget);
	gldrawable = gtk_widget_get_gl_drawable (widget);

	if (!gdk_gl_drawable_gl_begin (gldrawable, glcontext)){
	  pthread_mutex_unlock(&gl_mutex);
	  return FALSE;
	}

	if (worldView == TRUE) {
		initWorldCanvasSettings ();

	  /** Robot Frame of Reference **/
	  glMatrixMode(GL_MODELVIEW);
/*	  glLoadIdentity();
	  if (myencoders!=NULL){
	     mypioneer.posx=myencoders[0];
	     mypioneer.posy=myencoders[1];
	     mypioneer.posz=0.;
	     mypioneer.foax=myencoders[0];
	     mypioneer.foay=myencoders[1];
	     mypioneer.foaz=10.;
	     mypioneer.roll=myencoders[2]*RADTODEG;
	  }
	  else{
	     mypioneer.posx=0.;
	     mypioneer.posy=0.;
	     mypioneer.posz=0.;
	     mypioneer.foax=0.;
	     mypioneer.foay=0.;
	     mypioneer.foaz=10.;
	     mypioneer.roll=0.;
	  }
	  glTranslatef(mypioneer.posx,mypioneer.posy,mypioneer.posz);
	  dxPioneer = (mypioneer.foax-mypioneer.posx);
	  dyPioneer = (mypioneer.foay-mypioneer.posy);
	  dzPioneer = (mypioneer.foaz-mypioneer.posz);
	  longiPioneer = (float)atan2(dyPioneer,dxPioneer)*360./(2.*PI);
	  glRotatef (longiPioneer,0.,0.,1.);
	  rPioneer = sqrt(dxPioneer*dxPioneer+dyPioneer*dyPioneer+dzPioneer*dzPioneer);
	  if (rPioneer<0.00001) latiPioneer=0.;
	  else latiPioneer=acos(dzPioneer/rPioneer)*360./(2.*PI);
	  glRotatef(latiPioneer,0.,1.,0.);
	  glRotatef(mypioneer.roll,0.,0.,1.);

		glEnable (GL_LIGHTING); // LUCES Y.... ACCION!!
		glPushMatrix();
		glTranslatef(1.,0.,0.);
		// the body it is not centered. With this translation we center it
		glScalef (100., 100., 100.);
		loadModel();
		glPopMatrix();
		glDisable (GL_LIGHTING); // FUERA LUCES, VOLVEMOS A LA VIDA REAL
*/
		glLoadIdentity ();
		drawMyLines ();
		glLoadIdentity ();
		glLoadIdentity ();
		drawGroundLines ();
	}

	if (gdk_gl_drawable_is_double_buffered (gldrawable)){
	  gdk_gl_drawable_swap_buffers (gldrawable);
	}
	else{
	  glFlush ();
	}

	gdk_gl_drawable_gl_end (gldrawable);

	pthread_mutex_unlock(&gl_mutex);
	return TRUE;
}

void frontera_guidisplay()
{
	pthread_mutex_lock(&main_mutex);
	gdk_threads_enter();
	GtkImage *filImg = GTK_IMAGE(glade_xml_get_widget(xml, "filteredImage"));
	GtkImage *GroImg = GTK_IMAGE(glade_xml_get_widget(xml, "groundedImage"));
	GtkImage *OriImg = GTK_IMAGE(glade_xml_get_widget(xml, "originalImage"));
	GtkImage *BorImg = GTK_IMAGE(glade_xml_get_widget(xml, "borderImage"));

	sliderPANTILT_BASE_HEIGHT = gtk_range_get_value(glade_xml_get_widget(xml, "sliderPANTILT_BASE_HEIGHT"));
	sliderISIGHT_OPTICAL_CENTER = gtk_range_get_value(glade_xml_get_widget(xml, "sliderOPTICAL_CENTER"));
	sliderTILT_HEIGHT = gtk_range_get_value(glade_xml_get_widget(xml, "sliderTILT_HEIGHT"));
	sliderCAMERA_TILT_HEIGHT = gtk_range_get_value(glade_xml_get_widget(xml, "sliderCAMERA_TILT_HEIGHT"));
	sliderPANTILT_BASE_X = gtk_range_get_value(glade_xml_get_widget(xml, "sliderPANTILT_BASE_X"));
	panAngle = gtk_range_get_value(glade_xml_get_widget(xml, "sliderPAN_ANGLE"));
	tiltAngle = gtk_range_get_value(glade_xml_get_widget(xml, "sliderTILT_ANGLE"));

	gtk_widget_queue_draw(GTK_WIDGET(filImg));
	gtk_widget_queue_draw(GTK_WIDGET(GroImg));
	gtk_widget_queue_draw(GTK_WIDGET(OriImg));
	gtk_widget_queue_draw(GTK_WIDGET(BorImg));
	//if (worldView == TRUE) if (fronteraCanvas!=NULL) 
	expose_event(fronteraCanvas, NULL, NULL);
	//else if (floorView == TRUE) if (floorCanvas!=NULL) expose_event(floorCanvas, NULL, NULL);

	gtk_widget_queue_draw(GTK_WIDGET(win));
	gdk_threads_leave();
	pthread_mutex_unlock(&main_mutex);
}

void frontera_hide() {
  if (win!=NULL) {
      gdk_threads_enter();
      gtk_widget_hide(win);
      gdk_threads_leave();
	}
  mydelete_displaycallback(frontera_guidisplay);
	all[frontera_id].guistate=off;
}

void frontera_show()
{
	int loadedgui=0;
	static pthread_mutex_t frontera_gui_mutex;
	GtkButton *floorButton, *worldButton, *go1Button, *go50Button, *turn45RButton, *turn45LButton, *turn90RButton, *turn90LButton, *flashButton;
	GtkToggleButton *camera1Button, *camera2Button, *camera3Button, *camera4Button, *ceilCameraButton, *userCameraButton;
	GtkWidget *widget1;

	pthread_mutex_lock(&frontera_gui_mutex);
	if (!loadedgui){
		loadglade ld_fn;
		loadedgui=1;
		pthread_mutex_unlock(&frontera_gui_mutex);

		/*Load the window from the .glade xml file*/
		gdk_threads_enter();  
		if ((ld_fn=(loadglade)myimport("graphics_gtk","load_glade"))==NULL){
		    fprintf (stderr,"I can't fetch 'load_glade' from 'graphics_gtk'.\n");
		    jdeshutdown(1);
		}

		xml = ld_fn ("frontera.glade");
		if (xml==NULL){
		    fprintf(stderr, "Error loading graphical frontera on xml\n");
		    jdeshutdown(1);
		}

		// Set OpenGL Parameters
		GdkGLConfig *glconfig;
		/* Try double-buffered visual */
		glconfig = gdk_gl_config_new_by_mode ((GdkGLConfigMode)(GDK_GL_MODE_RGB |	GDK_GL_MODE_DEPTH |	GDK_GL_MODE_DOUBLE));
		if (glconfig == NULL)  {
			g_print ("*** Cannot find the double-buffered visual.\n");
			g_print ("*** Trying single-buffered visual.\n");

			/* Try single-buffered visual */
			glconfig = gdk_gl_config_new_by_mode ((GdkGLConfigMode)(GDK_GL_MODE_RGB |	GDK_GL_MODE_DEPTH));
			if (glconfig == NULL) {
				g_print ("*** No appropriate OpenGL-capable visual found.\n");
				jdeshutdown(1);
			}
		}

		//floorCanvas = glade_xml_get_widget(xml, "floorCanvas");

    fronteraCanvas = glade_xml_get_widget(xml, "fronteraCanvas");
    //gtk_widget_set_size_request (fronteraCanvas, 1264, 568);

    gtk_widget_unrealize(fronteraCanvas);
    /* Set OpenGL-capability to the widget. */
    if (gtk_widget_set_gl_capability (fronteraCanvas,glconfig,NULL,TRUE,GDK_GL_RGBA_TYPE)==FALSE)
    {
        printf ("No Gl capability\n");
        jdeshutdown(1);
    }
    gtk_widget_realize(fronteraCanvas);

/*
		gtk_widget_unrealize(fronteraCanvas);
		//gtk_widget_unrealize(floorCanvas);
		if (fronteraCanvas == NULL) printf ("Frontera Canvas is NULL\n");
		//if (floorCanvas == NULL) printf ("Floor Canvas is NULL\n");

		// Set OpenGL-capability to the widget
		if (gtk_widget_set_gl_capability (fronteraCanvas,	glconfig,	NULL,	TRUE,	GDK_GL_RGBA_TYPE)==FALSE) {
			printf ("No Gl capability on fronteraCanvas\n");
			jdeshutdown(1);
		}*/
/*		if (gtk_widget_set_gl_capability (floorCanvas,	glconfig,	NULL,	TRUE,	GDK_GL_RGBA_TYPE)==FALSE) {
			printf ("No Gl capability on floorCanvas\n");
			jdeshutdown(1);
		}*/

		gtk_widget_set_child_visible (GTK_WIDGET(fronteraCanvas), TRUE);
		//gtk_widget_set_child_visible (GTK_WIDGET(floorCanvas), TRUE);

		gtk_widget_add_events ( fronteraCanvas,
		                        GDK_BUTTON1_MOTION_MASK    |
		                        GDK_BUTTON2_MOTION_MASK    |
		                        GDK_BUTTON3_MOTION_MASK    |
		                        GDK_BUTTON_PRESS_MASK      |
		                        GDK_BUTTON_RELEASE_MASK    |
		                        GDK_VISIBILITY_NOTIFY_MASK);	

		widget1=(GtkWidget *)glade_xml_get_widget(xml, "fronteraCanvas");

		g_signal_connect (G_OBJECT (widget1), "button_press_event", G_CALLBACK (button_press_event), NULL);
		g_signal_connect (G_OBJECT (widget1), "motion_notify_event", G_CALLBACK (motion_notify_event), NULL);
		g_signal_connect (G_OBJECT (widget1), "scroll-event", G_CALLBACK (scroll_event), fronteraCanvas);

		// CONNECT CALLBACKS
		win = glade_xml_get_widget(xml, "window1");
		//glade_xml_signal_autoconnect (xml); // Conectar los callbacks

		// OpenGl worlds control buttons:
		floorButton = (GtkToggleButton *)glade_xml_get_widget(xml, "floorButton");
		g_signal_connect (G_OBJECT (floorButton), "pressed", G_CALLBACK (floorButton_pressed), NULL);
		g_signal_connect (G_OBJECT (floorButton), "released", G_CALLBACK (floorButton_released), NULL);

		worldButton = (GtkToggleButton *)glade_xml_get_widget(xml, "worldButton");
		g_signal_connect (G_OBJECT (worldButton), "pressed", G_CALLBACK (worldButton_pressed), NULL);
		g_signal_connect (G_OBJECT (worldButton), "released", G_CALLBACK (worldButton_released), NULL);

		// Movement control buttons:
		go1Button = (GtkButton *)glade_xml_get_widget(xml, "go1Button");
		g_signal_connect (G_OBJECT (go1Button), "pressed", G_CALLBACK (go1Button_pressed), NULL);
		g_signal_connect (G_OBJECT (go1Button), "released", G_CALLBACK (go1Button_released), NULL);

		go50Button = (GtkButton *)glade_xml_get_widget(xml, "go50Button");
		g_signal_connect (G_OBJECT (go50Button), "pressed", G_CALLBACK (go50Button_pressed), NULL);
		g_signal_connect (G_OBJECT (go50Button), "released", G_CALLBACK (go50Button_released), NULL);

		turn45RButton = (GtkButton *)glade_xml_get_widget(xml, "turn45RButton");
		g_signal_connect (G_OBJECT (turn45RButton), "pressed", G_CALLBACK (turn45RButton_pressed), NULL);
		g_signal_connect (G_OBJECT (turn45RButton), "released", G_CALLBACK (turn45RButton_released), NULL);

		turn45LButton = (GtkButton *)glade_xml_get_widget(xml, "turn45LButton");
		g_signal_connect (G_OBJECT (turn45LButton), "pressed", G_CALLBACK (turn45LButton_pressed), NULL);
		g_signal_connect (G_OBJECT (turn45LButton), "released", G_CALLBACK (turn45LButton_released), NULL);

		turn90RButton = (GtkButton *)glade_xml_get_widget(xml, "turn90RButton");
		g_signal_connect (G_OBJECT (turn90RButton), "pressed", G_CALLBACK (turn90RButton_pressed), NULL);
		g_signal_connect (G_OBJECT (turn90RButton), "released", G_CALLBACK (turn90RButton_released), NULL);

		turn90LButton = (GtkButton *)glade_xml_get_widget(xml, "turn90LButton");
		g_signal_connect (G_OBJECT (turn90LButton), "pressed", G_CALLBACK (turn90LButton_pressed), NULL);
		g_signal_connect (G_OBJECT (turn90LButton), "released", G_CALLBACK (turn90LButton_released), NULL);

		flashButton = (GtkButton *)glade_xml_get_widget(xml, "flashButton");
		g_signal_connect (G_OBJECT (flashButton), "pressed", G_CALLBACK (flashButton_pressed), NULL);
		g_signal_connect (G_OBJECT (flashButton), "released", G_CALLBACK (flashButton_released), NULL);

		camera1Button = (GtkToggleButton *)glade_xml_get_widget(xml, "camera1Button");
		g_signal_connect (G_OBJECT (camera1Button), "pressed", G_CALLBACK (camera1Button_pressed), NULL);
		g_signal_connect (G_OBJECT (camera1Button), "released", G_CALLBACK (camera1Button_released), NULL);

		camera2Button = (GtkToggleButton *)glade_xml_get_widget(xml, "camera2Button");
		g_signal_connect (G_OBJECT (camera2Button), "pressed", G_CALLBACK (camera2Button_pressed), NULL);
		g_signal_connect (G_OBJECT (camera2Button), "released", G_CALLBACK (camera2Button_released), NULL);

		camera3Button = (GtkToggleButton *)glade_xml_get_widget(xml, "camera3Button");
		g_signal_connect (G_OBJECT (camera3Button), "pressed", G_CALLBACK (camera3Button_pressed), NULL);
		g_signal_connect (G_OBJECT (camera3Button), "released", G_CALLBACK (camera3Button_released), NULL);

		camera4Button = (GtkToggleButton *)glade_xml_get_widget(xml, "camera4Button");
		g_signal_connect (G_OBJECT (camera4Button), "pressed", G_CALLBACK (camera4Button_pressed), NULL);
		g_signal_connect (G_OBJECT (camera4Button), "released", G_CALLBACK (camera4Button_released), NULL);

		ceilCameraButton = (GtkToggleButton *)glade_xml_get_widget(xml, "ceilCameraButton");
		g_signal_connect (G_OBJECT (ceilCameraButton), "pressed", G_CALLBACK (ceilCameraButton_pressed), NULL);
		g_signal_connect (G_OBJECT (ceilCameraButton), "released", G_CALLBACK (ceilCameraButton_released), NULL);

		userCameraButton = (GtkToggleButton *)glade_xml_get_widget(xml, "userCameraButton");
		g_signal_connect (G_OBJECT (userCameraButton), "pressed", G_CALLBACK (userCameraButton_pressed), NULL);
		g_signal_connect (G_OBJECT (userCameraButton), "released", G_CALLBACK (userCameraButton_released), NULL);

		gtk_range_set_value(glade_xml_get_widget(xml, "sliderPAN_ANGLE"),panAngle);
		gtk_range_set_value(glade_xml_get_widget(xml, "sliderTILT_ANGLE"),tiltAngle);
		gtk_range_set_value(glade_xml_get_widget(xml, "sliderPANTILT_BASE_HEIGHT"),sliderPANTILT_BASE_HEIGHT);
		gtk_range_set_value(glade_xml_get_widget(xml, "sliderOPTICAL_CENTER"),sliderISIGHT_OPTICAL_CENTER);
		gtk_range_set_value(glade_xml_get_widget(xml, "sliderTILT_HEIGHT"),sliderTILT_HEIGHT);
		gtk_range_set_value(glade_xml_get_widget(xml, "sliderCAMERA_TILT_HEIGHT"),sliderCAMERA_TILT_HEIGHT);
		gtk_range_set_value(glade_xml_get_widget(xml, "sliderPANTILT_BASE_X"),sliderPANTILT_BASE_X);

		if (win==NULL){
			fprintf(stderr, "Error loading graphic interface\n");
			jdeshutdown(1);
		} else {
			gtk_widget_show(win);
			gtk_widget_queue_draw(GTK_WIDGET(win));
		}
		gdk_threads_leave();
	} else {
		pthread_mutex_unlock(&frontera_gui_mutex);
		gdk_threads_enter();
		gtk_widget_show(win);
		gtk_widget_queue_draw(GTK_WIDGET(win));
		gdk_threads_leave();
	}

	gdk_threads_enter();
	// Initialization of the image buffers
	myregister_displaycallback(frontera_guidisplay);

	GdkPixbuf *originalImageBuf, *filteredImageBuf, *borderImageBuf, *groundedImageBuf;

	originalImage = GTK_IMAGE(glade_xml_get_widget(xml, "originalImage"));
	filteredImage = GTK_IMAGE(glade_xml_get_widget(xml, "filteredImage"));
	groundedImage = GTK_IMAGE(glade_xml_get_widget(xml, "groundedImage"));
	borderImage = GTK_IMAGE(glade_xml_get_widget(xml, "borderImage"));

	originalImageBuf = gdk_pixbuf_new_from_data(originalImageRGB->image,
					GDK_COLORSPACE_RGB,0,8,
					originalImageRGB->width,originalImageRGB->height,
					originalImageRGB->width*3,NULL,NULL);
	filteredImageBuf = gdk_pixbuf_new_from_data(filteredImageRGB->image,
					GDK_COLORSPACE_RGB,0,8,
					filteredImageRGB->width,filteredImageRGB->height,
					filteredImageRGB->width*3,NULL,NULL);
	groundedImageBuf = gdk_pixbuf_new_from_data(groundedImageRGB->image,
					GDK_COLORSPACE_RGB,0,8,
					groundedImageRGB->width,groundedImageRGB->height,
					groundedImageRGB->width*3,NULL,NULL);
	borderImageBuf = gdk_pixbuf_new_from_data(contourImage,
					GDK_COLORSPACE_RGB,0,8,
					SIFNTSC_COLUMNS,SIFNTSC_ROWS,
					SIFNTSC_COLUMNS*3,NULL,NULL);

	gtk_image_set_from_pixbuf(originalImage, originalImageBuf);
	gtk_image_set_from_pixbuf(filteredImage, filteredImageBuf);
	gtk_image_set_from_pixbuf(groundedImage, groundedImageBuf);
	gtk_image_set_from_pixbuf(borderImage, borderImageBuf);

	int a = 0;
	//glutInit(&a, NULL);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	gdk_threads_leave();
}

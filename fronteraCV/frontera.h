#include <GL/gl.h>              
#include <GL/glx.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <GL/freeglut_std.h>
#include <forms.h>
#include <glcanvas.h>

#include <glade/glade.h>
#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <gtk/gtkgl.h>
#include <gtkextra/gtkextra.h>

#include <jde.h>
#include <pioneer.h>
#include <graphics_gtk.h>
#include <progeo.h>
#include <colorspaces.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_multifit.h>

#include <opencv/cv.h>

#define ROOM_MAX_X 7925.
#define v3f glVertex3f
#define PI 3.141592654

#define MAX_RADIUS_VALUE 10000000000
#define MIN_RADIUS_VALUE 0
#define WHEEL_DELTA 10

#define ANCHO_IMAGEN 320
#define LARGO_IMAGEN 240
#define MAXWORLD 20000.
#define V_MAX 100

#define ENCOD_TO_DEG (3.086/60.) /* CTE que nos pasa de unidades PANTILT a grados */
#define DEG_TO_ENCOD (60./3.086) /* CTE que nos pasa de grados a unidades PANTILT */
#define MAXPAN_POS 95
#define MAXPAN_NEG -95
#define VEL_MAX_PAN 2500.0*ENCOD_TO_DEG
#define VEL_MIN_PAN 350.0*ENCOD_TO_DEG
#define POS_MIN_PAN 40.0
#define POS_MAX_PAN 160.0
#define VEL_MAX_TILT 1000.0*ENCOD_TO_DEG
#define VEL_MIN_TILT 300.0*ENCOD_TO_DEG
#define POS_MIN_TILT 40.0
#define POS_MAX_TILT 120.0
#define TIME_TO_LOOK 5
#define MAX_LINES_TO_DETECT 100
#define MAX_LINES_IN_MEMORY 1000

// Parámetros extrínsecos de los distintos elementos del Pioneer + Pantilt + Cámara
#define PANTILT_BASE_HEIGHT 250.
#define ISIGHT_OPTICAL_CENTER -80.
#define TILT_HEIGHT 70.
#define CAMERA_TILT_HEIGHT 120.
#define PANTILT_BASE_X 60.
#define PANTILT_BASE_Y 0.

extern void frontera_init();
extern void frontera_stop();
extern void frontera_run(int father, int *brothers, arbitration fn);
extern int frontera_cycle;

typedef struct SoRtype{
  struct SoRtype *father;
  float posx;
  float posy;
  float posz;
  float foax;
  float foay;
  float foaz;
  float roll;
} SofReference;


struct image_struct {
	int width;
	int height;
	int bpp;	// bytes per pixel
	char *image;
};

typedef struct HPoint3DCol {
  float X;
  float Y;
  float Z;
	float H;
  int idColor;
} HPoint3DColor;

enum movement_pantilt {up,down,left,right};


/* Form definition file generated with fdesign. */

#include "forms.h"
#include <stdlib.h>
#include "fronteragui.h"

FD_fronteragui *create_form_fronteragui(void)
{
  FL_OBJECT *obj;
  FD_fronteragui *fdui = (FD_fronteragui *) fl_calloc(1, sizeof(*fdui));

  fdui->fronteragui = fl_bgn_form(FL_NO_BOX, 1270, 940);
  obj = fl_add_box(FL_FRAME_BOX,0,0,1270,940,"");
    fl_set_object_lcolor(obj,FL_BLUE);
  fdui->hide = obj = fl_add_button(FL_NORMAL_BUTTON,1210,10,50,20,"HIDE");
    fl_set_object_boxtype(obj,FL_FRAME_BOX);
    fl_set_object_color(obj,FL_DARKCYAN,FL_COL1);
    fl_set_object_lcolor(obj,FL_CYAN);
    fl_set_object_lsize(obj,FL_NORMAL_SIZE);
  fdui->ventanaA = obj = fl_add_free(FL_NORMAL_FREE,830,10,160,120,"ventanaA",
			freeobj_ventanaAA_handle);
    fl_set_object_boxtype(obj,FL_FRAME_BOX);
  fdui->ventanaB = obj = fl_add_free(FL_NORMAL_FREE,1000,10,160,120,"ventanaB",
			freeobj_ventanaBB_handle);
    fl_set_object_boxtype(obj,FL_FRAME_BOX);
  fdui->canvas = obj = fl_add_glcanvas(FL_NORMAL_CANVAS,10,10,810,570,"");
    fl_set_object_boxtype(obj,FL_FRAME_BOX);
  fdui->colorA = obj = fl_add_button(FL_PUSH_BUTTON,870,140,70,20,"camera A");
    fl_set_object_boxtype(obj,FL_FRAME_BOX);
    fl_set_object_color(obj,FL_COL1,FL_INACTIVE);
    fl_set_object_lcolor(obj,FL_DARKTOMATO);
  fdui->colorB = obj = fl_add_button(FL_PUSH_BUTTON,1050,140,70,20,"camera B");
    fl_set_object_boxtype(obj,FL_FRAME_BOX);
    fl_set_object_color(obj,FL_COL1,FL_INACTIVE);
    fl_set_object_lcolor(obj,FL_DARKTOMATO);
  fdui->filteredImage = obj = fl_add_free(FL_NORMAL_FREE,10,630,320,240,"",
			freeobj_filteredImage_handle);
    fl_set_object_boxtype(obj,FL_FRAME_BOX);
  fdui->groundImage = obj = fl_add_free(FL_NORMAL_FREE,340,630,320,240,"",
			freeobj_groundImage_handle);
    fl_set_object_boxtype(obj,FL_FRAME_BOX);
  obj = fl_add_text(FL_NORMAL_TEXT,120,600,100,20,"Filtered image");
    fl_set_object_lcolor(obj,FL_GREEN);
    fl_set_object_lalign(obj,FL_ALIGN_LEFT|FL_ALIGN_INSIDE);
  obj = fl_add_text(FL_NORMAL_TEXT,460,600,100,20,"Ground filter");
    fl_set_object_lcolor(obj,FL_SLATEBLUE);
    fl_set_object_lalign(obj,FL_ALIGN_LEFT|FL_ALIGN_INSIDE);
  fdui->minicanvas = obj = fl_add_glcanvas(FL_NORMAL_CANVAS,670,590,590,340,"");
    fl_set_object_boxtype(obj,FL_FRAME_BOX);
  fdui->canvasButton = obj = fl_add_button(FL_PUSH_BUTTON,1170,100,90,30,"Ground View");
    fl_set_object_lcolor(obj,FL_DODGERBLUE);
    fl_set_object_lstyle(obj,FL_NORMAL_STYLE+FL_EMBOSSED_STYLE);
  fdui->fronteraButton = obj = fl_add_button(FL_PUSH_BUTTON,1170,50,90,30,"Border View");
    fl_set_object_lcolor(obj,FL_YELLOW);
    fl_set_object_lstyle(obj,FL_NORMAL_STYLE+FL_EMBOSSED_STYLE);
  fdui->fronteracanvas = obj = fl_add_glcanvas(FL_NORMAL_CANVAS,830,170,430,410,"");
    fl_set_object_boxtype(obj,FL_FRAME_BOX);
  fl_end_form();

  fdui->fronteragui->fdui = fdui;

  return fdui;
}
/*---------------------------------------*/


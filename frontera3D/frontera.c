#include "frontera.h"
#define v3f glVertex3f

#define DISPLAY_ROBOT 0x01UL
#define DISPLAY_SONARS 0x04UL
#define DISPLAY_LASER 0x08U
#define DISPLAY_COLORIMAGEA 0x10UL
#define DISPLAY_COLORIMAGEB 0x20UL

//DEFINIMOS LOS VALORES QUE TENDRA QUE CUMPLIR EL FILTRO HSI
#define H_SUELO_MAX   224.321*DEGTORAD
#define H_SUELO_MIN   282.025*DEGTORAD
#define S_SUELO_MAX   1.0  
#define S_SUELO_MIN   0.0

// Posibles estados de las celdas de la rejilla
#define OCCUPIED -1
#define EMPTY -2

#define PI 3.141592654

#define MOUSELEFT 1
#define PUSHED 1
#define RELEASED 0

#define MAX_RADIUS_VALUE 10000000
#define MIN_RADIUS_VALUE 0
#define WHEEL_DELTA 1000

#define ANCHO_IMAGEN 320
#define LARGO_IMAGEN 240

// Valores imprescindibles para hacer estimaciones de distancias
#define foco 4.0
#define	focover 37.0	// Angulos de apertura
#define	focohor 48.0

// ARCO QUE REALIZA LA CAMARA AL REALIZAR EL BARRIDO
#define GRADOS_ARCO 180

#define resolucion (int) ((SIFNTSC_COLUMNS/focohor)*GRADOS_ARCO)

#define puntos_clave 5
#define IZQ  1
#define DER  -1

#define MAXWORLD 30000.

int DEBUG = 1;
#define MAX_TIME_ON_GRID 99999 * 1000000 /* tiempo m�ximo de permanencia de un punto en la rejilla (dado en microseg.) */

typedef struct map {
	Tvoxel *point;
	int *state;
	unsigned long int *updatingTime;
} Tmap;

Tvoxel centrogrid;
Tgrid *occupancyGrid = NULL; // grid que contiene informaci�n recibida por la c�mara
int isInitGrid = FALSE;

int pendienteDeReubicacionGrafica = FALSE;
Tmap tempMap;

// Variables para guardar las matrices K, R y T de nuestra c�mara:
gsl_matrix *K_1,*R_1;
gsl_vector* x_1;
HPoint2D progtest2D;
HPoint3D progtest3D;

struct HSV* myHSV;
TPinHoleCamera virtualcam0; /* para ver el minimundillo */
TPinHoleCamera virtualcam1; /* para moverme por el mundillo */
TPinHoleCamera virtualcam2; /* para ver el entorno circundante al robot */
TPinHoleCamera robotCamera;
TPinHoleCamera robotCamera2;
TPinHoleCamera *myCamera; // puntero temporal

int flashImage = TRUE;

Display *mydisplay;
int *myscreen;

char *configFile;

HPoint2D myActualPoint2D; // variables para el c�lculo de los rayos virtuales
HPoint3D myActualPoint3D;
HPoint2D myActualPointImage;
HPoint3D cameraPos3D;
HPoint3D myEndPoint3D;
HPoint3D intersectionPoint;

HPoint3D groundPoints[SIFNTSC_COLUMNS*SIFNTSC_ROWS], fronteraPoints[SIFNTSC_COLUMNS*SIFNTSC_ROWS];
int actualGroundPoint = 0, actualFronteraPoint = 0; // contador para el punto actual del suelo...

float distanciaPlanoImagen, incremento;

HPoint2D mouse_on_canvas, mouse_on_minicanvas, mouse_on_fronteracanvas;

/*Gui callbacks*/
registerbuttons myregister_buttonscallback;
registerdisplay myregister_displaycallback;
deletebuttons mydelete_buttonscallback;
deletedisplay mydelete_displaycallback;

float myR, myG, myB;
static int vmode;
static XImage *imagenA, *imagenB, *imagenFiltrada, *imagenSuelo;
static char *imagenA_buf, *imagenB_buf, *imagenFiltrada_buf, *imagenSuelo_buf; /* puntero a memoria para la imagen a visualizar en el servidor X. No compartida con el servidor */
static long int tabla[256]; 
/* tabla con la traduccion de niveles de gris a numero de pixel en Pseudocolor-8bpp. Cada numero de pixel apunta al valor adecuado del ColorMap, con el color adecuado instalado */
static int pixel8bpp_rojo, pixel8bpp_blanco, pixel8bpp_amarillo;

int groundView = FALSE, fronteraView = FALSE;
int foa_mode, cam_mode;
float radius = 500;
float t = 0.5; // lambda

static int message = FALSE;

static char *samplesource=NULL;

int frontera_id=0; 
int frontera_brothers[MAX_SCHEMAS];
arbitration frontera_callforarbitration;
int frontera_cycle=1000; /* ms */

char **myActualCamera;

char **mycolorA;
resumeFn colorAresume;
suspendFn colorAsuspend;

char **mycolorB;
resumeFn colorBresume;
suspendFn colorBsuspend;

FD_fronteragui *fd_fronteragui;
GC fronteragui_gc;
Window fronteragui_win; 
unsigned long display_state;

unsigned long int dameTiempo() {
	struct timeval t; 

	gettimeofday(&t,NULL);
	return t.tv_sec*1000000 + t.tv_usec;	
}

void updateGridTiming () {
	int j;

	for(j=0;j< ((occupancyGrid->size)*(occupancyGrid->size)); j++) {
		if ((dameTiempo() - occupancyGrid->map[j].ultima_actualizacion) > MAX_TIME_ON_GRID) { // es punto viejete
			occupancyGrid->map[j].estado = EMPTY; // lo marcamos a vacio para no me haga de frontera
			occupancyGrid->map[j].ultima_actualizacion = dameTiempo ();
		}
	}
}
/*
int xy2celdaRejilla (Tvoxel mi_punto) {
  int celda=-1, i, j, resolucion = occupancyGrid->resolucion;
  
  j=(mi_punto.x-occupancyGrid->lo.x-resolucion/2)/resolucion;
  i=(mi_punto.y-occupancyGrid->lo.y-resolucion/2)/resolucion;

	if ((i>=0) && (i<occupancyGrid->size) && (j>=0) && (j<occupancyGrid->size)) { // punto perteneciente a rejilla
	  if ((i>=0) && (j>=0)) celda=i*occupancyGrid->size+j;
	}
  
  return celda;
}
*/
void initCalibration () {

  K_1 = gsl_matrix_alloc(3,3);
  R_1 = gsl_matrix_alloc(3,3);
  x_1 = gsl_vector_alloc(3);

/*	// Fill K_1 matrix, with information got on Calibrator GUI
	gsl_matrix_set(K_1,0,0,428.0);
  gsl_matrix_set(K_1,0,1,-5.66);
  gsl_matrix_set(K_1,0,2,173.71);

	gsl_matrix_set(K_1,1,0,0);
  gsl_matrix_set(K_1,1,1,439.39);
  gsl_matrix_set(K_1,1,2,129.55);

	gsl_matrix_set(K_1,2,0,0);
  gsl_matrix_set(K_1,2,1,0);
  gsl_matrix_set(K_1,2,2,0);

	// Fill R_1 matrix, with information got on Calibrator GUI
	gsl_matrix_set(R_1,0,0,0.66);
  gsl_matrix_set(R_1,0,1,-0.75);
  gsl_matrix_set(R_1,0,2,-0.02);

	gsl_matrix_set(R_1,1,0,-0.36);
  gsl_matrix_set(R_1,1,1,-0.34);
  gsl_matrix_set(R_1,1,2,0.87);

	gsl_matrix_set(R_1,2,0,0.66);
  gsl_matrix_set(R_1,2,1,0.57);
  gsl_matrix_set(R_1,2,2,0.50);

	// Fill x_1 vector, with information got on Calibrator GUI
  gsl_vector_set(x_1, 0, -275.9);
  gsl_vector_set(x_1, 1, -276.5);
  gsl_vector_set(x_1, 2, -223.6); */

	// Fill K_1 matrix, with information got on Calibrator GUI
	gsl_matrix_set(K_1,0,0,468.0);
  gsl_matrix_set(K_1,0,1,-1.16);
  gsl_matrix_set(K_1,0,2,179.89);

	gsl_matrix_set(K_1,1,0,0);
  gsl_matrix_set(K_1,1,1,473.43);
  gsl_matrix_set(K_1,1,2,152.59);

	gsl_matrix_set(K_1,2,0,0);
  gsl_matrix_set(K_1,2,1,0);
  gsl_matrix_set(K_1,2,2,1);

	// Fill R_1 matrix, with information got on Calibrator GUI
	gsl_matrix_set(R_1,0,0,0.69);
  gsl_matrix_set(R_1,0,1,-0.72);
  gsl_matrix_set(R_1,0,2,-0.03);

	gsl_matrix_set(R_1,1,0,-0.54);
  gsl_matrix_set(R_1,1,1,-0.55);
  gsl_matrix_set(R_1,1,2,0.64);

	gsl_matrix_set(R_1,2,0,0.47);
  gsl_matrix_set(R_1,2,1,0.43);
  gsl_matrix_set(R_1,2,2,0.77);

	// Fill x_1 vector, with information got on Calibrator GUI
  gsl_vector_set(x_1, 0, -280.4);
  gsl_vector_set(x_1, 1, -265.3);
  gsl_vector_set(x_1, 2, -359.4);
}

double rad2deg(double alfa){
  return (alfa*180)/3.14159264;
}


void drawRobotCamera(double ppx, double ppy, int camera){
  // Estos incrementos se usan para obtener el siguiente punto
  float inc_x = (1./SIFNTSC_COLUMNS)*100;
  float inc_y = (1./SIFNTSC_ROWS)*100;
  
  int i,j,offset,red,green,blue;

	// dibujamos el rectangulo donde se proyectar� la imagen
  glColor3f( 0.0, 0.0, 0.0 );  
  glScalef(0.2,0.2,0.2);
  
  glBegin(GL_LINES);
  v3f( -50, 50, -100 );   
  v3f( 50, 50, -100 );   
  glEnd();
  
  glBegin(GL_LINES);
  v3f( -50, 50, -100 );   
  v3f( -50, -50, -100 );   
  glEnd();

  glBegin(GL_LINES);
  v3f( 50, 50, -100 );   
  v3f( 50, -50, -100 );   
  glEnd();

  glBegin(GL_LINES);
  v3f( 50, -50, -100 );   
  v3f( -50, -50, -100 );   
  glEnd();

  // pintamos la imagen
  if ((mycolorA!=NULL) || (mycolorB!=NULL))
    for (j=0;j<SIFNTSC_ROWS;j++)
      for(i=0;i<SIFNTSC_COLUMNS;i++) {
			  // obtenemos el color del pixel de la imagen de entrada
				offset = j*320+i;
				if (((display_state&DISPLAY_COLORIMAGEA)!=0) && (mycolorA!=NULL)) {
				  red = (*mycolorA)[offset*3+2];
				  green = (*mycolorA)[offset*3+1];
				  blue = (*mycolorA)[offset*3+0];
				}
				
				glColor3f( (float)red/255.0, (float)green/255.0, (float)blue/255.0 );  
				
				// pintamos el punto correspondiente en el escenario pintado con OGL
				glBegin(GL_POINTS);
				v3f(-50.+(inc_x*i),50.-(inc_y*j),-100. );
				glEnd();
			}
  
  // fin del dibujo del rectangulo y su imagen interior. Ahora dibujamos los tri�ngulos.
  glColor3f( 1.0, 0.0, 0.0 );  
  glBegin(GL_LINES);
  v3f( 0.0, 0.0, 0.0 );   
  v3f( -50, 50, -100 );   
  glEnd();

  glColor3f( 0.0, 1.0, 0.0 );  
  glBegin(GL_LINES);
  v3f( 0.0, 0.0, 0.0 );   
  v3f( 50, 50, -100 );   
  glEnd();

  glColor3f( 0.0, 0.0, 1.0 );  
  glBegin(GL_LINES);
  v3f( 0.0, 0.0, 0.0 );   
  v3f( 50, -50, -100 );   
  glEnd();
  
  glColor3f( 1.0, 1.0, 0.0 );  
  glBegin(GL_LINES);
  v3f( 0.0, 0.0, 0.0 );   
  v3f( -50, -50, -100 );   
  glEnd();

  glBegin(GL_LINES);
  glColor3f( 0.0, 1.0, 0.0 );  
  v3f( 0.0, 0.0, 0.0 );   
  v3f( -50+ppx,-50+ppy, -100 );   
  glEnd();

  /* Por alguna razon, el tama�o maximo que se puede utilizar para un punto
     es de un solo pixel que es practicamente invisible para un ser humano.
     Visto esto, se ha decidido usar un cubo de un lado muy pequeño para 
     dibujar "puntos grandes
  ***/
  glLoadIdentity();
  glColor3f( 1.0, 0.0, 0.0 );
  if (((display_state&DISPLAY_COLORIMAGEA)!=0) && (mycolorA!=NULL))
    glTranslatef( 
		 -(float)gsl_vector_get(x_1,0),
		 -(float)gsl_vector_get(x_1,1),
		 -(float)gsl_vector_get(x_1,2));
  
  glutSolidCube(0.01);
	glScalef(1,1,1); // reestablecemos la escala
}

void setRobotCamera () {
  double psi,theta,gama;
  float ppx = gsl_matrix_get(K_1,0,2)/SIFNTSC_COLUMNS;
  float ppy = gsl_matrix_get(K_1,1,2)/SIFNTSC_COLUMNS;
  
  // Aplicamos las transformaciones necesarias, para pasar
  //   de nuestra mariz de rotacion a la angulos Euler
  theta = acos(gsl_matrix_get(R_1,2,2));
  gama = acos(gsl_matrix_get(R_1,2,1)/-sin(theta));
  psi = asin(gsl_matrix_get(R_1,0,2)/sin(theta));
  theta = rad2deg(theta);
  gama  = rad2deg(gama);
  psi = rad2deg(psi);

  glMatrixMode(GL_MODELVIEW);  
  glLoadIdentity();
  glTranslatef(
	       -robotCamera.position.X,
	       -robotCamera.position.Y,
	       -robotCamera.position.Z
	       );
  
  // Rotacion de gama grados en Z
  glRotatef(gama,0.,0.,1.);
  // Rotacion de gama grados en X
  glRotatef(theta,1.,0.,0.);
  // Rotacion de gama grados en Z
  glRotatef(psi,0.,0.,1.);

	drawRobotCamera (ppx, ppy, 1);
}

void drawAxes() {
  int i=0;

  /* ejes X, Y, Z*/
  glLineWidth(2.3f);
  glMatrixMode(GL_MODELVIEW);

  glLoadIdentity();
  glColor3f( 0.7, 0., 0. );
  glBegin(GL_LINES);
  v3f( 0.0, 0.0, 0.0 );   
  v3f( 10000.0, 0.0, 0.0 );
  glEnd();

  glColor3f( 0.0, 0.7, 0. );
  glBegin(GL_LINES);
  v3f( 0.0, 0.0, 0.0 );   
  v3f( 0.0, 10000.0, 0.0 );
  glEnd();

  glColor3f( 0., 0., 0.7 );
  glBegin(GL_LINES);
  v3f( 0.0, 0.0, 0.0 );   
  v3f( 0.0, 0.0, 10000.0 );
  glEnd();
  glLineWidth(1.0f);
}

void setRobotCameraPos () {

  HPoint2D p2;
  HPoint3D p3;
  int i,j=0;
  gsl_matrix *RT,*T,*Res;
  
  RT = gsl_matrix_alloc(4,4);
  T = gsl_matrix_alloc(3,4);
  Res = gsl_matrix_alloc(3,4);
  
  gsl_matrix_set(T,0,0,1);
  gsl_matrix_set(T,1,1,1);
  gsl_matrix_set(T,2,2,1);

  gsl_matrix_set(T,0,3,gsl_vector_get(x_1,0));
  gsl_matrix_set(T,1,3,gsl_vector_get(x_1,1));
  gsl_matrix_set(T,2,3,gsl_vector_get(x_1,2));

  gsl_linalg_matmult (R_1,T,Res);  

  for (i=0;i<3;i++)
    for (j=0;j<4;j++)
      gsl_matrix_set(RT,i,j,gsl_matrix_get(Res,i,j));
  
  
  /** set 0001 in the last row of RT */
  gsl_matrix_set(RT,3,0,0);
  gsl_matrix_set(RT,3,1,0);
  gsl_matrix_set(RT,3,2,0);
  gsl_matrix_set(RT,3,3,1);

  /** Set camera position*/

  robotCamera.position.X = gsl_vector_get(x_1,0);
  robotCamera.position.Y = gsl_vector_get(x_1,1);
  robotCamera.position.Z = gsl_vector_get(x_1,2);

  printf("Camera position %f:%f:%f\n",robotCamera.position.X, robotCamera.position.Y, robotCamera.position.Z);
      
  /** Seting intrensic matrix*/
  robotCamera.k11 = gsl_matrix_get(K_1,0,0);
  robotCamera.k12 = gsl_matrix_get(K_1,0,1);
  robotCamera.k13 = gsl_matrix_get(K_1,0,2);
  robotCamera.k14 = 0;

  robotCamera.k21 = gsl_matrix_get(K_1,1,0);
  robotCamera.k22 = gsl_matrix_get(K_1,1,1);
  robotCamera.k23 = gsl_matrix_get(K_1,1,2);
  robotCamera.k24 = 0;

  robotCamera.k31 = gsl_matrix_get(K_1,2,0);
  robotCamera.k32 = gsl_matrix_get(K_1,2,1);
  robotCamera.k33 = gsl_matrix_get(K_1,2,2);
  robotCamera.k34 = 0;

  /** Seting extrensic*/
  robotCamera.rt11 = gsl_matrix_get(RT,0,0);
  robotCamera.rt12 = gsl_matrix_get(RT,0,1);
  robotCamera.rt13 = gsl_matrix_get(RT,0,2);
  robotCamera.rt14 = gsl_matrix_get(RT,0,3);
  
  robotCamera.rt21 = gsl_matrix_get(RT,1,0);
  robotCamera.rt22 = gsl_matrix_get(RT,1,1);
  robotCamera.rt23 = gsl_matrix_get(RT,1,2);
  robotCamera.rt24 = gsl_matrix_get(RT,1,3);

  robotCamera.rt31 = gsl_matrix_get(RT,2,0);
  robotCamera.rt32 = gsl_matrix_get(RT,2,1);
  robotCamera.rt33 = gsl_matrix_get(RT,2,2);
  robotCamera.rt34 = gsl_matrix_get(RT,2,3);

  robotCamera.rt41 = gsl_matrix_get(RT,3,0);
  robotCamera.rt42 = gsl_matrix_get(RT,3,1);
  robotCamera.rt43 = gsl_matrix_get(RT,3,2);
  robotCamera.rt44 = gsl_matrix_get(RT,3,3);

  p3.X = 100;
  p3.Y = 100;
  p3.Z = 100;
  p3.H = 1;

  progtest2D.x = 0;
  progtest2D.y = 0;
  progtest2D.h = 1;

  project(p3,&p2,robotCamera);
  
  //if (DEBUG)
  printf("El punto project en %f:%f:%f\n",p2.x,p2.y,p2.h);

  if (backproject(&progtest3D, progtest2D, robotCamera)){
    
    progtest3D.X =  progtest3D.X/progtest3D.H;
    progtest3D.Y =  progtest3D.Y/progtest3D.H;
    progtest3D.Z =  progtest3D.Z/progtest3D.H;

  //if (DEBUG)
    printf("el backproject de %.2f-%.2f es %.2f-%.2f-%.2f-%.2f \n\n",
	   progtest2D.x,progtest2D.y,
	   progtest3D.X, progtest3D.Y, progtest3D.Z, progtest3D.H
	   );
  }
}

void drawCube() {
  
  float i;

  glLoadIdentity();
  /*las 3 caras del cubo */
  
  glColor3f( 1.0, 1.0, 1.0 );
  glBegin(GL_POLYGON);
  v3f(98,98,98);
  v3f(0,98,98);
  v3f(0,98,0);
  v3f(98,98,0);
  glEnd();

  glColor3f( 1.0, 1.0, 1.0 );
  glBegin(GL_POLYGON);
  v3f(98,0,98);
  v3f(98,98,98);
  v3f(98,98,0);
  v3f(98,0,0);
  glEnd();
  
  glColor3f( 1.0, 1.0, 1.0 );
  glBegin(GL_POLYGON);
  v3f(98,0,98);
  v3f(0,0,98);
  v3f(0,98,98);
  v3f(98,98,98);
  glEnd();

  glColor3f( .0, .0, .0 );
  glBegin(GL_LINES);
  for (i=100;i>0;i=i-16){

    v3f(100,i,100);
    v3f(0,i,100);
    v3f(i,0,100);
    v3f(i,100,100);

    v3f(100,100,i);
    v3f(100,0,i);
    v3f(100,i,0);
    v3f(100,i,100);

    v3f(100,100,i);
    v3f(0,100,i);
    v3f(i,100,0);
    v3f(i,100,100);

  }
  glEnd();


  glColor3f( 0.0, 0.0, 1.0 );
  glBegin(GL_POLYGON);
  v3f(52,52,100);
  v3f(52,35,100);
  v3f(35,35,100);  
  v3f(35,52,100);
  glEnd();

  glColor3f( 1.0, 0.0, 0.0 );
  glBegin(GL_POLYGON);
  v3f(100,35,35);
  v3f(100,35,52);
  v3f(100,52,52);
  v3f(100,52,35);
  glEnd();

  glColor3f( 0.0, 1.0, 0.0 );
  glBegin(GL_POLYGON);
  v3f(52,100,35);
  v3f(52,100,52);
  v3f(35,100,52);
  v3f(35,100,35);
  glEnd();

  glFlush();

}

void drawRoboticLabPlanes () {
	/* LABORATORIO DE ROB�TICA */
  glLineWidth(2.0f);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

	/* SUELO */
  glColor3f(0.93, 0.93, 0.455);
	// Parte central:
  glBegin(GL_QUADS);
	  v3f(0.000000, 0.000000, 0.000000);
	  v3f(0.000000, 4589.000000, 0.000000);
	  v3f(7925.000000, 4589.000000, 0.000000);
	  v3f(7925.000000, 0.000000, 0.000000);
  glEnd();

	// Lateral de la puerta (1� parte):
  glBegin(GL_QUADS);
	  v3f(1211.000000, -554.000000, 0.000000);
	  v3f(1211.000000, 5.000000, 0.000000);
	  v3f(5476.000000, 5.000000, 0.000000);
	  v3f(5476.000000, -554.000000, 0.000000);
  glEnd();

	// Lateral de la puerta (trocito intermedio de sobrante defectuoso):
  glBegin(GL_QUADS);
	  v3f(5470.000000, -120.000000, 0.000000);
	  v3f(5470.000000, 10.000000, 0.000000);
	  v3f(6115.000000, 10.000000, 0.000000);
	  v3f(6115.000000, -120.000000, 0.000000);
  glEnd();

	// Lateral de la puerta (2� parte):
  glBegin(GL_QUADS);
	  v3f(6110.000000, -554.000000, 0.000000);
	  v3f(6110.000000, 5.000000, 0.000000);
	  v3f(7925.000000, 5.000000, 0.000000);
	  v3f(7925.000000, -554.000000, 0.000000);
  glEnd();

	// Lateral enfrente de la puerta (1� parte):
  glBegin(GL_QUADS);
	  v3f(2419.000000, 4580.000000, 0.000000);
	  v3f(2419.000000, 4879.000000, 0.000000);
	  v3f(5498.000000, 4879.000000, 0.000000);
	  v3f(5498.000000, 4580.000000, 0.000000);
  glEnd();

	// Lateral enfrente de la puerta (2� parte):
  glBegin(GL_QUADS);
	  v3f(6264.000000, 4580.000000, 0.000000);
	  v3f(6264.000000, 4879.000000, 0.000000);
	  v3f(7925.000000, 4879.000000, 0.000000);
	  v3f(7925.000000, 4580.000000, 0.000000);
  glEnd();

	/* CAMPO DE FUTBOL */
  glColor3f(0., 0.59, 0.);
  glBegin(GL_QUADS); // daremos algo de altura para no causar "conflictos graficos" con el suelo
	  v3f(1560.000000, 596.000000, 25.000000);
	  v3f(1560.000000, 4589.000000, 25.000000);
	  v3f(7925.000000, 4589.000000, 25.000000);
	  v3f(7925.000000, 596.000000, 25.000000);
  glEnd();

	/* PAREDES */

	// Lateral izquierdo:
  glColor3f(0.98, 0.98, 0.98);
  glBegin(GL_QUADS);
	  v3f(0.000000, 0.000000, 3000.000000);
	  v3f(0.000000, 4589.000000, 3000.000000);
	  v3f(0.000000, 4589.000000, 0.000000);
	  v3f(0.000000, 0.000000, 0.000000);
	glEnd();

	// Lateral enfrente de la puerta:
  glBegin(GL_QUADS); // 1� parte
	  v3f(0.000000, 4589.000000, 0.000000);
	  v3f(0.000000, 4589.000000, 3000.000000);
	  v3f(2419.000000, 4589.000000, 3000.000000);
	  v3f(2419.000000, 4589.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 2� parte
	  v3f(2419.000000, 4589.000000, 0.000000);
	  v3f(2419.000000, 4589.000000, 3000.000000);
	  v3f(2419.000000, 4879.000000, 3000.000000);
	  v3f(2419.000000, 4879.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 3� parte
	  v3f(2419.000000, 4879.000000, 0.000000);
	  v3f(2419.000000, 4879.000000, 3000.000000);
	  v3f(5476.000000, 4879.000000, 3000.000000);
	  v3f(5476.000000, 4879.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 4� parte
	  v3f(5476.000000, 4879.000000, 0.000000);
	  v3f(5476.000000, 4879.000000, 3000.000000);
	  v3f(5476.000000, 4589.000000, 3000.000000);
	  v3f(5476.000000, 4589.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 5� parte
	  v3f(5476.000000, 4589.000000, 0.000000);
	  v3f(5476.000000, 4589.000000, 3000.000000);
	  v3f(6264.000000, 4589.000000, 3000.000000);
	  v3f(6264.000000, 4589.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 6� parte
	  v3f(6264.000000, 4589.000000, 0.000000);
	  v3f(6264.000000, 4589.000000, 3000.000000);
	  v3f(6264.000000, 4879.000000, 3000.000000);
	  v3f(6264.000000, 4879.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 6� parte
	  v3f(6264.000000, 4879.000000, 0.000000);
	  v3f(6264.000000, 4879.000000, 3000.000000);
	  v3f(7925.000000, 4879.000000, 3000.000000);
	  v3f(7925.000000, 4879.000000, 0.000000);
	glEnd();

	// Lateral a la derecha de la puerta:
  glBegin(GL_QUADS); // 6� parte
	  v3f(7925.000000, 4879.000000, 0.000000);
	  v3f(7925.000000, 4879.000000, 3000.000000);
	  v3f(7925.000000, -554.000000, 3000.000000);
	  v3f(7925.000000, -554.000000, 0.000000);
	glEnd();

	// Lateral de la puerta:
  glBegin(GL_QUADS); // 1� parte
	  v3f(7925.000000, -554.000000, 0.000000);
	  v3f(7925.000000, -554.000000, 3000.000000);
	  v3f(6110.000000, -554.000000, 3000.000000);
	  v3f(6110.000000, -554.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 2� parte
	  v3f(6110.000000, -554.000000, 0.000000);
	  v3f(6110.000000, -554.000000, 3000.000000);
	  v3f(6110.000000, -119.000000, 3000.000000);
	  v3f(6110.000000, -119.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 3� parte
	  v3f(6110.000000, -119.000000, 0.000000);
	  v3f(6110.000000, -119.000000, 3000.000000);
	  v3f(5476.000000, -119.000000, 3000.000000);
	  v3f(5476.000000, -119.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 4� parte
	  v3f(5476.000000, -554.000000, 0.000000);
	  v3f(5476.000000, -554.000000, 3000.000000);
	  v3f(1211.000000, -554.000000, 3000.000000);
	  v3f(1211.000000, -554.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 5� parte
	  v3f(1211.000000, -554.000000, 0.000000);
	  v3f(1211.000000, -554.000000, 3000.000000);
	  v3f(1211.000000, 0.000000, 3000.000000);
	  v3f(1211.000000, 0.000000, 0.000000);
	glEnd();

  glBegin(GL_QUADS); // 6� parte
	  v3f(1211.000000, 0.000000, 0.000000);
	  v3f(1211.000000, 0.000000, 3000.000000);
	  v3f(0.000000, 0.000000, 3000.000000);
	  v3f(0.000000, 0.000000, 0.000000);
	glEnd();

	/* PUERTA */
  glColor3f(0.67, 0., 0.);
  glBegin(GL_QUADS);
	  v3f(4613.000000, -554.000000, 0.000000);
	  v3f(4613.000000, -554.000000, 2030.000000);
	  v3f(3879.000000, -554.000000, 2030.000000);
	  v3f(3879.000000, -554.000000, 0.000000);
  glEnd();

	/* VENTANA */
  glColor3f(0.5, 0.5, 1.);
  glBegin(GL_QUADS);
	  v3f(2585.000000, 4879.000000, 835.000000);
	  v3f(2585.000000, 4879.000000, 2532.000000);
	  v3f(5335.000000, 4879.000000, 2532.000000);
	  v3f(5335.000000, 4879.000000, 835.000000);
  glEnd();
}

void drawRoboticLabGround () {
  glLineWidth(2.0f);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

	glColor3f (1.,1.,1.);
  glBegin(GL_LINES);
  v3f(0.000000, 0.000000, 0.000000);
  v3f(0.000000, 4589.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(0.000000, 0.000000, 0.000000);
  v3f(1211.000000, 0.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(1211.000000, 0.000000, 0.000000);
  v3f(1211.000000, -554.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(1211.000000, -554.000000, 0.000000);
  v3f(5476.000000, -554.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5476.000000, -554.000000, 0.000000);
  v3f(5476.000000, -119.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5476.000000, -119.000000, 0.000000);
  v3f(6110.000000, -119.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6110.000000, -119.000000, 0.000000);
  v3f(6110.000000, -554.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6110.000000, -554.000000, 0.000000);
  v3f(7925.000000, -554.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7925.000000, -554.000000, 0.000000);
  v3f(7925.000000, 4879.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7925.000000, 4879.000000, 0.000000);
  v3f(6264.000000, 4879.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6264.000000, 4879.000000, 0.000000);
  v3f(6264.000000, 4589.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6264.000000, 4589.000000, 0.000000);
  v3f(5498.000000, 4589.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5498.000000, 4589.000000, 0.000000);
  v3f(5498.000000, 4879.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5498.000000, 4879.000000, 0.000000);
  v3f(2419.000000, 4879.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2419.000000, 4879.000000, 0.000000);
  v3f(2419.000000, 4589.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2419.000000, 4589.000000, 0.000000);
  v3f(0.000000, 4589.000000, 0.000000);
  glEnd();

  glColor3f(1., 1., 1.);
	// LINEAS LIMITROFES DEL CAMPO FUTBOL
  glBegin(GL_LINES);
  v3f(1560.000000, 596.000000, 0.000000);
  v3f(1560.000000, 4589.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5498.000000, 4589.000000, 0.000000);
  v3f(2419.000000, 4589.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(1560.000000, 596.000000, 0.000000);
  v3f(7925.000000, 596.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6264.000000, 4589.000000, 0.000000);
  v3f(7925.000000, 4589.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2160.000000, 796.000000, 0.000000);
  v3f(2160.000000, 4301.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2160.000000, 796.000000, 0.000000);
  v3f(7560.000000, 796.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2160.000000, 4301.000000, 0.000000);
  v3f(7560.000000, 4301.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7560.000000, 796.000000, 0.000000);
  v3f(7560.000000, 4301.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(4860.000000, 796.000000, 0.000000);
  v3f(4860.000000, 4301.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2160.000000, 2051.000000, 0.000000);
  v3f(2810.000000, 2051.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2810.000000, 2051.000000, 0.000000);
  v3f(2810.000000, 3351.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2810.000000, 3351.000000, 0.000000);
  v3f(2160.000000, 3351.000000, 0.000000 );
  glEnd();

  glBegin(GL_LINES);
  v3f(7560.000000, 2051.000000, 0.000000);
  v3f(6910.000000, 2051.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7560.000000, 3351.000000, 0.000000);
  v3f(6910.000000, 3351.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6910.000000, 2051.000000, 0.000000);
  v3f(6910.000000, 3351.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(4500.000000, 2341.000000, 0.000000);
  v3f(4500.000000, 3061.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5220.000000, 2341.000000, 0.000000);
  v3f(5220.000000, 3061.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(4500.000000, 2341.000000, 0.000000);
  v3f(5220.000000, 2341.000000, 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(4500.000000, 3061.000000, 0.000000);
  v3f(5220.000000, 3061.000000, 0.000000);
  glEnd();
}

void drawRoboticLabLines () {
  glColor3f(1., 1., 1.);
  glBegin(GL_LINES);
  v3f(0.000000, 0.000000, 3000.000000);
  v3f(0.000000, 4589.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(0.000000, 0.000000, 3000.000000);
  v3f(1211.000000, 0.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(1211.000000, 0.000000, 3000.000000);
  v3f(1211.000000, -554.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(1211.000000, -554.000000, 3000.000000);
  v3f(5476.000000, -554.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5476.000000, -554.000000, 3000.000000);
  v3f(5476.000000, -119.000000, 3000.000000);
  glEnd();
	
  glBegin(GL_LINES);
  v3f(5476.000000, -119.000000, 3000.000000);
  v3f(6110.000000, -119.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6110.000000, -119.000000, 3000.000000);
  v3f(6110.000000, -554.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6110.000000, -554.000000, 3000.000000);
  v3f(7925.000000, -554.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7925.000000, -554.000000, 3000.000000);
  v3f(7925.000000, 4879.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7925.000000, 4879.000000, 3000.000000);
  v3f(6264.000000, 4879.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6264.000000, 4879.000000, 3000.000000);
  v3f(6264.000000, 4589.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6264.000000, 4589.000000, 3000.000000);
  v3f(5498.000000, 4589.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5498.000000, 4589.000000, 3000.000000);
  v3f(5498.000000, 4879.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5498.000000, 4879.000000, 3000.000000);
  v3f(2419.000000, 4879.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2419.000000, 4879.000000, 3000.000000);
  v3f(2419.000000, 4589.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2419.000000, 4589.000000, 3000.000000);
  v3f(0.000000, 4589.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(0.000000, 0.000000, 30.000000);
  v3f(0.000000, 4589.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(0.000000, 0.000000, 30.000000);
  v3f(1211.000000, 0.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(1211.000000, 0.000000, 30.000000);
  v3f(1211.000000, -554.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(1211.000000, -554.000000, 30.000000);
  v3f(5476.000000, -554.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5476.000000, -554.000000, 30.000000);
  v3f(5476.000000, -119.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5476.000000, -119.000000, 30.000000);
  v3f(6110.000000, -119.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6110.000000, -119.000000, 30.000000);
  v3f(6110.000000, -554.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6110.000000, -554.000000, 30.000000);
  v3f(7925.000000, -554.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7925.000000, -554.000000, 30.000000);
  v3f(7925.000000, 4879.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7925.000000, 4879.000000, 30.000000);
  v3f(6264.000000, 4879.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6264.000000, 4879.000000, 30.000000);
  v3f(6264.000000, 4589.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6264.000000, 4589.000000, 30.000000);
  v3f(5498.000000, 4589.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5498.000000, 4589.000000, 30.000000);
  v3f(5498.000000, 4879.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5498.000000, 4879.000000, 30.000000);
  v3f(2419.000000, 4879.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2419.000000, 4879.000000, 30.000000);
  v3f(2419.000000, 4589.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2419.000000, 4589.000000, 30.000000);
  v3f(0.000000, 4589.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(0.000000, 0.000000, 0.000000);
  v3f(0.000000, 0.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(1211.000000, 0.000000, 0.000000);
  v3f(1211.000000, 0.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(1211.000000, -554.000000, 0.000000);
  v3f(1211.000000, -554.000000, 3000.000000);
  glEnd();

	// LINEAS LIMITROFES PUERTA-------------------
  glColor3f(1., 0.5, 0.);
  glBegin(GL_LINES);
  v3f(3879.000000, -554.000000, 0.000000);
  v3f(3879.000000, -554.000000, 2030.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(3879.000000, -554.000000, 2030.000000);
  v3f(4613.000000, -554.000000, 2030.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(4613.000000, -554.000000, 2030.000000);
  v3f(4613.000000, -554.000000, 0.000000);
  glEnd();
	//--------------------------------------------

  glColor3f(1., 1., 1.);
  glBegin(GL_LINES);
  v3f(5476.000000, -554.000000, 0.000000);
  v3f(5476.000000, -554.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5476.000000, -119.000000, 0.000000);
  v3f(5476.000000, -119.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6110.000000, -119.000000, 0.000000);
  v3f(6110.000000, -119.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6110.000000, -554.000000, 0.000000);
  v3f(6110.000000, -554.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7925.000000, -554.000000, 0.000000);
  v3f(7925.000000, -554.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7925.000000, 4879.000000, 0.000000);
  v3f(7925.000000, 4879.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6264.000000, 4879.000000, 0.000000);
  v3f(6264.000000, 4879.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6264.000000, 4589.000000, 0.000000);
  v3f(6264.000000, 4589.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5498.000000, 4589.000000, 0.000000);
  v3f(5498.000000, 4589.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5498.000000, 4879.000000, 0.000000);
  v3f(5498.000000, 4879.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2419.000000, 4879.000000, 0.000000);
  v3f(2419.000000, 4879.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2419.000000, 4589.000000, 0.000000);
  v3f(2419.000000, 4589.000000, 3000.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(0.000000, 4589.000000, 0.000000);
  v3f(0.000000, 4589.000000, 3000.000000);
  glEnd();

	// LINEAS LIMITROFES DE LA VENTANA-----------
  glColor3f(1., 0.5, 0.);
  glBegin(GL_LINES);
  v3f(2585.000000, 4879.000000, 835.000000);
  v3f(5335.000000, 4879.000000, 835.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2585.000000, 4879.000000, 2532.000000);
  v3f(5335.000000, 4879.000000, 2532.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2585.000000, 4879.000000, 835.000000);
  v3f(2585.000000, 4879.000000, 2532.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5335.000000, 4879.000000, 835.000000);
  v3f(5335.000000, 4879.000000, 2532.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(3960.000000, 4879.000000, 835.000000);
  v3f(3960.000000, 4879.000000, 2532.000000);
  glEnd();
	//--------------------------------------------

  glColor3f(1., 1., 1.);
	// LINEAS LIMITROFES DEL CAMPO FUTBOL
  glBegin(GL_LINES);
  v3f(1560.000000, 596.000000, 30.000000);
  v3f(1560.000000, 4589.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5498.000000, 4589.000000, 30.000000);
  v3f(2419.000000, 4589.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(1560.000000, 596.000000, 30.000000);
  v3f(7925.000000, 596.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6264.000000, 4589.000000, 30.000000);
  v3f(7925.000000, 4589.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2160.000000, 796.000000, 30.000000);
  v3f(2160.000000, 4301.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2160.000000, 796.000000, 30.000000);
  v3f(7560.000000, 796.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2160.000000, 4301.000000, 30.000000);
  v3f(7560.000000, 4301.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7560.000000, 796.000000, 30.000000);
  v3f(7560.000000, 4301.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(4860.000000, 796.000000, 30.000000);
  v3f(4860.000000, 4301.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2160.000000, 2051.000000, 30.000000);
  v3f(2810.000000, 2051.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2810.000000, 2051.000000, 30.000000);
  v3f(2810.000000, 3351.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2810.000000, 3351.000000, 30.000000);
  v3f(2160.000000, 3351.000000, 30.000000 );
  glEnd();

  glBegin(GL_LINES);
  v3f(7560.000000, 2051.000000, 30.000000);
  v3f(6910.000000, 2051.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(7560.000000, 3351.000000, 30.000000);
  v3f(6910.000000, 3351.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(6910.000000, 2051.000000, 30.000000);
  v3f(6910.000000, 3351.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(4500.000000, 2341.000000, 30.000000);
  v3f(4500.000000, 3061.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(5220.000000, 2341.000000, 30.000000);
  v3f(5220.000000, 3061.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(4500.000000, 2341.000000, 30.000000);
  v3f(5220.000000, 2341.000000, 30.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(4500.000000, 3061.000000, 30.000000);
  v3f(5220.000000, 3061.000000, 30.000000);
  glEnd();
}

/* We want to know the intersection between a line given by two points (A and B) on 3D, and a plan (the ground, for example). Then, we'll get another point; it'll be our solution */
int linePlaneIntersection (HPoint3D A, HPoint3D B, HPoint3D *intersectionPoint) {
	HPoint3D v;	// Line director vector: it the same to take A or B as origin or destination extrem...

	A.X = -A.X;
	A.Y = -A.Y;
	A.Z = -A.Z;

	B.X = -B.X;
	B.Y = -B.Y;
	B.Z = -B.Z;

	v.X = (A.X - B.X);
	v.Y = (A.Y - B.Y);
	v.Z = (A.Z - B.Z);

	// We'll calculate the groun intersection (Z = 0) on our robot system. Parametric equations:
	intersectionPoint->Z = 0; // intersectionPoint->Z = A.Z + t*v.Z => t = (-A.Z / v.Z)
	t = (-A.Z) / (v.Z);

	intersectionPoint->X = A.X + (t*v.X);
	intersectionPoint->Y = A.Y + (t*v.Y);
}

void pointProjection (HPoint2D point, char **myColor) {
	if (myColor == mycolorA)
		myCamera = &robotCamera;
	else if (myColor == mycolorB)
		myCamera = &robotCamera2;

	myActualPoint2D.y = point.x;
	myActualPoint2D.x = point.y;
	myActualPoint2D.h = 1.;

	// Proyectamos tal punto en 3D sobre el Plano Imagen de nuestra c�mara virtual
	backproject(&myActualPoint3D, myActualPoint2D, *myCamera);

	// Coordenadas en 3D de la posicion de la c�mara
	cameraPos3D.X = myCamera->position.X;
	cameraPos3D.Y = myCamera->position.Y;
	cameraPos3D.Z = myCamera->position.Z;
	cameraPos3D.H = 1;
/*
	// Distancia entre los puntos myActualPoint3D - cameraPos3D
	distanciaPlanoImagen=(float)sqrt((double)((myActualPoint3D.X-cameraPos3D.X)*(myActualPoint3D.X-cameraPos3D.X)+(myActualPoint3D.Y-cameraPos3D.Y)*(myActualPoint3D.Y-cameraPos3D.Y)+(myActualPoint3D.Z-cameraPos3D.Z)*(myActualPoint3D.Z-cameraPos3D.Z)));
	incremento = ROOM_MAX_X/distanciaPlanoImagen; // distancia "relativa"
	
	// Alargamos tal distancia (lanzamiento del rayo �ptico)
	myEndPoint3D.X = ((incremento * myActualPoint3D.X + (1. - incremento) * cameraPos3D.X));
	myEndPoint3D.Y = ((incremento * myActualPoint3D.Y + (1. - incremento) * cameraPos3D.Y));
	myEndPoint3D.Z = ((incremento * myActualPoint3D.Z + (1. - incremento) * cameraPos3D.Z));
	myEndPoint3D.H = 1.;
*/

	linePlaneIntersection (myActualPoint3D, cameraPos3D, &intersectionPoint); // luego negaremos los puntos

	if ((fronteraView == FALSE) && (groundView == FALSE)) {
		glColor3f (0.5,0.5,0.5);
	  glBegin(GL_LINES);
		  v3f(-cameraPos3D.X, -cameraPos3D.Y, -cameraPos3D.Z);  
		  v3f(-myActualPoint3D.X, -myActualPoint3D.Y, -myActualPoint3D.Z);
	  glEnd();
	} else if (fronteraView == TRUE) {
		if (actualFronteraPoint == ((SIFNTSC_COLUMNS*SIFNTSC_ROWS) - 1))
			actualFronteraPoint = 0;

		fronteraPoints[actualFronteraPoint].X = intersectionPoint.X;
		fronteraPoints[actualFronteraPoint].Y = intersectionPoint.Y;
		fronteraPoints[actualFronteraPoint].Z = intersectionPoint.Z;

		actualFronteraPoint ++;
	}

	if (groundView == TRUE) {
		if (actualGroundPoint == ((SIFNTSC_COLUMNS*SIFNTSC_ROWS) - 1))
			actualGroundPoint = 0;

		groundPoints[actualGroundPoint].X = intersectionPoint.X;
		groundPoints[actualGroundPoint].Y = intersectionPoint.Y;
		groundPoints[actualGroundPoint].Z = intersectionPoint.Z;

		actualGroundPoint ++;
	}
}

void drawOpticalRays (TPinHoleCamera *cam) {
  HPoint3D a3A;
  HPoint2D a;

	glColor3f( 0.7, 0.7, 0.7 );

	cameraPos3D.X=0.;
	cameraPos3D.Y=0.;
	cameraPos3D.Z=0.;
	cameraPos3D.H=1.;

	/* optical axis of camera */
	//if (((display_state&DISPLAY_COLORIMAGEA)!=0) && (mycolorA!=NULL) && (cam == &robotCamera)) {

		a.x = SIFNTSC_ROWS-1;
		a.y = SIFNTSC_COLUMNS-1;
		a.h = 1.;
		backproject (&a3A, a, *cam);

		distanciaPlanoImagen=(float)sqrt((double)((a3A.X-cameraPos3D.X)*(a3A.X-cameraPos3D.X)+(a3A.Y-cameraPos3D.Y)*(a3A.Y-cameraPos3D.Y)+(a3A.Z-cameraPos3D.Z)*(a3A.Z-cameraPos3D.Z)));
		incremento = ROOM_MAX_X/distanciaPlanoImagen; // distancia "relativa"

		a3A.X = incremento * a3A.X + (1. - incremento) * cameraPos3D.X;
		a3A.Y = incremento * a3A.Y + (1. - incremento) * cameraPos3D.Y;
		a3A.Z = incremento * a3A.Z + (1. - incremento) * cameraPos3D.Z;
		a3A.H = 1.;

		glBegin(GL_LINES);
			v3f( a3A.X, a3A.Y, a3A.Z );
			v3f( cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
		glEnd();

		/**/
		a.x = 0;
		a.y = 0;
		a.h = 1.;
		backproject (&a3A, a, *cam);

		distanciaPlanoImagen=(float)sqrt((double)((a3A.X-cameraPos3D.X)*(a3A.X-cameraPos3D.X)+(a3A.Y-cameraPos3D.Y)*(a3A.Y-cameraPos3D.Y)+(a3A.Z-cameraPos3D.Z)*(a3A.Z-cameraPos3D.Z)));
		incremento = ROOM_MAX_X/distanciaPlanoImagen; // distancia "relativa"

		a3A.X = incremento * a3A.X + (1. - incremento) * cameraPos3D.X;
		a3A.Y = incremento * a3A.Y + (1. - incremento) * cameraPos3D.Y;
		a3A.Z = incremento * a3A.Z + (1. - incremento) * cameraPos3D.Z;
		a3A.H = 1.;

		glBegin(GL_LINES);
			v3f( a3A.X, a3A.Y, a3A.Z );
			v3f( cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
		glEnd();

		/**/
		a.x = SIFNTSC_ROWS-1;
		a.y = 0;
		a.h = 1.;
		backproject (&a3A, a, *cam);

		distanciaPlanoImagen=(float)sqrt((double)((a3A.X-cameraPos3D.X)*(a3A.X-cameraPos3D.X)+(a3A.Y-cameraPos3D.Y)*(a3A.Y-cameraPos3D.Y)+(a3A.Z-cameraPos3D.Z)*(a3A.Z-cameraPos3D.Z)));
		incremento = ROOM_MAX_X/distanciaPlanoImagen; // distancia "relativa"

		a3A.X = incremento * a3A.X + (1. - incremento) * cameraPos3D.X;
		a3A.Y = incremento * a3A.Y + (1. - incremento) * cameraPos3D.Y;
		a3A.Z = incremento * a3A.Z + (1. - incremento) * cameraPos3D.Z;
		a3A.H = 1.;

		glBegin(GL_LINES);
			v3f( a3A.X, a3A.Y, a3A.Z );
			v3f( cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
		glEnd();

		/**/
		a.x = 0;
		a.y = SIFNTSC_COLUMNS-1;
		a.h = 1.;
		backproject (&a3A, a, *cam);

		distanciaPlanoImagen=(float)sqrt((double)((a3A.X-cameraPos3D.X)*(a3A.X-cameraPos3D.X)+(a3A.Y-cameraPos3D.Y)*(a3A.Y-cameraPos3D.Y)+(a3A.Z-cameraPos3D.Z)*(a3A.Z-cameraPos3D.Z)));
		incremento = ROOM_MAX_X/distanciaPlanoImagen; // distancia "relativa"

		a3A.X = incremento * a3A.X + (1. - incremento) * cameraPos3D.X;
		a3A.Y = incremento * a3A.Y + (1. - incremento) * cameraPos3D.Y;
		a3A.Z = incremento * a3A.Z + (1. - incremento) * cameraPos3D.Z;
		a3A.H = 1.;

		glBegin(GL_LINES);
			v3f( a3A.X, a3A.Y, a3A.Z );
			v3f( cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
		glEnd();
	//} // fin if (dibujamos los rayos opticos, si estamos en la c�mara oportuna...)
}

void drawCam(TPinHoleCamera *cam, int b)
{
  HPoint3D a3A, a3B, a3C, a3D;
  HPoint2D a;

/*	if (b==2){
		glColor3f( 0.7, 0.7, 0.7 );
	} else if (b==3){
		glColor3f( 0.7, 0.7, 0.7 );
	} else if (b==4){
		glColor3f( 0.7, 0.7, 0.7 );
	} else if (b==5){
		glColor3f( 0.7, 0.7, 0.7 );
	}
*/
	glColor3f( 0.7, 0.7, 0.7 );

	cameraPos3D.X=-cam->position.X;
	cameraPos3D.Y=-cam->position.Y;
	cameraPos3D.Z=-cam->position.Z;
	cameraPos3D.H=cam->position.H;

	/* optical axis of camera */
	if ((((display_state&DISPLAY_COLORIMAGEA)!=0) && (mycolorA!=NULL) && (cam == &robotCamera)) ||
			(((display_state&DISPLAY_COLORIMAGEB)!=0) && (mycolorB!=NULL) && (cam == &robotCamera2))) {

		a.x = SIFNTSC_ROWS-1;
		a.y = SIFNTSC_COLUMNS-1;
		a.h = 1.;
		//a.y = GRAPHIC_TO_OPTICAL_Y(a.y,a.x);
		//a.x = GRAPHIC_TO_OPTICAL_X(a.y,a.x);
		backproject (&a3A, a, *cam);

		a3A.X = -a3A.X;
		a3A.Y = -a3A.Y;
		a3A.Z = -a3A.Z;

		distanciaPlanoImagen=(float)sqrt((double)((a3A.X-cameraPos3D.X)*(a3A.X-cameraPos3D.X)+(a3A.Y-cameraPos3D.Y)*(a3A.Y-cameraPos3D.Y)+(a3A.Z-cameraPos3D.Z)*(a3A.Z-cameraPos3D.Z)));
		incremento = ROOM_MAX_X/distanciaPlanoImagen; // distancia "relativa"

		a3A.X = incremento * a3A.X + (1. - incremento) * cameraPos3D.X;
		a3A.Y = incremento * a3A.Y + (1. - incremento) * cameraPos3D.Y;
		a3A.Z = incremento * a3A.Z + (1. - incremento) * cameraPos3D.Z;
		a3A.H = 1.;

		glBegin(GL_LINES);
			v3f( a3A.X, a3A.Y, a3A.Z );
			v3f( cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
		glEnd();

		/**/
		a.x = 0;
		a.y = 0;
		a.h = 1.;
		//a.y = GRAPHIC_TO_OPTICAL_Y(a.y,a.x);
		//a.x = GRAPHIC_TO_OPTICAL_X(a.y,a.x);

		backproject (&a3A, a, *cam);

		distanciaPlanoImagen=(float)sqrt((double)((a3A.X-cameraPos3D.X)*(a3A.X-cameraPos3D.X)+(a3A.Y-cameraPos3D.Y)*(a3A.Y-cameraPos3D.Y)+(a3A.Z-cameraPos3D.Z)*(a3A.Z-cameraPos3D.Z)));
		incremento = ROOM_MAX_X/distanciaPlanoImagen; // distancia "relativa"

		a3A.X = incremento * a3A.X + (1. - incremento) * cameraPos3D.X;
		a3A.Y = incremento * a3A.Y + (1. - incremento) * cameraPos3D.Y;
		a3A.Z = incremento * a3A.Z + (1. - incremento) * cameraPos3D.Z;
		a3A.H = 1.;

		glBegin(GL_LINES);
			v3f( -a3A.X, -a3A.Y, -a3A.Z );
			v3f( cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
		glEnd();

		/**/
		a.x = SIFNTSC_ROWS-1;
		a.y = 0;
		a.h = 1.;
		//a.y = GRAPHIC_TO_OPTICAL_Y(a.y,a.x);
		//a.x = GRAPHIC_TO_OPTICAL_X(a.y,a.x);

		backproject (&a3A, a, *cam);

		a3A.X = -a3A.X;
		a3A.Y = -a3A.Y;
		a3A.Z = -a3A.Z;

		distanciaPlanoImagen=(float)sqrt((double)((a3A.X-cameraPos3D.X)*(a3A.X-cameraPos3D.X)+(a3A.Y-cameraPos3D.Y)*(a3A.Y-cameraPos3D.Y)+(a3A.Z-cameraPos3D.Z)*(a3A.Z-cameraPos3D.Z)));
		incremento = ROOM_MAX_X/distanciaPlanoImagen; // distancia "relativa"

		a3A.X = incremento * a3A.X + (1. - incremento) * cameraPos3D.X;
		a3A.Y = incremento * a3A.Y + (1. - incremento) * cameraPos3D.Y;
		a3A.Z = incremento * a3A.Z + (1. - incremento) * cameraPos3D.Z;
		a3A.H = 1.;

		glBegin(GL_LINES);
			v3f( a3A.X, a3A.Y, a3A.Z );
			v3f( cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
		glEnd();

		/**/
		a.x = 0;
		a.y = SIFNTSC_COLUMNS-1;
		a.h = 1.;
		//a.y = GRAPHIC_TO_OPTICAL_Y(a.y,a.x);
		//a.x = GRAPHIC_TO_OPTICAL_X(a.y,a.x);

		backproject (&a3A, a, *cam);

		a3A.X = -a3A.X;
		a3A.Y = -a3A.Y;
		a3A.Z = -a3A.Z;

		distanciaPlanoImagen=(float)sqrt((double)((a3A.X-cameraPos3D.X)*(a3A.X-cameraPos3D.X)+(a3A.Y-cameraPos3D.Y)*(a3A.Y-cameraPos3D.Y)+(a3A.Z-cameraPos3D.Z)*(a3A.Z-cameraPos3D.Z)));
		incremento = ROOM_MAX_X/distanciaPlanoImagen; // distancia "relativa"

		a3A.X = incremento * a3A.X + (1. - incremento) * cameraPos3D.X;
		a3A.Y = incremento * a3A.Y + (1. - incremento) * cameraPos3D.Y;
		a3A.Z = incremento * a3A.Z + (1. - incremento) * cameraPos3D.Z;
		a3A.H = 1.;

		glBegin(GL_LINES);
			v3f( a3A.X, a3A.Y, a3A.Z );
			v3f( cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
		glEnd();
	} // fin if (dibujamos los rayos opticos, si estamos en la c�mara oportuna...)

	/* FieldOfView of camera */
	a.x = SIFNTSC_ROWS - 1.;
	a.y = SIFNTSC_COLUMNS - 1.;
	a.h = 1.;
	backproject(&a3A, a, *cam);
	a.x = 0.;
	a.y = SIFNTSC_COLUMNS - 1.;
	a.h = 1.;
	backproject(&a3B, a, *cam);
	a.x = 0.;
	a.y = 0.;
	a.h = 1.;
	backproject(&a3C, a, *cam);
	a.x = SIFNTSC_ROWS - 1.;
	a.y = 0.;
	a.h = 1.;
	backproject(&a3D, a, *cam);

	// first triangle
	glBegin(GL_LINES);
		glVertex3f(cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
		glVertex3f(-a3A.X, -a3A.Y, -a3A.Z );
	glEnd();

	glBegin(GL_LINES);
		glVertex3f(-a3A.X, -a3A.Y, -a3A.Z );
		glVertex3f(-a3B.X, -a3B.Y, -a3B.Z );
	glEnd ();

	glBegin(GL_LINES);
		glVertex3f(-a3B.X, -a3B.Y, -a3B.Z );
		glVertex3f(cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
	glEnd ();

	glColor3f( 0.3, 0.3, 0.3 );
	// second triangle
	glBegin(GL_LINES);
		glVertex3f(cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
		glVertex3f(-a3B.X, -a3B.Y, -a3B.Z );
	glEnd();

	glBegin(GL_LINES);
		glVertex3f(-a3B.X, -a3B.Y, -a3B.Z );
		glVertex3f(-a3C.X, -a3C.Y, -a3C.Z );
	glEnd();

	glBegin(GL_LINES);
		glVertex3f(-a3C.X, -a3C.Y, -a3C.Z );
		glVertex3f(cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
	glEnd();

	glColor3f( 0.7, 0.7, 0.7 );
	// third triangle
	glBegin(GL_LINES);
		glVertex3f(cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
		glVertex3f(-a3C.X, -a3C.Y, -a3C.Z );
	glEnd();

	glBegin(GL_LINES);
		glVertex3f(-a3C.X, -a3C.Y, -a3C.Z );
		glVertex3f(-a3D.X, -a3D.Y, -a3D.Z );
	glEnd();

	glBegin(GL_LINES);
		glVertex3f(-a3D.X, -a3D.Y, -a3D.Z );
		glVertex3f(cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
	glEnd();

	glColor3f( 0.3, 0.3, 0.3 );
	// last triangle
	glBegin(GL_LINES);
		glVertex3f(cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
		glVertex3f(-a3D.X, -a3D.Y, -a3D.Z );
	glEnd();

	glBegin(GL_LINES);
		glVertex3f(-a3D.X, -a3D.Y, -a3D.Z );
		glVertex3f(-a3A.X, -a3A.Y, -a3A.Z );
	glEnd();

	glBegin(GL_LINES);
		glVertex3f(-a3A.X, -a3A.Y, -a3A.Z );
		glVertex3f(cameraPos3D.X, cameraPos3D.Y, cameraPos3D.Z );
	glEnd();

	glColor3f( 0.1, 0.1, 0.1 );
	// square
	glBegin(GL_LINES);
		glVertex3f(-a3A.X, -a3A.Y, -a3A.Z );
		glVertex3f(-a3D.X, -a3D.Y, -a3D.Z );
	glEnd();

	glBegin(GL_LINES);
		glVertex3f(-a3D.X, -a3D.Y, -a3D.Z );
		glVertex3f(-a3C.X, -a3C.Y, -a3C.Z );
	glEnd();

	glBegin(GL_LINES);
		glVertex3f(-a3C.X, -a3C.Y, -a3C.Z );
		glVertex3f(-a3B.X, -a3B.Y, -a3B.Z );
	glEnd();

	glBegin(GL_LINES);
		glVertex3f(-a3B.X, -a3B.Y, -a3B.Z );
		glVertex3f(-a3A.X, -a3A.Y, -a3A.Z );
	glEnd();
}

void initVirtualCam () {
  float r,lati,longi;

	if (!flashImage) {
	  if (foa_mode){
	    longi=2*PI*mouse_on_canvas.x/360.;
	    lati=2*PI*mouse_on_canvas.y/360.;
	    r=500;
	    virtualcam1.foa.X=r*cos(lati)*cos(longi);
	    virtualcam1.foa.Y=r*cos(lati)*sin(longi);
	    virtualcam1.foa.Z=r*sin(lati);
	  }
	  
	  if (cam_mode) {
	    longi=2*PI*mouse_on_canvas.x/360.;
	    lati=2*PI*mouse_on_canvas.y/360.;
	    
	    virtualcam1.position.X=radius*cos(lati)*cos(longi);
	    virtualcam1.position.Y=radius*cos(lati)*sin(longi);
	    virtualcam1.position.Z=radius*sin(lati);
	    
	    virtualcam1.foa.X=0;
	    virtualcam1.foa.Y=0;
	    virtualcam1.foa.Z=0;
	  }
	}
}

void initOccupancyGrid () {
	int i;

	centrogrid.x = 275.9;//myencoders[0];
	centrogrid.y = 276.5;//myencoders[1];

	occupancyGrid = inicia_grid (centrogrid, configFile); // memoria

	tempMap.point = (Tvoxel*)malloc((occupancyGrid->size*occupancyGrid->size)*sizeof(Tvoxel));
	tempMap.state = (int*)malloc((occupancyGrid->size*occupancyGrid->size)*sizeof(int));
	tempMap.updatingTime = (unsigned long int*)malloc((occupancyGrid->size*occupancyGrid->size)*sizeof(unsigned long int));

	for(i=0;i< ((occupancyGrid->size)*(occupancyGrid->size)) ;i++) {
		occupancyGrid->map[i].estado = EMPTY;
		occupancyGrid->map[i].ultima_actualizacion = dameTiempo ();

		tempMap.point[i] = occupancyGrid->map[i].centro;
		tempMap.state[i] = EMPTY;
		tempMap.updatingTime[i] = occupancyGrid->map[i].ultima_actualizacion;
	}

	if (occupancyGrid==NULL) { // si no hubo ning�n problema en la inicializaci�n
/*		if (DEBUG) printf("Occupancy Grid initialized correctly--> Size %.0f mm, each cell %.0f mm\n",occupancyGrid->size*occupancyGrid->resolucion,occupancyGrid->resolucion);
	} else {*/
		if (DEBUG) printf("Not enough memory for occupancy grid\n");
		exit (-1);
	}
} 

void updateRejillaGPP () {
	int i, j, k, celda;
	Tvoxel tope, posRobot, actual;
	int finRayo;
	int celdilla;

	updateGridTiming ();

	// 1�.- Actualizaremos los puntos de forma que solaparemos los que ya ten�amos, en la nueva rejilla
	if (pendienteDeReubicacionGrafica) { // se ha reubicado la rejilla, por lo que puede haber solapamiento
		if (DEBUG) printf("Updating occupancy grid according to previous occupancy grid and actual visual information...\n");
		for(j=0;j< ((occupancyGrid->size)*(occupancyGrid->size)); j++) {
			occupancyGrid->map[j].estado = EMPTY; // inicializaci�n: desconocemos la nueva zona
			occupancyGrid->map[j].ultima_actualizacion = dameTiempo ();
			for(k=0;k< ((occupancyGrid->size)*(occupancyGrid->size)); k++) {
				if ((occupancyGrid->map[j].centro.x == tempMap.point[k].x) && (occupancyGrid->map[j].centro.y == tempMap.point[k].y)) {
					occupancyGrid->map[j].estado = tempMap.state[k]; // solapamos estado: es zona conocida
					occupancyGrid->map[j].ultima_actualizacion = tempMap.updatingTime[k]; // nos quedamos con su tiempo
					break;
				}
			}
		}
		pendienteDeReubicacionGrafica = FALSE;
	}

	posRobot.x = 0; // TODO:myencoders [0];
	posRobot.y = 0; // TODO:myencoders [1];

	// 2�.- Ahora actualizaremos seg�n la informaci�n que nos llega de los l�sers: zona conocida tambi�n y actualizada
	
}

void frontera_iteration()
{
	speedcounter(frontera_id); 
/*
	if (isInitGrid == FALSE) { // podremos visualizar la informaci�n aun sin pinchar con el rat�n
		initOccupancyGrid ();
		reubica_grid (occupancyGrid, centrogrid);
		updateRejillaGPP ();
	} else
		updateRejillaGPP (); // iremos actualizando todo el rato, luego la hebra de visualizaci�n OGL ir� por su lado
*/
}

void frontera_suspend()
{
  if ((display_state & DISPLAY_COLORIMAGEA)!=0)
     colorAsuspend();
  if ((display_state & DISPLAY_COLORIMAGEB)!=0)
     colorBsuspend();

	RGB2HSV_destroyTable();

  pthread_mutex_lock(&(all[frontera_id].mymutex));
  put_state(frontera_id,slept);
  printf("frontera: off\n");
  pthread_mutex_unlock(&(all[frontera_id].mymutex));
}

void frontera_resume(int father, int *brothers, arbitration fn)
{
	int i;

  pthread_mutex_lock(&(all[frontera_id].mymutex));
  /* this schema resumes its execution with no children at all */
  for(i=0;i<MAX_SCHEMAS;i++) all[frontera_id].children[i]=FALSE;
 
  all[frontera_id].father=father;
  if (brothers!=NULL)
    {
      for(i=0;i<MAX_SCHEMAS;i++) frontera_brothers[i]=-1;
      i=0;
      while(brothers[i]!=-1) {frontera_brothers[i]=brothers[i];i++;}
    }
  frontera_callforarbitration=fn;
  put_state(frontera_id,notready);

  printf("frontera: on\n");
  pthread_cond_signal(&(all[frontera_id].condition));
  pthread_mutex_unlock(&(all[frontera_id].mymutex));
}

void *frontera_thread(void *not_used) 
{
  struct timeval a,b;
  long diff, next;

  for(;;)
    {
      pthread_mutex_lock(&(all[frontera_id].mymutex));

      if (all[frontera_id].state==slept) 
	{
	  pthread_cond_wait(&(all[frontera_id].condition),&(all[frontera_id].mymutex));
	  pthread_mutex_unlock(&(all[frontera_id].mymutex));
	}
      else 
	{
	  /* check preconditions. For now, preconditions are always satisfied*/
	  if (all[frontera_id].state==notready) put_state(frontera_id,ready);
	  else if (all[frontera_id].state==ready)	  /* check brothers and arbitrate. For now this is the only winner */
	    { 
	      put_state(frontera_id,winner);
	    }	  
	  else if (all[frontera_id].state==winner);

	  if (all[frontera_id].state==winner)
	    /* I'm the winner and must execute my iteration */
	    {
	      pthread_mutex_unlock(&(all[frontera_id].mymutex));
	      gettimeofday(&a,NULL);
	      frontera_iteration();
	      gettimeofday(&b,NULL);  

	      diff = (b.tv_sec-a.tv_sec)*1000000+b.tv_usec-a.tv_usec;
	      next = frontera_cycle*1000-diff-10000; 
	      if (next>0) 
		/* discounts 10ms taken by calling usleep itself */
		usleep(frontera_cycle*1000-diff);
	      else 
		/* just let this iteration go away. overhead time negligible */
		{printf("time interval violated: frontera\n"); 
		usleep(frontera_cycle*1000);
		}
	    }
	  else 
	    {
	      pthread_mutex_unlock(&(all[frontera_id].mymutex));
	      usleep(frontera_cycle*1000);
	    }
	}
    }
}

void frontera_close()
{
  pthread_mutex_lock(&(all[frontera_id].mymutex));
  frontera_suspend();  
  pthread_mutex_unlock(&(all[frontera_id].mymutex));
  sleep(2);
  fl_free_form(fd_fronteragui->fronteragui);
  free(imagenA_buf);
  free(imagenB_buf);
  free(imagenFiltrada_buf);
  free(imagenSuelo_buf);
}

static int InitOGL(FL_OBJECT *ob, Window win,int w,int h, XEvent *xev, void *ud)
 /* Inicializa OpenGL con los parametros que diran como se visualiza. */
{
   glViewport(0,0,(GLint)w,(GLint)h);  
   glDrawBuffer(GL_BACK);
   glClearColor(0.6f, 0.8f, 1.0f, 0.0f);  
   glClearDepth(1.0);
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
   glEnable(GL_TEXTURE_2D);     /* Enable Texture Mapping */
   glEnable (GL_AUTO_NORMAL);
   glEnable (GL_NORMALIZE);  
   glEnable(GL_DEPTH_TEST);     /* Enables Depth Testing */
   glDepthFunc(GL_LESS);  
   glShadeModel(GL_SMOOTH);     /* Enables Smooth Color Shading */
   glEnable(GL_BLEND);
   glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

   return 0;
}

static int InitFronteraOGL(FL_OBJECT *ob, Window win,int w,int h, XEvent *xev, void *ud)
 /* Inicializa OpenGL con los parametros que diran como se visualiza. */
{
   glViewport(0,0,(GLint)w,(GLint)h);  
   glDrawBuffer(GL_BACK);
   glClearColor(0.f, 0.8f, 0.5f, 0.0f);  
   glClearDepth(1.0);
   /* This Will Clear The Background Color To Light Blue */
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
   glEnable(GL_TEXTURE_2D);     /* Enable Texture Mapping */
   glEnable (GL_AUTO_NORMAL);
   glEnable (GL_NORMALIZE);  
   glEnable(GL_DEPTH_TEST);     /* Enables Depth Testing */
   glDepthFunc(GL_LESS);  
   glShadeModel(GL_SMOOTH);     /* Enables Smooth Color Shading */
   glEnable(GL_BLEND);
   glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

   return 0;
}

static int InitMiniOGL(FL_OBJECT *ob, Window win,int w,int h, XEvent *xev, void *ud)
 /* Inicializa OpenGL con los parametros que diran como se visualiza. */
{
   glViewport(0,0,(GLint)w,(GLint)h);  
   glDrawBuffer(GL_BACK);
   glClearColor(0.f, 0.8f, 0.5f, 0.0f);  
   glClearDepth(1.0);
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
   glEnable(GL_TEXTURE_2D);     /* Enable Texture Mapping */
   glEnable (GL_AUTO_NORMAL);
   glEnable (GL_NORMALIZE);  
   glEnable(GL_DEPTH_TEST);     /* Enables Depth Testing */
   glDepthFunc(GL_LESS);  
   glShadeModel(GL_SMOOTH);     /* Enables Smooth Color Shading */
   glEnable(GL_BLEND);
   glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

   return 0;
}

static int image_displaysetup() {
	/* Inicializa las ventanas, la paleta de colores y memoria compartida para visualizacion*/ 
	XGCValues gc_values;
	XWindowAttributes win_attributes;
	XColor nuevocolor;
	int pixelNum, numCols;
	int allocated_colors=0, non_allocated_colors=0;

	fronteragui_win= FL_ObjWin(fd_fronteragui->ventanaA);
	XGetWindowAttributes(mydisplay, fronteragui_win, &win_attributes);
	XMapWindow(mydisplay, fronteragui_win);
	/*XSelectInput(mydisplay, fronteragui_win, ButtonPress|StructureNotifyMask);*/   
	gc_values.graphics_exposures = False;
	fronteragui_gc = XCreateGC(mydisplay, fronteragui_win, GCGraphicsExposures, &gc_values);  

	myHSV = (struct HSV*)malloc(sizeof(struct HSV));

	imagenA_buf = (char *) malloc(SIFNTSC_COLUMNS*SIFNTSC_ROWS);
	imagenA = XCreateImage(mydisplay,DefaultVisual(mydisplay,*myscreen),24, ZPixmap,0,imagenA_buf,SIFNTSC_COLUMNS/2, SIFNTSC_ROWS/2,8,0);
	imagenB_buf = (char *) malloc(SIFNTSC_COLUMNS*SIFNTSC_ROWS);
	imagenB = XCreateImage(mydisplay,DefaultVisual(mydisplay,*myscreen),24, ZPixmap,0,imagenB_buf,SIFNTSC_COLUMNS/2, SIFNTSC_ROWS/2,8,0);
	imagenFiltrada_buf = (char *) malloc(SIFNTSC_COLUMNS*SIFNTSC_ROWS*4);
	imagenFiltrada = XCreateImage(mydisplay,DefaultVisual(mydisplay,*myscreen),24, ZPixmap,0,imagenFiltrada_buf,SIFNTSC_COLUMNS, SIFNTSC_ROWS,8,0);
	imagenSuelo_buf = (char *) malloc(SIFNTSC_COLUMNS*SIFNTSC_ROWS*4);
	imagenSuelo = XCreateImage(mydisplay,DefaultVisual(mydisplay,*myscreen),24, ZPixmap,0,imagenSuelo_buf,SIFNTSC_COLUMNS, SIFNTSC_ROWS,8,0);

	return win_attributes.depth;
}

void initConfiguration () {
  fl_activate_glcanvas(fd_fronteragui->canvas);
  /* Set the OpenGL state machine to the right context for this display */
  /* reset of the depth and color buffers */
  InitOGL(fd_fronteragui->canvas, FL_ObjWin(fd_fronteragui->canvas),fd_fronteragui->canvas->w,fd_fronteragui->canvas->h,NULL,NULL);
    
  /* Virtual camera */
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity(); 

  /* perspective projection. intrinsic parameters + frustrum */
  gluPerspective(45.,(GLfloat)640/(GLfloat)480,1.0,50000.0);
  /* extrinsic parameters */
  gluLookAt(virtualcam1.position.X,virtualcam1.position.Y,virtualcam1.position.Z,virtualcam1.foa.X,virtualcam1.foa.Y,virtualcam1.foa.Z,0.,0.,1.);

	glEnable (GL_CULL_FACE); // solo veremos una cara de los planos, para el "efecto transparencia"
	glCullFace (GL_FRONT);
}

void initFronteraCanvasSettings () {
  float r,lati,longi;

	if (!flashImage) {
	  if (foa_mode) {
	    longi=2*PI*mouse_on_fronteracanvas.x/360.;
	    lati=2*PI*mouse_on_fronteracanvas.y/360.;
	    r=500;
	    virtualcam2.foa.X=r*cos(lati)*cos(longi);
	    virtualcam2.foa.Y=r*cos(lati)*sin(longi);
	    virtualcam2.foa.Z=r*sin(lati);
	  }
	  
	  if (cam_mode) {
	    longi=2*PI*mouse_on_fronteracanvas.x/360.;
	    lati=2*PI*mouse_on_fronteracanvas.y/360.;
	    
	    virtualcam2.position.X=radius*cos(lati)*cos(longi);
	    virtualcam2.position.Y=radius*cos(lati)*sin(longi);
	    virtualcam2.position.Z=radius*sin(lati);
	    
	    virtualcam2.foa.X=0;
	    virtualcam2.foa.Y=0;
	    virtualcam2.foa.Z=0;
	  }
	}

	/* minicanvas settings */
  fl_activate_glcanvas(fd_fronteragui->fronteracanvas);
  InitFronteraOGL(fd_fronteragui->fronteracanvas, FL_ObjWin(fd_fronteragui->fronteracanvas),fd_fronteragui->fronteracanvas->w,fd_fronteragui->fronteracanvas->h,NULL,NULL);
    
  /* Virtual camera */
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity(); 

  /* perspective projection. intrinsic parameters + frustrum */
  gluPerspective(45.,(GLfloat)640/(GLfloat)480,1.0,50000.0);
  /* extrinsic parameters */
  gluLookAt(virtualcam2.position.X,virtualcam2.position.Y,virtualcam2.position.Z,virtualcam2.foa.X,virtualcam2.foa.Y,virtualcam2.foa.Z,0.,0.,1.);
}

void initMiniCanvasSettings () {
  float r,lati,longi;

	if (!flashImage) {
	  if (foa_mode) {
	    longi=2*PI*mouse_on_minicanvas.x/360.;
	    lati=2*PI*mouse_on_minicanvas.y/360.;
	    r=500;
	    virtualcam0.foa.X=r*cos(lati)*cos(longi);
	    virtualcam0.foa.Y=r*cos(lati)*sin(longi);
	    virtualcam0.foa.Z=r*sin(lati);
	  }
	  
	  if (cam_mode) {
	    longi=2*PI*mouse_on_minicanvas.x/360.;
	    lati=2*PI*mouse_on_minicanvas.y/360.;
	    
	    virtualcam0.position.X=radius*cos(lati)*cos(longi);
	    virtualcam0.position.Y=radius*cos(lati)*sin(longi);
	    virtualcam0.position.Z=radius*sin(lati);
	    
	    virtualcam0.foa.X=0;
	    virtualcam0.foa.Y=0;
	    virtualcam0.foa.Z=0;
	  }
	}

	/* minicanvas settings */
  fl_activate_glcanvas(fd_fronteragui->minicanvas);
  InitMiniOGL(fd_fronteragui->minicanvas, FL_ObjWin(fd_fronteragui->minicanvas),fd_fronteragui->minicanvas->w,fd_fronteragui->minicanvas->h,NULL,NULL);
    
  /* Virtual camera */
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity(); 

  /* perspective projection. intrinsic parameters + frustrum */
  gluPerspective(45.,(GLfloat)640/(GLfloat)480,1.0,50000.0);
  /* extrinsic parameters */
  gluLookAt(virtualcam0.position.X,virtualcam0.position.Y,virtualcam0.position.Z,virtualcam0.foa.X,virtualcam0.foa.Y,virtualcam0.foa.Z,0.,0.,1.);
}

void drawFilteredImage () {
	int i,c,row,j,k;
  double H, S, V;
  float myR, myG, myB;

  /* FilteredImage display */
	for (i=0, k=0/*(SIFNTSC_ROWS*SIFNTSC_COLUMNS)-1*/; i<SIFNTSC_ROWS*SIFNTSC_COLUMNS; i++, k++) {
		c=i%(SIFNTSC_COLUMNS);
		row=i/(SIFNTSC_COLUMNS);
		j=row*SIFNTSC_COLUMNS+c;

    if (((display_state&DISPLAY_COLORIMAGEA)!=0) && (mycolorA!=NULL)) {
			imagenFiltrada_buf[k*4] = (*mycolorA)[j*3];
			imagenFiltrada_buf[k*4+1] = (*mycolorA)[j*3+1];
			imagenFiltrada_buf[k*4+2] = (*mycolorA)[j*3+2];
		} else if (((display_state&DISPLAY_COLORIMAGEB)!=0) && (mycolorB!=NULL)) {
			imagenFiltrada_buf[k*4] = (*mycolorB)[j*3];
			imagenFiltrada_buf[k*4+1] = (*mycolorB)[j*3+1];
			imagenFiltrada_buf[k*4+2] = (*mycolorB)[j*3+2];
    } else { // no recibimos se�al
			imagenFiltrada_buf[k*4]=0;
			imagenFiltrada_buf[k*4+1]=0;
			imagenFiltrada_buf[k*4+2]=0;
    }
		imagenFiltrada_buf[k*4+3]=0; // dummy

		myR = (float)(unsigned int)(unsigned char)imagenFiltrada_buf[k*4+2];
		myG = (float)(unsigned int)(unsigned char)imagenFiltrada_buf[k*4+1];
		myB = (float)(unsigned int)(unsigned char)imagenFiltrada_buf[k*4+0];

		myHSV = (struct HSV*) RGB2HSV_getHSV ((int)myR,(int)myG,(int)myB); // pasamos de RGB -> HSV

		if (!((myHSV->H*DEGTORAD > 1.7017) && (myHSV->H*DEGTORAD < 2.5394) &&
			(myHSV->S > 0.31) && (myHSV->S < 0.7) &&
			(myHSV->V > 77.6) && (myHSV->V < 207.2))) { // si no pasamos el filtro de campo f�tbol
			imagenFiltrada_buf[k*4]=0;
			imagenFiltrada_buf[k*4+1]=0;
			imagenFiltrada_buf[k*4+2]=0;
		}
	}
}

void drawImageA () {
	int i,c,row,j,k;

  /* color imageA display */
  if ((display_state&DISPLAY_COLORIMAGEA)!=0){
    for(i=0, k=0/*(SIFNTSC_ROWS*SIFNTSC_COLUMNS/4)-1*/; i<SIFNTSC_ROWS*SIFNTSC_COLUMNS/4; i++, k++){
      c=i%(SIFNTSC_COLUMNS/2);
      row=i/(SIFNTSC_COLUMNS/2);
      j=2*row*SIFNTSC_COLUMNS+2*c;
      if (mycolorA!=NULL){
        imagenA_buf[k*4]=(*mycolorA)[j*3];
        imagenA_buf[k*4+1]=(*mycolorA)[j*3+1];
        imagenA_buf[k*4+2]=(*mycolorA)[j*3+2];
      }
      else{
        imagenA_buf[k*4]=0;
        imagenA_buf[k*4+1]=0;
        imagenA_buf[k*4+2]=0;
      }
      imagenA_buf[k*4+3]=UCHAR_MAX;
    }
	}
}

void drawImageB () {
	int i,c,row,j,k;

  /* color imageB display */
  if ((display_state&DISPLAY_COLORIMAGEB)!=0){
    for(i=0, k=(SIFNTSC_ROWS*SIFNTSC_COLUMNS/4)-1; i<SIFNTSC_ROWS*SIFNTSC_COLUMNS/4; i++, k--){
        c=i%(SIFNTSC_COLUMNS/2);
        row=i/(SIFNTSC_COLUMNS/2);
        j=2*row*SIFNTSC_COLUMNS+2*c;
        if (mycolorB!=NULL){
          imagenB_buf[k*4]=(*mycolorB)[j*3]; /* Blue Byte */
          imagenB_buf[k*4+1]=(*mycolorB)[j*3+1]; /* Green Byte */
          imagenB_buf[k*4+2]=(*mycolorB)[j*3+2]; /* Red Byte */
        }
        else{
          imagenB_buf[k*4]=0; /* Blue Byte */
          imagenB_buf[k*4+1]=0; /* Green Byte */
          imagenB_buf[k*4+2]=0; /* Red Byte */
        }
        imagenB_buf[k*4+3]=UCHAR_MAX; /* alpha byte */
    }
  }
}

void drawFronteraImage () {
  int i, j, c, row, pos, columnPoint, rowPoint, posPoint;

  double H, S, V;
  float myR, myG, myB;

	int esFrontera;
	int v1, v2, v3, v4, v5, v6, v7, v8;

	/* Ground image */
	for (j=0;j<ANCHO_IMAGEN;j++) { // recorrido en columnas
		i = LARGO_IMAGEN-1;
		esFrontera = FALSE;
		while ((i>=0) && (esFrontera == FALSE)) { // recorrido en filas
			if ((((display_state&DISPLAY_COLORIMAGEA)!=0) && (mycolorA!=NULL)) ||
					(((display_state&DISPLAY_COLORIMAGEB)!=0) && (mycolorB!=NULL))) {
				if (((display_state&DISPLAY_COLORIMAGEA)!=0) && (mycolorA!=NULL)) {
					myActualCamera = mycolorA;
				} else if (((display_state&DISPLAY_COLORIMAGEB)!=0) && (mycolorB!=NULL)) {
					myActualCamera = mycolorB;
				}

			  pos = i*ANCHO_IMAGEN+j; // posicion actual

				myR = (float)(unsigned int)(unsigned char)(*myActualCamera)[pos*3+2];
				myG = (float)(unsigned int)(unsigned char)(*myActualCamera)[pos*3+1];
				myB = (float)(unsigned int)(unsigned char)(*myActualCamera)[pos*3+0];

				myHSV = (struct HSV*) RGB2HSV_getHSV ((int)myR,(int)myG,(int)myB); // pasamos de RGB -> HSV

				if (!((myHSV->H*DEGTORAD > 1.7017) && (myHSV->H*DEGTORAD < 2.5394) &&
					(myHSV->S > 0.31) && (myHSV->S < 0.7) &&
					(myHSV->V > 77.6) && (myHSV->V < 207.2))) { // si soy negro

					esFrontera = TRUE; // lo damos por frontera en un principio, luego veremos

					// Calculo los dem�s vecinos, para ver de qu� color son...
					c = j - 1;
					row = i;
					v1 = row*ANCHO_IMAGEN+c;

					if (!((c >= 0) && (c < SIFNTSC_COLUMNS) && 
						 (row >= 0) && (row < SIFNTSC_ROWS))) { // si no es v�lido ponemos sus valores a 0
						myR = 0.;
						myG = 0.;
						myB = 0.;
					} else {
						myR = (float)(unsigned int)(unsigned char)(*myActualCamera)[v1*3+2];
						myG = (float)(unsigned int)(unsigned char)(*myActualCamera)[v1*3+1];
						myB = (float)(unsigned int)(unsigned char)(*myActualCamera)[v1*3+0];
					}

					myHSV = (struct HSV*) RGB2HSV_getHSV ((int)myR,(int)myG,(int)myB); // pasamos de RGB -> HSV

					if (!((myHSV->H*DEGTORAD > 1.7017) && (myHSV->H*DEGTORAD < 2.5394) &&
						(myHSV->S > 0.31) && (myHSV->S < 0.7) &&
						(myHSV->V > 77.6) && (myHSV->V < 207.2))) { // si no soy color campo, sigo comprobando

						c = j - 1;
						row = i - 1;
						v2 = row*ANCHO_IMAGEN+c;

						if (!((c >= 0) && (c < SIFNTSC_COLUMNS) && 
							 (row >= 0) && (row < SIFNTSC_ROWS))) { // si no es v�lido ponemos sus valores a 0
							myR = 0.;
							myG = 0.;
							myB = 0.;
						} else {
							myR = (float)(unsigned int)(unsigned char)(*myActualCamera)[v2*3+2];
							myG = (float)(unsigned int)(unsigned char)(*myActualCamera)[v2*3+1];
							myB = (float)(unsigned int)(unsigned char)(*myActualCamera)[v2*3+0];
						}

						myHSV = (struct HSV*) RGB2HSV_getHSV ((int)myR,(int)myG,(int)myB); // pasamos de RGB -> HSV

						if (!((myHSV->H*DEGTORAD > 1.7017) && (myHSV->H*DEGTORAD < 2.5394) &&
							(myHSV->S > 0.31) && (myHSV->S < 0.7) &&
							(myHSV->V > 77.6) && (myHSV->V < 207.2))) { // si no soy color campo, sigo comprobando

							c = j;
							row = i - 1;
							v3 = row*ANCHO_IMAGEN+c;

							if (!((c >= 0) && (c < SIFNTSC_COLUMNS) && 
								 (row >= 0) && (row < SIFNTSC_ROWS))) { // si no es v�lido ponemos sus valores a 0
								myR = 0.;
								myG = 0.;
								myB = 0.;
							} else {
								myR = (float)(unsigned int)(unsigned char)(*myActualCamera)[v3*3+2];
								myG = (float)(unsigned int)(unsigned char)(*myActualCamera)[v3*3+1];
								myB = (float)(unsigned int)(unsigned char)(*myActualCamera)[v3*3+0];
							}

							myHSV = (struct HSV*) RGB2HSV_getHSV ((int)myR,(int)myG,(int)myB); // pasamos de RGB -> HSV

							if (!((myHSV->H*DEGTORAD > 1.7017) && (myHSV->H*DEGTORAD < 2.5394) &&
								(myHSV->S > 0.31) && (myHSV->S < 0.7) &&
								(myHSV->V > 77.6) && (myHSV->V < 207.2))) { // si no soy color campo, sigo comprobando

								c = j + 1;
								row = i - 1;
								v4 = row*ANCHO_IMAGEN+c;

								if (!((c >= 0) && (c < SIFNTSC_COLUMNS) && 
									 (row >= 0) && (row < SIFNTSC_ROWS))) { // si no es v�lido ponemos sus valores a 0
									myR = 0.;
									myG = 0.;
									myB = 0.;
								} else {
									myR = (float)(unsigned int)(unsigned char)(*myActualCamera)[v4*3+2];
									myG = (float)(unsigned int)(unsigned char)(*myActualCamera)[v4*3+1];
									myB = (float)(unsigned int)(unsigned char)(*myActualCamera)[v4*3+0];
								}

								myHSV = (struct HSV*) RGB2HSV_getHSV ((int)myR,(int)myG,(int)myB); // pasamos de RGB -> HSV

								if (!((myHSV->H*DEGTORAD > 1.7017) && (myHSV->H*DEGTORAD < 2.5394) &&
									(myHSV->S > 0.31) && (myHSV->S < 0.7) &&
									(myHSV->V > 77.6) && (myHSV->V < 207.2))) { // si no soy color campo, sigo comprobando

									c = j + 1;
									row = i;
									v5 = row*ANCHO_IMAGEN+c;

									if (!((c >= 0) && (c < SIFNTSC_COLUMNS) && 
										 (row >= 0) && (row < SIFNTSC_ROWS))) { // si no es v�lido ponemos sus valores a 0
										myR = 0.;
										myG = 0.;
										myB = 0.;
									} else {
										myR = (float)(unsigned int)(unsigned char)(*myActualCamera)[v5*3+2];
										myG = (float)(unsigned int)(unsigned char)(*myActualCamera)[v5*3+1];
										myB = (float)(unsigned int)(unsigned char)(*myActualCamera)[v5*3+0];
									}

									myHSV = (struct HSV*) RGB2HSV_getHSV ((int)myR,(int)myG,(int)myB); // pasamos de RGB -> HSV

									if (!((myHSV->H*DEGTORAD > 1.7017) && (myHSV->H*DEGTORAD < 2.5394) &&
										(myHSV->S > 0.31) && (myHSV->S < 0.7) &&
										(myHSV->V > 77.6) && (myHSV->V < 207.2))) { // si no soy color campo, sigo comprobando

										c = j + 1;
										row = i + 1;
										v6 = row*ANCHO_IMAGEN+c;

										if (!((c >= 0) && (c < SIFNTSC_COLUMNS) && 
											 (row >= 0) && (row < SIFNTSC_ROWS))) { // si no es v�lido ponemos sus valores a 0
											myR = 0.;
											myG = 0.;
											myB = 0.;
										} else {
											myR = (float)(unsigned int)(unsigned char)(*myActualCamera)[v6*3+2];
											myG = (float)(unsigned int)(unsigned char)(*myActualCamera)[v6*3+1];
											myB = (float)(unsigned int)(unsigned char)(*myActualCamera)[v6*3+0];
										}

										myHSV = (struct HSV*) RGB2HSV_getHSV ((int)myR,(int)myG,(int)myB); // pasamos de RGB -> HSV

										if (!((myHSV->H*DEGTORAD > 1.7017) && (myHSV->H*DEGTORAD < 2.5394) &&
											(myHSV->S > 0.31) && (myHSV->S < 0.7) &&
											(myHSV->V > 77.6) && (myHSV->V < 207.2))) { // si no soy color campo, sigo comprobando

											c = j;
											row = i + 1;
											v7 = row*ANCHO_IMAGEN+c;

											if (!((c >= 0) && (c < SIFNTSC_COLUMNS) && 
												 (row >= 0) && (row < SIFNTSC_ROWS))) { // si no es v�lido ponemos sus valores a 0
												myR = 0.;
												myG = 0.;
												myB = 0.;
											} else {
												myR = (float)(unsigned int)(unsigned char)(*myActualCamera)[v7*3+2];
												myG = (float)(unsigned int)(unsigned char)(*myActualCamera)[v7*3+1];
												myB = (float)(unsigned int)(unsigned char)(*myActualCamera)[v7*3+0];
											}

											myHSV = (struct HSV*) RGB2HSV_getHSV ((int)myR,(int)myG,(int)myB); // pasamos de RGB -> HSV

											if (!((myHSV->H*DEGTORAD > 1.7017) && (myHSV->H*DEGTORAD < 2.5394) &&
												(myHSV->S > 0.31) && (myHSV->S < 0.7) &&
												(myHSV->V > 77.6) && (myHSV->V < 207.2))) { // si no soy color campo, sigo comprobando

												c = j - 1;
												row = i + 1;
												v8 = row*ANCHO_IMAGEN+c;

												if (!((c >= 0) && (c < SIFNTSC_COLUMNS) && 
													 (row >= 0) && (row < SIFNTSC_ROWS))) { // si no es v�lido ponemos sus valores a 0
													myR = 0.;
													myG = 0.;
													myB = 0.;
												} else {
													myR = (float)(unsigned int)(unsigned char)(*myActualCamera)[v8*3+2];
													myG = (float)(unsigned int)(unsigned char)(*myActualCamera)[v8*3+1];
													myB = (float)(unsigned int)(unsigned char)(*myActualCamera)[v8*3+0];
												}

												myHSV = (struct HSV*) RGB2HSV_getHSV ((int)myR,(int)myG,(int)myB); // pasamos de RGB -> HSV

												if (!((myHSV->H*DEGTORAD > 1.7017) && (myHSV->H*DEGTORAD < 2.5394) &&
													(myHSV->S > 0.31) && (myHSV->S < 0.7) &&
													(myHSV->V > 77.6) && (myHSV->V < 207.2))) { // si no soy color campo, se acab�, no es pto. frontera
													esFrontera = FALSE;
												} // fin if vecinito 8
											} // fin if vecinito 7
										} // fin if vecinito 6
									} // fin if vecinito 5
								} // fin if vecinito 4
							} // fin if vecinito 3
						} // fin if vecinito 2
					} // fin if vecinito 1

					if (esFrontera == TRUE) { // si NO SOY COLOR CAMPO y alguno de los vecinitos ES color campo...
						myActualPointImage.x = i;
						myActualPointImage.y = j;
							
						pointProjection (myActualPointImage, myActualCamera);
					}
				} // fin if (soy negro)
			} // fin if myColorX != NULL
			i --;
		} // fin while (filas)
 	} // fin for (columnas)
}

void drawGroundImage () {
  int i, k, c, row, pos, columnPoint, rowPoint, posPoint;

  double H, S, V;
  float myR, myG, myB;

	int esFrontera;
	int v1, v2, v3, v4, v5, v6, v7, v8;

	/* Ground image */
	for (i=0, k=0; i<SIFNTSC_ROWS*SIFNTSC_COLUMNS; i++, k++) {
		if ((((display_state&DISPLAY_COLORIMAGEA)!=0) && (mycolorA!=NULL)) ||
				(((display_state&DISPLAY_COLORIMAGEB)!=0) && (mycolorB!=NULL))) {
			if (((display_state&DISPLAY_COLORIMAGEA)!=0) && (mycolorA!=NULL)) {
				myActualCamera = mycolorA;
			} else if (((display_state&DISPLAY_COLORIMAGEB)!=0) && (mycolorB!=NULL)) {
				myActualCamera = mycolorB;
			}

			columnPoint = i%(SIFNTSC_COLUMNS);
			rowPoint = i/(SIFNTSC_COLUMNS);
			pos = rowPoint*SIFNTSC_COLUMNS+columnPoint;

			myR = (float)(unsigned int)(unsigned char)(*myActualCamera)[pos*3+2];
			myG = (float)(unsigned int)(unsigned char)(*myActualCamera)[pos*3+1];
			myB = (float)(unsigned int)(unsigned char)(*myActualCamera)[pos*3+0];

			myHSV = (struct HSV*) RGB2HSV_getHSV ((int)myR,(int)myG,(int)myB); // pasamos de RGB -> HSV

			if (!((myHSV->H*DEGTORAD > 1.7017) && (myHSV->H*DEGTORAD < 2.5394) &&
				(myHSV->S > 0.31) && (myHSV->S < 0.7) &&
				(myHSV->V > 77.6) && (myHSV->V < 207.2))) { // si soy negro

				esFrontera = TRUE; // lo damos por frontera en un principio, luego veremos

				// Calculo los dem�s vecinos, para ver de qu� color son...
				c = (i-(SIFNTSC_COLUMNS))%(SIFNTSC_COLUMNS);
				row=(i-(SIFNTSC_COLUMNS))/(SIFNTSC_COLUMNS);
				v1=row*SIFNTSC_COLUMNS+c; // vecinito 1

				if (!((c >= 0) && (c < SIFNTSC_COLUMNS) && 
					 (row >= 0) && (row < SIFNTSC_ROWS))) { // si no es v�lido ponemos sus valores a 0
					myR = 0.;
					myG = 0.;
					myB = 0.;
				} else {
					myR = (float)(unsigned int)(unsigned char)(*myActualCamera)[v1*3+2];
					myG = (float)(unsigned int)(unsigned char)(*myActualCamera)[v1*3+1];
					myB = (float)(unsigned int)(unsigned char)(*myActualCamera)[v1*3+0];
				}

				myHSV = (struct HSV*) RGB2HSV_getHSV ((int)myR,(int)myG,(int)myB); // pasamos de RGB -> HSV

				if (!((myHSV->H*DEGTORAD > 1.7017) && (myHSV->H*DEGTORAD < 2.5394) &&
					(myHSV->S > 0.31) && (myHSV->S < 0.7) &&
					(myHSV->V > 77.6) && (myHSV->V < 207.2))) { // si no soy color campo, sigo comprobando

					c = (i+(SIFNTSC_COLUMNS))%(SIFNTSC_COLUMNS);
					row=(i+(SIFNTSC_COLUMNS))/(SIFNTSC_COLUMNS);
					v2=row*SIFNTSC_COLUMNS+c; // vecinito 2

					if (!((c >= 0) && (c < SIFNTSC_COLUMNS) && 
						 (row >= 0) && (row < SIFNTSC_ROWS))) { // si no es v�lido ponemos sus valores a 0
						myR = 0.;
						myG = 0.;
						myB = 0.;
					} else {
						myR = (float)(unsigned int)(unsigned char)(*myActualCamera)[v2*3+2];
						myG = (float)(unsigned int)(unsigned char)(*myActualCamera)[v2*3+1];
						myB = (float)(unsigned int)(unsigned char)(*myActualCamera)[v2*3+0];
					}

					myHSV = (struct HSV*) RGB2HSV_getHSV ((int)myR,(int)myG,(int)myB); // pasamos de RGB -> HSV

					if (!((myHSV->H*DEGTORAD > 1.7017) && (myHSV->H*DEGTORAD < 2.5394) &&
						(myHSV->S > 0.31) && (myHSV->S < 0.7) &&
						(myHSV->V > 77.6) && (myHSV->V < 207.2))) { // si no soy color campo, sigo comprobando

						c = (i-1)%(SIFNTSC_COLUMNS);
						row=(i-1)/(SIFNTSC_COLUMNS);
						v3=row*SIFNTSC_COLUMNS+c; // vecinito 3

						if (!((c >= 0) && (c < SIFNTSC_COLUMNS) && 
							 (row >= 0) && (row < SIFNTSC_ROWS))) { // si no es v�lido ponemos sus valores a 0
							myR = 0.;
							myG = 0.;
							myB = 0.;
						} else {
							myR = (float)(unsigned int)(unsigned char)(*myActualCamera)[v3*3+2];
							myG = (float)(unsigned int)(unsigned char)(*myActualCamera)[v3*3+1];
							myB = (float)(unsigned int)(unsigned char)(*myActualCamera)[v3*3+0];
						}

						myHSV = (struct HSV*) RGB2HSV_getHSV ((int)myR,(int)myG,(int)myB); // pasamos de RGB -> HSV

						if (!((myHSV->H*DEGTORAD > 1.7017) && (myHSV->H*DEGTORAD < 2.5394) &&
							(myHSV->S > 0.31) && (myHSV->S < 0.7) &&
							(myHSV->V > 77.6) && (myHSV->V < 207.2))) { // si no soy color campo, sigo comprobando

							c = ((i-1)-(SIFNTSC_COLUMNS))%(SIFNTSC_COLUMNS);
							row=((i-1)-(SIFNTSC_COLUMNS))/(SIFNTSC_COLUMNS);
							v4=row*SIFNTSC_COLUMNS+c; // vecinito 4

							if (!((c >= 0) && (c < SIFNTSC_COLUMNS) && 
								 (row >= 0) && (row < SIFNTSC_ROWS))) { // si no es v�lido ponemos sus valores a 0
								myR = 0.;
								myG = 0.;
								myB = 0.;
							} else {
								myR = (float)(unsigned int)(unsigned char)(*myActualCamera)[v4*3+2];
								myG = (float)(unsigned int)(unsigned char)(*myActualCamera)[v4*3+1];
								myB = (float)(unsigned int)(unsigned char)(*myActualCamera)[v4*3+0];
							}

							myHSV = (struct HSV*) RGB2HSV_getHSV ((int)myR,(int)myG,(int)myB); // pasamos de RGB -> HSV

							if (!((myHSV->H*DEGTORAD > 1.7017) && (myHSV->H*DEGTORAD < 2.5394) &&
								(myHSV->S > 0.31) && (myHSV->S < 0.7) &&
								(myHSV->V > 77.6) && (myHSV->V < 207.2))) { // si no soy color campo, sigo comprobando

								c = ((i-1)+(SIFNTSC_COLUMNS))%(SIFNTSC_COLUMNS);
								row=((i-1)+(SIFNTSC_COLUMNS))/(SIFNTSC_COLUMNS);
								v5=row*SIFNTSC_COLUMNS+c; // vecinito 5

								if (!((c >= 0) && (c < SIFNTSC_COLUMNS) && 
									 (row >= 0) && (row < SIFNTSC_ROWS))) { // si no es v�lido ponemos sus valores a 0
									myR = 0.;
									myG = 0.;
									myB = 0.;
								} else {
									myR = (float)(unsigned int)(unsigned char)(*myActualCamera)[v5*3+2];
									myG = (float)(unsigned int)(unsigned char)(*myActualCamera)[v5*3+1];
									myB = (float)(unsigned int)(unsigned char)(*myActualCamera)[v5*3+0];
								}

								myHSV = (struct HSV*) RGB2HSV_getHSV ((int)myR,(int)myG,(int)myB); // pasamos de RGB -> HSV

								if (!((myHSV->H*DEGTORAD > 1.7017) && (myHSV->H*DEGTORAD < 2.5394) &&
									(myHSV->S > 0.31) && (myHSV->S < 0.7) &&
									(myHSV->V > 77.6) && (myHSV->V < 207.2))) { // si no soy color campo, sigo comprobando

									c = (i+1)%(SIFNTSC_COLUMNS);
									row=(i+1)/(SIFNTSC_COLUMNS);
									v6=row*SIFNTSC_COLUMNS+c; // vecinito 6

									if (!((c >= 0) && (c < SIFNTSC_COLUMNS) && 
										 (row >= 0) && (row < SIFNTSC_ROWS))) { // si no es v�lido ponemos sus valores a 0
										myR = 0.;
										myG = 0.;
										myB = 0.;
									} else {
										myR = (float)(unsigned int)(unsigned char)(*myActualCamera)[v6*3+2];
										myG = (float)(unsigned int)(unsigned char)(*myActualCamera)[v6*3+1];
										myB = (float)(unsigned int)(unsigned char)(*myActualCamera)[v6*3+0];
									}

									myHSV = (struct HSV*) RGB2HSV_getHSV ((int)myR,(int)myG,(int)myB); // pasamos de RGB -> HSV

									if (!((myHSV->H*DEGTORAD > 1.7017) && (myHSV->H*DEGTORAD < 2.5394) &&
										(myHSV->S > 0.31) && (myHSV->S < 0.7) &&
										(myHSV->V > 77.6) && (myHSV->V < 207.2))) { // si no soy color campo, sigo comprobando

										c = ((i+1)-(SIFNTSC_COLUMNS))%(SIFNTSC_COLUMNS);
										row=((i+1)-(SIFNTSC_COLUMNS))/(SIFNTSC_COLUMNS);
										v7=row*SIFNTSC_COLUMNS+c; // vecinito 7

										if (!((c >= 0) && (c < SIFNTSC_COLUMNS) && 
											 (row >= 0) && (row < SIFNTSC_ROWS))) { // si no es v�lido ponemos sus valores a 0
											myR = 0.;
											myG = 0.;
											myB = 0.;
										} else {
											myR = (float)(unsigned int)(unsigned char)(*myActualCamera)[v7*3+2];
											myG = (float)(unsigned int)(unsigned char)(*myActualCamera)[v7*3+1];
											myB = (float)(unsigned int)(unsigned char)(*myActualCamera)[v7*3+0];
										}

										myHSV = (struct HSV*) RGB2HSV_getHSV ((int)myR,(int)myG,(int)myB); // pasamos de RGB -> HSV

										if (!((myHSV->H*DEGTORAD > 1.7017) && (myHSV->H*DEGTORAD < 2.5394) &&
											(myHSV->S > 0.31) && (myHSV->S < 0.7) &&
											(myHSV->V > 77.6) && (myHSV->V < 207.2))) { // si no soy color campo, sigo comprobando

											c = ((i+1)+(SIFNTSC_COLUMNS))%(SIFNTSC_COLUMNS);
											row=((i+1)+(SIFNTSC_COLUMNS))/(SIFNTSC_COLUMNS);
											v8=row*SIFNTSC_COLUMNS+c; // vecinito 8

											if (!((c >= 0) && (c < SIFNTSC_COLUMNS) && 
												 (row >= 0) && (row < SIFNTSC_ROWS))) { // si no es v�lido ponemos sus valores a 0
												myR = 0.;
												myG = 0.;
												myB = 0.;
											} else {
												myR = (float)(unsigned int)(unsigned char)(*myActualCamera)[v8*3+2];
												myG = (float)(unsigned int)(unsigned char)(*myActualCamera)[v8*3+1];
												myB = (float)(unsigned int)(unsigned char)(*myActualCamera)[v8*3+0];
											}

											myHSV = (struct HSV*) RGB2HSV_getHSV ((int)myR,(int)myG,(int)myB); // pasamos de RGB -> HSV

											if (!((myHSV->H*DEGTORAD > 1.7017) && (myHSV->H*DEGTORAD < 2.5394) &&
												(myHSV->S > 0.31) && (myHSV->S < 0.7) &&
												(myHSV->V > 77.6) && (myHSV->V < 207.2))) { // si no soy color campo, se acab�, no es pto. frontera
												esFrontera = FALSE;
											} // fin if vecinito 8
										} // fin if vecinito 7
									} // fin if vecinito 6
								} // fin if vecinito 5
							} // fin if vecinito 4
						} // fin if vecinito 3
					} // fin if vecinito 2
				} // fin if vecinito 1

				if (esFrontera == TRUE) { // si NO SOY COLOR CAMPO y alguno de los vecinitos ES color campo...
					imagenSuelo_buf[k*4]=255;
					imagenSuelo_buf[k*4+1]=255;
					imagenSuelo_buf[k*4+2]=255;

					myActualPointImage.x = rowPoint;
					myActualPointImage.y = columnPoint;
						
					pointProjection (myActualPointImage, myActualCamera);
				} else { // si todos los vecinitos son color NO campo
					imagenSuelo_buf[k*4]=0;
					imagenSuelo_buf[k*4+1]=0;
					imagenSuelo_buf[k*4+2]=0;
				}
			} else { // fin if soy negro (else no soy negro)
				imagenSuelo_buf[k*4]=0;
				imagenSuelo_buf[k*4+1]=0;
				imagenSuelo_buf[k*4+2]=0;
			}

			imagenSuelo_buf[k*4+3]=0;
		} // fin if myColorX != NULL
	} // fin FOR
}

void displayFilteredImage () {
  if (((display_state&DISPLAY_COLORIMAGEA) != 0) ||
			((display_state&DISPLAY_COLORIMAGEB) != 0)) {
		XPutImage(mydisplay,fronteragui_win,fronteragui_gc,imagenFiltrada,0,0,fd_fronteragui->filteredImage->x+1, fd_fronteragui->filteredImage->y+1, SIFNTSC_COLUMNS, SIFNTSC_ROWS);
		XPutImage(mydisplay,fronteragui_win,fronteragui_gc,imagenSuelo,0,0,fd_fronteragui->groundImage->x+1, fd_fronteragui->filteredImage->y+1, SIFNTSC_COLUMNS, SIFNTSC_ROWS);
	}
}

void displayImageA () {
  if ((display_state&DISPLAY_COLORIMAGEA)!=0) { /* Draw *myscreen onto display */
		XPutImage(mydisplay,fronteragui_win,fronteragui_gc,imagenA,0,0,fd_fronteragui->ventanaA->x+1, fd_fronteragui->ventanaA->y+1, SIFNTSC_COLUMNS/2, SIFNTSC_ROWS/2);
	}
}
void displayImageB () {
  if ((display_state&DISPLAY_COLORIMAGEB)!=0)
    {    /* Draw *myscreen onto display */
      XPutImage(mydisplay,fronteragui_win,fronteragui_gc,imagenB,0,0,fd_fronteragui->ventanaB->x+1, fd_fronteragui->ventanaB->y+1, SIFNTSC_COLUMNS/2, SIFNTSC_ROWS/2);
    }
}
/*
void initGroundPoints () {
	int i;

	for (i = 0; i < SIFNTSC_COLUMNS*SIFNTSC_ROWS; i ++) {
		groundPoints[i].X = 0.;
		groundPoints[i].X = 0.;
		groundPoints[i].X = 0.;
	}
	actualGroundPoint = 0; // inicializamos contador
}

void initFronteraPoints () {
	int i;

	for (i = 0; i < SIFNTSC_COLUMNS*SIFNTSC_ROWS; i ++) {
		fronteraPoints[i].X = 0.;
		fronteraPoints[i].X = 0.;
		fronteraPoints[i].X = 0.;
	}
	actualFronteraPoint = 0; // inicializamos contador
}
*/
void drawGroundPoints () {
	int i;

	glColor3f(0.0f, 0.0f, 1.0f);
	glBegin (GL_POINTS);
		for (i = 0; i < SIFNTSC_COLUMNS*SIFNTSC_ROWS; i ++) {
			glVertex3f (groundPoints [i].X, groundPoints [i].Y, groundPoints [i].Z);
		}
	glEnd ();
}

void drawFronteraPoints () {
	int i;

	glColor3f(0.0f, 0.0f, 1.0f);
	glBegin (GL_POINTS);
		for (i = 0; i < SIFNTSC_COLUMNS*SIFNTSC_ROWS; i ++) {
			glVertex3f (fronteraPoints [i].X, fronteraPoints [i].Y, fronteraPoints [i].Z);
		}
	glEnd ();
}

void frontera_guidisplay() {	
	if (groundView == TRUE) { // modo proyecci�n de puntos en el suelo
		initMiniCanvasSettings ();

		drawRoboticLabGround ();
		drawGroundImage (); // aqu� tambi�n se ir�n generando los puntos de intersecci�n con el plano suelo

		drawGroundPoints ();

		glXSwapBuffers(fl_display, fl_get_canvas_id(fd_fronteragui->minicanvas));
	} else if (fronteraView == TRUE) { // modo puntos frontera que me rodean a mi alrededor...
		initFronteraCanvasSettings ();

		drawRoboticLabGround ();
		drawFronteraImage ();

		drawFronteraPoints ();

		glXSwapBuffers(fl_display, fl_get_canvas_id(fd_fronteragui->fronteracanvas));
	} else { // vista normal de openGL
		initVirtualCam ();

	  initConfiguration ();

		drawRoboticLabPlanes ();
		drawRoboticLabLines ();

		drawAxes ();
		drawCube();

		setRobotCamera ();

		glLoadIdentity ();

		drawFilteredImage ();

		drawGroundImage ();

		drawImageA ();
		// drawImageB ();

		displayFilteredImage ();

		displayImageA ();
		// displayImageB ();

	  /** intercambio de buffers: ha de hacerse lo �ltimo para tomar siempre todos los objetos de OpenGL **/
	  glXSwapBuffers(fl_display, fl_get_canvas_id(fd_fronteragui->canvas));
	}
}

void frontera_guibuttons(void *obj1) {
	 float lati, longi, r;

   FL_OBJECT *obj;
   obj=(FL_OBJECT *)obj1;

   if (obj == fd_fronteragui->colorA){
      if (fl_get_button(fd_fronteragui->colorA)==PUSHED){
         mycolorA=myimport ("colorA", "colorA");
         colorAresume=(resumeFn)myimport("colorA", "resume");
         colorAsuspend=(suspendFn)myimport("colorA", "suspend");
         if (colorAresume!=NULL){
            display_state = display_state | DISPLAY_COLORIMAGEA;
            colorAresume(frontera_id, NULL, NULL);
         }
         else{
            display_state = display_state & ~DISPLAY_COLORIMAGEA;
            fl_set_button(obj, RELEASED);
         }
      }
      else {
         display_state = display_state & ~DISPLAY_COLORIMAGEA;
         if (colorAsuspend!=NULL)
            colorAsuspend();
      }
   }
   else if (obj == fd_fronteragui->colorB){
      if (fl_get_button(fd_fronteragui->colorB)==PUSHED){
         mycolorB=myimport ("colorB", "colorB");
         colorBresume=(resumeFn)myimport("colorB", "resume");
         colorBsuspend=(suspendFn)myimport("colorB", "suspend");
         if (colorBresume!=NULL){
            display_state = display_state | DISPLAY_COLORIMAGEB;
            colorBresume(frontera_id, NULL, NULL);
         }
         else{
            display_state = display_state & ~DISPLAY_COLORIMAGEB;
            fl_set_button(obj, RELEASED);
         }
      }
      else {
         display_state = display_state & ~DISPLAY_COLORIMAGEB;
         if (colorBsuspend!=NULL)
            colorBsuspend();
      }
   }

   else if (obj == fd_fronteragui->canvasButton) {
      if (fl_get_button(fd_fronteragui->canvasButton)==PUSHED) {
				groundView = TRUE;
				fronteraView = FALSE;
        fl_set_button(fd_fronteragui->fronteraButton, RELEASED);
      } else
				groundView = FALSE;
    }

   else if (obj == fd_fronteragui->fronteraButton) {
      if (fl_get_button(fd_fronteragui->fronteraButton)==PUSHED) {
				fronteraView = TRUE;
				groundView = FALSE;
        fl_set_button(fd_fronteragui->canvasButton, RELEASED);
      } else
				fronteraView = FALSE;
    }
}

void frontera_guisuspend_aux(void)
{
  /* to make a safety stop when the robot is being teleoperated from GUI */
  mydelete_buttonscallback(frontera_guibuttons);
  mydelete_displaycallback(frontera_guidisplay);
  fl_hide_form(fd_fronteragui->fronteragui);
}

void frontera_guisuspend(){
   static callback fn=NULL;
   if (fn==NULL){
      if ((fn=(callback)myimport ("graphics_xforms", "suspend_callback"))!=NULL){
         fn ((gui_function)frontera_guisuspend_aux);
      }
   }
   else{
      fn ((gui_function)frontera_guisuspend_aux);
   }
}

int freeobj_filteredImage_handle(FL_OBJECT *obj, int event, FL_Coord mx, FL_Coord my,int key, void *xev){
  if ((event==FL_PUSH)&&(key==MOUSELEFT)&&
      ((display_state&DISPLAY_COLORIMAGEA)!=0))
    samplesource=*mycolorA;
  return 0;
}

int freeobj_groundImage_handle(FL_OBJECT *obj, int event, FL_Coord mx, FL_Coord my,int key, void *xev){
  if ((event==FL_PUSH)&&(key==MOUSELEFT)&&
      ((display_state&DISPLAY_COLORIMAGEA)!=0))
    samplesource=*mycolorA;
  return 0;
}

int freeobj_ventanaAA_handle(FL_OBJECT *obj, int event, FL_Coord mx, FL_Coord my,int key, void *xev){
  if ((event==FL_PUSH)&&(key==MOUSELEFT)&&
      ((display_state&DISPLAY_COLORIMAGEA)!=0))
    samplesource=*mycolorA;
  return 0;
}

int freeobj_ventanaBB_handle(FL_OBJECT *obj, int event, FL_Coord mx, FL_Coord my,int key, void *xev){
  if ((event==FL_PUSH)&&(key==MOUSELEFT)&&
      ((display_state&DISPLAY_COLORIMAGEB)!=0))
    samplesource=*mycolorB;
  return 0;
}

void motion_event(FL_OBJECT *ob, Window win, int win_width, 
		  int win_height, XKeyEvent *xev, void *user_data){
  float longi, lati,r ;

	xev = (XKeyPressedEvent*)xev;

	/* las siguiente ecuaciones sirven mapear el movimiento:

	Eje X :: (0,w) -> (-180,180)
	Eje Y :: (0,h) -> (-90,90)

	donde (h,w) es el larcho,ancho del gl_canvas
	Si el button pulsado es 2 (centro del raton) entonces no actualizamos la posicion

	*/

	if (2 == xev->keycode) {
		flashImage = TRUE;
	  return;
	}

	mouse_on_canvas.x = ((xev->x*360/fd_fronteragui->canvas->w)-180);
	mouse_on_canvas.y = ((xev->y*-180/fd_fronteragui->canvas->h)+90);
	
	mouse_on_minicanvas.x = ((xev->x*360/fd_fronteragui->minicanvas->w)-180);
	mouse_on_minicanvas.y = ((xev->y*-180/fd_fronteragui->minicanvas->h)+90);

	mouse_on_fronteracanvas.x = ((xev->x*360/fd_fronteragui->fronteracanvas->w)-180);
	mouse_on_fronteracanvas.y = ((xev->y*-180/fd_fronteragui->fronteracanvas->h)+90);

}

void button_press_event (FL_OBJECT *ob, Window win, int win_width, 
			 int win_height, XKeyEvent *xev, void *user_data){
  
  xev = (XKeyPressedEvent*)xev;
  float longi, lati,r;

	/* Los dos modos son excluyentes */
	if (2 == xev->keycode) {
		flashImage = TRUE;
	  return;
	}else {
		flashImage = FALSE;
		if (1 == xev->keycode){
		  cam_mode = 1;
		  foa_mode = 0;  
		}else if (3 == xev->keycode){
		  foa_mode = 1;
		  cam_mode = 0;
		}else if (5 == xev->keycode){
		  if (radius < MAX_RADIUS_VALUE)
		    radius+=WHEEL_DELTA;
		}else if (4 == xev->keycode){
		  if (radius>MIN_RADIUS_VALUE)
		    radius-=WHEEL_DELTA;
		}
	}
	
}

void frontera_guiresume_aux(void)
{
  static int k=0;

  if (k==0) /* not initialized */ {
		k++;
		fd_fronteragui = create_form_fronteragui();
		fl_set_form_position(fd_fronteragui->fronteragui,400,50);
		fl_add_canvas_handler(fd_fronteragui->canvas,Expose,InitOGL,0);
		fl_add_canvas_handler(fd_fronteragui->minicanvas,Expose,InitMiniOGL,0);
		fl_add_canvas_handler(fd_fronteragui->fronteracanvas,Expose,InitFronteraOGL,0);
		fl_add_canvas_handler(fd_fronteragui->canvas,MotionNotify,motion_event,0);
		fl_add_canvas_handler(fd_fronteragui->minicanvas,MotionNotify,motion_event,0);
		fl_add_canvas_handler(fd_fronteragui->fronteracanvas,MotionNotify,motion_event,0);
		fl_add_canvas_handler(fd_fronteragui->canvas,ButtonPress,button_press_event,0);
		fl_add_canvas_handler(fd_fronteragui->minicanvas,ButtonPress,button_press_event,0);
		fl_add_canvas_handler(fd_fronteragui->fronteracanvas,ButtonPress,button_press_event,0);
  }

	myregister_buttonscallback(frontera_guibuttons);
	myregister_displaycallback(frontera_guidisplay);
	fl_show_form(fd_fronteragui->fronteragui,FL_PLACE_POSITION,FL_FULLBORDER,"frontera");
	fronteragui_win = FL_ObjWin(fd_fronteragui->ventanaA);
	image_displaysetup();
}

void frontera_guiresume(){
   static callback fn=NULL;
   if (fn==NULL){
      if ((fn=(callback)myimport ("graphics_xforms", "resume_callback"))!=NULL){
         fn ((gui_function)frontera_guiresume_aux);
      }
   }
   else{
      fn ((gui_function)frontera_guiresume_aux);
   }
}

//extern void frontera_guisuspend(void);
void createConfigFile () {
	FILE *salida;
	salida = fopen (configFile,"w");

	if (salida == NULL) {
		fprintf(stderr,"Parser: I can't found file %s\n", configFile); exit(-1);
	}

	printf("Building config file %s...\n", configFile);
	fprintf(salida,"dimension = %d\n", 4000);
	fprintf(salida,"resolucion = 50\n");
	fprintf(salida,"tipo = ec_diferencial\n");
	fprintf(salida,"ec_diferencial_speed = 1\n");
	fprintf(salida,"paso_tiempo = nulo\n");
	fprintf(salida,"cell_angles = 0\n");
	fprintf(salida,"mayoria_saturacion = 30\n");
	fprintf(salida,"mayoria_ruido = 10\n");
	fprintf(salida,"long_historia = 0\n");
	fprintf(salida,"\n");
	fprintf(salida,"sonar_filtra = independientes\n");
	fprintf(salida,"sonar_geometria = cono_denso\n");
	fprintf(salida,"sonar_apertura = 20.\n");
	fprintf(salida,"sonar_noobstacle = 3000.\n");
	fprintf(salida,"sonar_radialerror = 10.\n");
	fprintf(salida,"sonar_fdistancia = lineal\n");
	fprintf(salida,"sonar_residuo = 0.05\n");
	fprintf(salida,"sonar_o = 0.4\n");
	fprintf(salida,"sonar_e = -0.5\n");
	fprintf(salida,"sonar_mind = 700\n");
	fprintf(salida,"sonar_maxd = 1100.\n");
	fprintf(salida,"\n");
	fprintf(salida,"robot_geometria = cilindro\n");
	fprintf(salida,"robot_radio = 248.\n");
	fprintf(salida,"robot_e = -0.8\n");
	fprintf(salida,"\n");
	fprintf(salida,"laser_geometria = cono_denso\n");
	fprintf(salida,"laser_apertura = 0.5\n");
	fprintf(salida,"laser_muestras = 90\n");
	fprintf(salida,"laser_noobstacle = 8000.\n");
	fprintf(salida,"laser_o = 1\n");
	fprintf(salida,"laser_e = -0.7\n");

	fclose(salida);
}

void frontera_startup()
{
  pthread_mutex_lock(&(all[frontera_id].mymutex));
  myexport("frontera","id",&frontera_id);
  myexport("frontera","resume",(void *) &frontera_resume);
  myexport("frontera","suspend",(void *) &frontera_suspend);
  printf("frontera schema started up\n");
  put_state(frontera_id,slept);
  pthread_create(&(all[frontera_id].mythread),NULL,frontera_thread,NULL);
	if (myregister_buttonscallback==NULL){
    if ((myregister_buttonscallback=
	 (registerbuttons)myimport ("graphics_xforms", "register_buttonscallback"))==NULL){
      printf ("I can't fetch register_buttonscallback from graphics_xforms\n");
      jdeshutdown(1);
    }
    if ((mydelete_buttonscallback=(deletebuttons)myimport ("graphics_xforms", "delete_buttonscallback"))==NULL){
      printf ("I can't fetch delete_buttonscallback from graphics_xforms\n");
      jdeshutdown(1);
    }
    if ((myregister_displaycallback=
	 (registerdisplay)myimport ("graphics_xforms", "register_displaycallback"))==NULL){
      printf ("I can't fetch register_displaycallback from graphics_xforms\n");
      jdeshutdown(1);
    }
    if ((mydelete_displaycallback=(deletedisplay)myimport ("graphics_xforms", "delete_displaycallback"))==NULL){
      jdeshutdown(1);
      printf ("I can't fetch delete_displaycallback from graphics_xforms\n");
    }
  }

  if ((myscreen=(int *)myimport("graphics_xforms", "screen"))==NULL){
    fprintf (stderr, "frontera: I can't fetch screen from graphics_xforms\n");
    jdeshutdown(1);
  }
  if ((mydisplay=(Display *)myimport("graphics_xforms", "display"))==NULL){
    fprintf (stderr, "frontera: I can't fetch display from graphics_xforms\n");
    jdeshutdown(1);
  }
  pthread_mutex_unlock(&(all[frontera_id].mymutex));

	// Valores iniciales para la c�mara virtual con la que observo la escena de puntos 3D en el suelo
  virtualcam0.position.X=4000.;
  virtualcam0.position.Y=4000.;
  virtualcam0.position.Z=6000.;
  virtualcam0.position.H=1.;
  virtualcam0.foa.X=0.;
  virtualcam0.foa.Y=0.;
  virtualcam0.foa.Z=0.;
  virtualcam0.position.H=1.;
  virtualcam0.roll=0.;

	// Valores iniciales para la c�mara virtual con la que observo la escena
  virtualcam1.position.X=4000;
  virtualcam1.position.Y=4000.;
  virtualcam1.position.Z=6000.;
  virtualcam1.position.H=1.;
  virtualcam1.foa.X=0.;
  virtualcam1.foa.Y=0.;
  virtualcam1.foa.Z=0.;
  virtualcam1.position.H=1.;
  virtualcam1.roll=0.;

	// Valores para la c�mara virtual con la que observo el entorno de frontera al robot
  virtualcam2.position.X=4000;
  virtualcam2.position.Y=4000;
  virtualcam2.position.Z=6000;
  virtualcam2.position.H=1.;
  virtualcam2.foa.X=0;
  virtualcam2.foa.Y=0;
  virtualcam2.foa.Z=0;
  virtualcam2.foa.H=1.;
  virtualcam2.roll=0;

	initCalibration (); // calibramos la c�mara del robot
	setRobotCameraPos (); // con los par�metros de calibraci�n, posicionamos nuestra robotCamera

	configFile = "./gradientPlanning.conf";
	createConfigFile ();

  int argc=1;
  char **argv;
  char *aa;
  char a[]="myjde";
 
  aa=a;
  argv=&aa;

	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);

	RGB2HSV_init();
	RGB2HSV_createTable();
}
